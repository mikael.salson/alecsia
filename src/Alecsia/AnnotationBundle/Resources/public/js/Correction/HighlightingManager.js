function HighlightingManager(id_fichier) {

   this.id_fichier = id_fichier;
   this.$pre = $("#cp_" + id_fichier).children("pre");
   if (this.$pre.length == 0) {
      return;
   }
   this.$hlpool;

   // Resultats du pré-calcul
   this.margin_width;
   this.char_width;
   this.line_height;


   if (typeof HighlightingManager.initialized == "undefined") {

      /*==================
       = Initialisation =
       ================== */
      HighlightingManager.prototype.init = function () {

         // Taille de la marge (Num. de lignes)
         this.margin_width = parseInt(this.$pre.find("ol > li > div").position()['left']);
         this.margin_width += parseInt(this.$pre.find("ol > li > div").css("paddingLeft"));

         // Fonte
         var font = this.$pre.css("font");

         // Calcul de la largeur de la fonte
         var $span = $(document.createElement("span"));
         $span.html("a");
         // $span.css("font",font);
         $("pre").eq(0).append($span);
         this.char_width = $span.width();
         $span.remove();

         // Calcul de la hauteur d'une ligne
         this.line_height = this.$pre.find("li").outerHeight(true);

         // Création du HL Pool
         this.$hlpool = $('<div id="hlpool_' + this.id_fichier + '" class="hlpool"></div>');
         this.$pre.append(this.$hlpool);

         // Référencement des instances
         HighlightingManager[this.id_fichier] = this;

      };


      /*=======================
       = Ajout d'annotations =
       ======================= */
      // Prépare le surlignage d'une partie du document selon les bornes données
      HighlightingManager.prototype.addAnnotation =
              function (id_annot, bl, bc, el, ec) {
                 // On s'assure que l'on a à faire à des entiers
                 bl = parseInt(bl);
                 bc = parseInt(bc);
                 el = parseInt(el);
                 ec = parseInt(ec);

                 var $parts = $();
                 var iligne = bl;
                 while (iligne <= el) {
                    var col_b = bc;
                    var col_e = ec;

                    if (iligne != bl) {
                       col_b = 1;
                    }
                    if (iligne != el) {
                       var taille_iligne = this.$pre.find("li").eq(iligne - 1).text().length;
                       col_e = taille_iligne + 1;
                    }

                    var $part = this.addPart(iligne, col_b, col_e);
                    $parts = $parts.add($part);
                    iligne++;
                 }

                 // Création de l'élement annotation
                 var $hl = $('<div id="hl_' + id_annot + '" class="hlannot"></div>').append($parts);
                 this.$hlpool.append($hl);


              };

      // Prépare le surlignage d'une partie (ou la globalité) d'une ligne
      // selon les bornes données.
      HighlightingManager.prototype.addPart =
              function (l, bc, ec) {
                 var taille_ligne = this.$pre.find("li").eq(l - 1).text().length;

                 var last_column = taille_ligne + 1;

                 // Calcul des "vraies" bornes à appliquer
                 var bhl = bc;
                 var ehl = ec;

                 if (bc < 1) {
                    bhl = 1
                 }
                 if (ec > last_column) {
                    ehl = last_column
                 }

                 // Calcul de la position/taille du surlignage
                 var top = (l - 1) * this.line_height;
                 var left = this.margin_width + (bhl - 1) * this.char_width;
                 var width = (ehl - bhl) * this.char_width;

                 // Création de l'élément
                 var $part = $(document.createElement("div"));
                 $part.css({
                    "top": top,
                    "left": left,
                    "width": width,
                    "height": this.line_height
                 });

                 return $part;
              }

      /*===============
       = Suppression =
       =============== */
      HighlightingManager.prototype.delAnnotation = function (id_annot) {
         $("#hl_" + id_annot).remove();
      };

      /*==============
       = Surlignage =
       ============== */
      HighlightingManager.prototype.highlight = function (id_annot) {
         $("#hl_" + id_annot).addClass("active");
      };

      HighlightingManager.prototype.unhighlight = function (id_annot) {
         $("#hl_" + id_annot).removeClass("active");
      };

      HighlightingManager.initialized = true;
   }

   this.init();
}

// Recherche auto des fichiers à gérer
function installHLM() {
   $(".annotation_panel > ul").each(function () {
      var $this = $(this);
      var id = $this.attr("id").substr(3);
      var hlm = new HighlightingManager(id)






   });
}

$(installHLM);
