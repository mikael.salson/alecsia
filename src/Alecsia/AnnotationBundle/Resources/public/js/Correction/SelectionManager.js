// Fichier récupérant à partir d'une selection la position des indices de début
// et de fin de celle-ci

$(function () {
   SelectionManager = new Object();

   /*============================
    = Fonctions intermédiaires =
    ============================ */
   // Etant donné un noeud DOM renvoie le nombre de caractères le précédent
   SelectionManager.textBefore = function (node) {
      var nbchar = 0
      var basenode = node.previousSibling;
      while (basenode != null) {
         nbchar += $(basenode).text().length;
         basenode = basenode.previousSibling;
      }
      return nbchar;
   }

   // Renvoie la colonne dans le fichier en fonction d'un noeud de texte et d'un offset.
   // /!\ On suppose les node positionnés dans un fichier correct !
   SelectionManager.getColumnFromDOMInfos = function (node, offset) {
      var column = offset;

      while ($(node).filter("div").size() == 0) {
         column += SelectionManager.textBefore(node);
         node = node.parentNode;
      }

      return column + 1;
   };


   // Renvoie la ligne dans le fichier en fonction d'un noeud de texte et d'un offset.
   // /!\ On suppose les node positionnés dans un fichier correct !
   SelectionManager.getLineFromDOMInfos = function (node) {
      return $(node).parents("li").index() + 1;
   };

   // Renvoie l'id du fichier dans lequel se situe le node donné en paramètre.
   // Renvoie -1 si le node est situé à l'extérieur d'un fichier
   SelectionManager.getFileFromDOMInfos = function (node) {
      var $fichier_panel = $(node).parents(".geshi").parents(".fichier_panel");
      if ($fichier_panel.size() == 0) {
         return -1;
      }
      else {
         return parseInt($fichier_panel.attr("id").substring(2));
      }
   };

   /*==================
    = "The" fonction =
    ================== */
   // Renvoie un array sous la forme [fid,dl,dc,fl,fc] correspondant aux offsets
   // de la sélection en cours. Renvoie null dans le cas où la sélection ne correspond
   // pas à une sélection annotable (sélection hors fichier ou sélection englobant plusieurs fichiers)
   SelectionManager.getSelectionOffsets = function () {

      // Recupération de la sélection
      var selection = window.getSelection();
      if (selection.isCollapsed) {
         return null;
      } // Si la selection est nulle renvoyer null.

      // Détails de la selection
      var node1 = selection.anchorNode;
      var offset1 = selection.anchorOffset;

      var node2 = selection.focusNode;
      var offset2 = selection.focusOffset;

      // Récupération du fichier sélectionné
      var file1 = SelectionManager.getFileFromDOMInfos(node1);
      var file2 = SelectionManager.getFileFromDOMInfos(node2);
      var f_id;

      if (file1 != file2 || file1 == -1) {
         return null;
      }
      else {
         f_id = file1;
      }

      // On est assuré que la sélection est contenue dans un et un seul fichier
      // Récupération des offsets
      var l1 = SelectionManager.getLineFromDOMInfos(node1);
      var c1 = SelectionManager.getColumnFromDOMInfos(node1, offset1);
      var l2 = SelectionManager.getLineFromDOMInfos(node2);
      var c2 = SelectionManager.getColumnFromDOMInfos(node2, offset2);

      // On a les offsets, on doit désormais reconnaître le minimum.
      var dl, dc, fl, fc;
      if (l1 < l2) {
         dl = l1;
         dc = c1;
         fl = l2;
         fc = c2;
      }
      else if (l2 < l1) {
         dl = l2;
         dc = c2;
         fl = l1;
         fc = c1;
      }
      else {
         if (c1 < c2) {
            dl = l1;
            dc = c1;
            fl = l2;
            fc = c2;
         }
         else {
            dl = l2;
            dc = c2;
            fl = l1;
            fc = c1;
         }
      }

      return [f_id, dl, dc, fl, fc];


   }
});

