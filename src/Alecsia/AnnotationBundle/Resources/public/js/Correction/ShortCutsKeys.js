/**
 * Created by root on 14/02/15.
 */
/**
 * Created by David JOSIAS on 14/02/15.
 *
 * Code JQuery associant le raccourci Ctrl + Shift (MAJ) + F à la page de correction des archives afin de:
 * - marquer l'archive comme étant terminée
 * - retourner au sujet si l'archive est déjà marquée comme terminée
 *
 */


document.onkeydown = function (e) {
   if (document.activeElement.id == "") {
      // Ctrl+Shift+F
      if (e.which == 70 && e.ctrlKey && e.shiftKey) {
         e.preventDefault();
         var nextPage = $("#end_correction_top_button").attr("href");
         if (nextPage != null)
            window.location.href = nextPage;
         return false;
      }
   }
}
