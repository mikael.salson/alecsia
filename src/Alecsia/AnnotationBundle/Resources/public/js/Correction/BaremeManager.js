/*
 * Manager chargé de synchroniser l'affichage du barème avec le serveur.
 */

BaremeManager = new Object();

BaremeManager.$wrapper = $("#bareme");
BaremeManager.$bouton = $("#bouton_bareme");
BaremeManager.$panel = $("#bareme_panel");

BaremeManager.rendu = $("#rendu_id").val();
BaremeManager.urlcontrolleur = $("#url_refreshbareme").val();

// Passe le bouton en mode loading
BaremeManager.loading = function () {
   BaremeManager.$bouton.removeClass("error");
   BaremeManager.$bouton.addClass("loading");
   BaremeManager.$bouton.html("<span>Calcul...</span>");
   BaremeManager.$bouton.unbind("click");
}

// Passe le bouton en mode erreur
BaremeManager.error = function () {
   BaremeManager.$bouton.removeClass("loading");
   BaremeManager.$bouton.addClass("error");

   BaremeManager.$bouton.html("Erreur survenue. Réessayer?");
   BaremeManager.$bouton.click(BaremeManager.refresh);
}

// Remplace les boutons/panels existant par de nouveaux, donnés par le code HTML.
BaremeManager.traiterReponse = function (html) {
   var $html = $(html);
   var $bareme = $html.filter("#bareme");

   // Remplacement du contenu
   if ($bareme.size() == 0) {
      return false;
   } else {
      BaremeManager.$wrapper.html($bareme.html());
   }

   // Actu des variables
   BaremeManager.$wrapper = $("#bareme");
   BaremeManager.$bouton = $("#bouton_bareme");
   BaremeManager.$panel = $("#bareme_panel");

   // Recréation du DDLink
   new DDLink(BaremeManager.$bouton.get(0), BaremeManager.$panel.get(0), true);

   // Animation
   if (BaremeManager.$bouton.hasClass("lookatmenegatif")) {
      BaremeManager.$bouton.removeClass("lookatmenegatif", 1000);
   } else {
      BaremeManager.$bouton.removeClass("lookatme", 1000);
   }

}


BaremeManager.refresh = function () {

   // REQUETE AJAX
   var data = new Object();
   data["rendu_id"] = BaremeManager.rendu;
   data["ancienne_note"] = $("#bareme_notefinale").text();

   if (typeof BaremeManager.urlcontrolleur != 'undefined') {
      $.post(BaremeManager.urlcontrolleur, data, BaremeManager.traiterReponse, "html")
              .error(BaremeManager.error);

      // Feedback de chargement
      BaremeManager.loading();
   }
}
