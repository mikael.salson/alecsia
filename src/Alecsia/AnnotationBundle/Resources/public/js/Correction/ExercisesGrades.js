/**
 * Created by David on 22/02/15.
 */

function activateExercise(tr) {

    var data = new Object();

    var comment = $(tr).find("input#commentaire").val();
    var form = $(tr).find("form");
    var exoName = $(tr).find("input#nom").val();

    //S'il existe des annotations pour le même exercice, on récupère leurs identifiants pour les supprimer.
    var $annotations = $("div#ap_entete").find("ul#ap_0 li").has("div.header:contains('"+exoName+"') + div.contenu:contains('"+comment+"')");
    if ($annotations.length > 0) {
        deleteExistingExerciseAnnotations($annotations, form.attr('data-delaction'))
        return ;
    }

    //on récupère la note correspondant à l'exercice
    var note = $(tr).find("span.note");
    
    note.html("0");
    note.removeClass("positif");
    note.removeClass("negatif");
    note.addClass("negatif");
    data["valeur"] = '-100%';


    //Données à envoyer en post via ajax
    data["rendu_id"] = $(tr).find("input#rendu").val();
    data["nom"] = exoName;
    data["commentaire"] = $(tr).find("input#commentaire").val();
    data["exercice"] = $(tr).find("input#exercice").val();
    data["makemodelein"] = -1;
    data["modele_id"] = -1;
    data["debutligne"] = 0;
    data["debutcolonne"] = 0;
    data["fincolonne"] = 0;
    data["finligne"] = 0;

    // Envoi de la requête AJAX au contrôleur
    var URL_createAnnot = form.attr("data-addaction");

    $.post(URL_createAnnot, data, AnnotationFormManager.traiterReponse, "json")
        .error(AnnotationFormManager.traiterErreurServeur);
}

function deleteExistingExerciseAnnotations(annotations, url) {
    var $delannots = "";
    var $id;
    var data = new Object();
    //Pour chaque élément li dans le tableau annotations, on y met l'id de celui-ci (l'id correspond en vérité à l'id de l'annotation).
    for(var i=0;i<annotations.length;i++){
        $id = $(annotations[i]).attr("id");
        data['annot_id'] = $id.substring("an_".length, $id.length)
        $.post(url, data, AnnotationFormManager.traiterReponse, 'json')
            .error(AnnotationFormManager.traiterErreurServeur);
    }
}    
