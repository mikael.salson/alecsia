function ModeleChooser(inputid) {

   ModeleChooser.$input = $("#" + inputid);
   ModeleChooser.data_url = $("#modelechooser_url").val();
   ModeleChooser.nberrors = 0;

   if (typeof ModeleChooser.initialized == "undefined") {

      ModeleChooser.activer = function () {
         ModeleChooser.$input.css("display", "block");
      }

      ModeleChooser.desactiver = function () {
         ModeleChooser.$input.css("display", "none");
      }

      ModeleChooser.clean = function () {
         ModeleChooser.$input.val("");
      }

      // Change le json base des infos procurées au widget
      ModeleChooser.changerSource = function (json) {
         ModeleChooser.nberrors = 0;
         ModeleChooser.$input.catcomplete("option", "source", json);
      }

      // Récupère la liste de modèles depuis le serveur
      ModeleChooser.recupererModeles = function (exercice) {
         if ((typeof (exercice) == "undefined") || (exercice == null)) {
            exercice = -1;
         }

         $.post(ModeleChooser.data_url, {"exercice_id": exercice}, ModeleChooser.changerSource, "json")
                 .error(function () {
                    ModeleChooser.nberrors++;
                    if (ModeleChooser.nberrors > 3) {
                       Errors.show("Impossible de communiquer avec le sujet",
                               "Il est possible que la liste de modèles ne soit plus à jour. <br/> Le sujet a-t-il été supprimé?");
                    } else {
                       ModeleChooser.recupererModeles();
                    }
                 })
      }

      /*================
       = Key Handlers =
       ================ */
      // Rend à la touche tab un simple rôle de sélection
      ModeleChooser.handleTabKey = function (e) {
         var TABKEY = 9;
         if (e.which == TABKEY) {
            e.preventDefault();
            return false;
         }
      }


      /*==================
       = Initialisation =
       ================== */

      ModeleChooser.itemSelected = function (evt, ui) {
         var item = ui.item;
         var keys = Object.keys(item);

         // Remplissage des champs & desactivation de certains
         for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            if (key.substr(0, 4) == "fma_") {
               var $input = $("#" + key);
               $input.val(item[key]);
            }
            if (key.substr(0, 8) == "disable_") {
               var idinput = key.substr(8)
               var $input = $("#" + idinput);

               if (item[key] == true) {
                  $input.attr("disabled", "disabled");
               } else {
                  $input.removeAttr("disabled");
               }

            }
         }

         // Focus 
         $("#" + item.focus).focus();
      }


      // Lie le widget aux formulaires
      ModeleChooser.eventify = function () {
         ModeleChooser.$input.bind("catcompleteselect", ModeleChooser.itemSelected);
         ModeleChooser.$input.bind("focus", function () {
            $(this).catcomplete("search", "");
         });
         ModeleChooser.$input.bind("keydown", ModeleChooser.handleTabKey);
      }

      ModeleChooser.init = function (inputid) {
         var availableTags = [];

         ModeleChooser.$input.catcomplete({
            source: availableTags,
            autoFocus: true,
            delay: 0,
            minLength: 0,
            html: true,
            appendTo: "#modelechooser_widget",
         });

         ModeleChooser.recupererModeles();
         ModeleChooser.eventify();
      }

      ModeleChooser.initialized = true;
   }

   ModeleChooser.init(inputid);
}


// Recherche auto des forms à gérer
function installMC() {
   new ModeleChooser("modelechooser_input");
}

$(installMC);
