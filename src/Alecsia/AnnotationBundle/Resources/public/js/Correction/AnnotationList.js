function AnnotationList(id_list) {

   this.$list = $("ul#" + id_list);
   this.annotPosManager;
   this.liaisonManager;
   this.highlightingManager;

   if (typeof AnnotationList.initialized == "undefined") {

      /*=====================================
       = Ajout / Suppression d'annotations =
       ===================================== */

      /* Ajoute une annotation grâce à son code HTML. */
      AnnotationList.prototype.addAnnotation = function (html) {
         $html = $(html);

         // Vérification de la validité de l'annotation :
         // 1 - Nombre d'annotations = 1
         var nbAnnotDansHtml = $html.filter("li.annot").size();
         if (nbAnnotDansHtml != 1) {
            return false;
         }

         // 2 - Annotation non existante.
         var idAnnot = $html.attr("id");
         var nbAnnotAvecMemeId = $("#" + idAnnot).size();
         if (nbAnnotAvecMemeId != 0) {
            return false;
         }

         // Tout est OK, Ajout de l'annotation
         this.$list.append($html);
         AnnotationList.attachToManagers($html);
         AnnotationList.eventify($html);
         this.annotPosManager.repositionner();
         this.liaisonManager.refresh();

         // Animation
         $html.removeClass("lookatme", 1000);
      };

      /* Modifier une annotation grâce au code HTML de cette dernière, modifiée. */
      AnnotationList.prototype.updateAnnotation = function (html) {
         $newannot = $(html);

         // Vérification de la validité de l'annotation :
         // 1 - Nombre d'annotations = 1
         var nbAnnotDansHtml = $newannot.filter("li.annot").size();
         if (nbAnnotDansHtml != 1) {
            return false;
         }

         // 2 - Annotation existante?
         var idAnnot = $newannot.attr("id");
         var $annotAModifier = $("#" + idAnnot);
         if ($annotAModifier.size() == 0) {
            return false;
         }

         // Modification de l'annotation
         $annotAModifier.html($newannot.html());

         // Repositionnement
         this.annotPosManager.repositionner();
         this.liaisonManager.refresh();

         // Animation
         $annotAModifier.addClass("lookatme");
         $annotAModifier.removeClass("lookatme", 1000);



         // TODO : Modification du surlignage ?
      };

      /* Supprimer une annotation */
      AnnotationList.prototype.deleteAnnotation = function (id) {
         var annot_id = "an_" + id;
         var $annot = $("#" + annot_id);

         if ($annot.size() != 0) {
            // Suppression...
            // ...liaison
            this.liaisonManager.retirerLiaison(id);
            this.liaisonManager.refresh();

            // ... element html
            $annot.animate({left: "-600px"}, 400, "easeInOutSine", function () {
               // On supprime l'annotation et on repositionne les autres
               var $this = $(this);
               var al_id = parseInt($this.parent().attr("id").substr(3));

               $this.remove();
               AnnotPosManager[al_id].repositionner();
               LiaisonsManager[al_id].refresh();

            });

            // ... surlignage
            this.highlightingManager.delAnnotation(id);
         }
      };


      /* ==================
       = Event Handlers =
       ================== */
      AnnotationList.handlers = new Object();

      // Appelé lorsque la souris entre sur une annot.
      AnnotationList.handlers.mouseInAnAnnotation = function (evt) {
         $this = $(this);
         $this.addClass("hover");

         var idAnnot = $this.attr("id").substr(3);
         var idFichier = $this.parent("ul").attr("id").substr(3);

         LiaisonsManager[idFichier].activer(idAnnot);
         LiaisonsManager[idFichier].refresh();

         HighlightingManager[idFichier].highlight(idAnnot);
      };

      // Appelé lorsque la souris sort d'une annot.
      AnnotationList.handlers.mouseOutAnAnnotation = function (evt) {
         $this = $(this);
         $this.removeClass("hover");

         var idAnnot = $this.attr("id").substr(3);
         var idFichier = $this.parent("ul").attr("id").substr(3);

         LiaisonsManager[idFichier].desactiver(idAnnot);
         LiaisonsManager[idFichier].refresh();

         HighlightingManager[idFichier].unhighlight(idAnnot);
      };

      // Appelé lors d'un clic sur une annotation
      AnnotationList.handlers.clickOnAnAnnotation = function (evt) {
         $this = $(this);

         var idAnnot = $this.attr("id");

         AnnotationFormManager.get.initFormModif(idAnnot);
         $("#modal_add_annotation").jqmShow();
      };


      // Ajout des comportements sur une annotations
      AnnotationList.eventify = function ($annot) {

         $annot.hover(
                 AnnotationList.handlers.mouseInAnAnnotation,
                 AnnotationList.handlers.mouseOutAnAnnotation
                 );

         if (!($("#correctionUI").hasClass("lectureseule"))) {
            $annot.click(
                    AnnotationList.handlers.clickOnAnAnnotation
                    );
         }

      };


      /*=============================
       = Interaction avec handlers =
       ============================= */

      // Rend cette annotation visible des managers 
      AnnotationList.prototype.attachToManagers = function ($annot) {

         // Ajout du surlignage
         var id_annot = $annot.attr("id").substr(3);
         var $infos = $annot.children(".infos_annot");
         var dl = $infos.children(".dl").text();
         var dc = $infos.children(".dc").text();
         var fl = $infos.children(".fl").text();
         var fc = $infos.children(".fc").text();
         this.highlightingManager.addAnnotation(id_annot, dl, dc, fl, fc);

         // Ajout de la liaison
         this.liaisonManager.ajouterLiaison(id_annot, dl);
      };

      // Version statique de la fonction précédente
      AnnotationList.attachToManagers = function ($annot) {
         var id_al = $annot.find(".infos_annot > .f_id").text();
         AnnotationList[id_al].attachToManagers($annot);
      };

      /* ==================
       = Initialisation =
       ================== */
      AnnotationList.prototype.init = function (id_list) {

         // Référencement des objets AnnotationList
         var id = id_list.substr(3);
         AnnotationList[id] = this;

         // Récupération des "Manager"
         this.liaisonManager = LiaisonsManager[id];
         this.highlightingManager = HighlightingManager[id];
         this.annotPosManager = AnnotPosManager[id];

         // Association des comportements aux annotations
         this.$list.children(".annot").each(function () {
            $this = $(this);
            AnnotationList.attachToManagers($this);
            AnnotationList.eventify($this);
         });



         // positionnement des annot
         this.annotPosManager.repositionner();

         // refresh des liaisons
         this.liaisonManager.refresh();

      };


      AnnotationList.initialized = true;
   }

   this.init(id_list);
}

// Recherche auto des listes à gérer
function installALM() {
   $(".annotation_panel").children("ul").each(function () {
      $this = $(this);
      new AnnotationList($this.attr("id"));
   });
}

$(installALM);