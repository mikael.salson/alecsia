// Fichier gérant le dessin des liens entre annotations et 
// lignes de code


function LiaisonsManager(apcan_id, ap_id, f_id) {
   this.canvas = $("#apcan_" + apcan_id);
   this.annotation_panel = $("#ap_" + ap_id);
   this.fichier_panel = $("#f_" + f_id);

   this.width = 0;
   this.height = 0;
   this.liaisons = new Array();

   this.ol = this.canvas.parent().parent().find("ol");

   if (typeof LiaisonsManager.initialized == "undefined") {

      /*========================
       = Gestion des liaisons =
       ======================== */
      LiaisonsManager.prototype.ajouterLiaison =
              function (annot_id, offset_ligne) {
                 var liaison = new Array(offset_ligne, 0);
                 this.liaisons[annot_id] = liaison;
              };

      LiaisonsManager.prototype.activer = function (annot_id) {
         if (typeof (this.liaisons[annot_id]) != "undefined") {
            this.liaisons[annot_id][1] = 1;
         }
      };

      LiaisonsManager.prototype.desactiver = function (annot_id) {
         if (typeof (this.liaisons[annot_id]) != "undefined") {
            this.liaisons[annot_id][1] = 0;
         }
      };

      LiaisonsManager.prototype.retirerLiaison = function (annot_id) {
         this.liaisons[annot_id] = undefined;
      };

      /*==========
       = Dessin =
       ========== */

      // "Charge" le crayon fin (lignes normales)
      LiaisonsManager.prototype.crayonFin =
              function (ctx) {
                 ctx.strokeStyle = "#888888";
                 ctx.lineWidth = 1;
              }

      // "Charge" le crayon épais (lignes sélectionnées)
      LiaisonsManager.prototype.crayonEpais =
              function (ctx) {
                 ctx.strokeStyle = "#D3CC98";
                 ctx.lineWidth = 3;
              }

      // Vide le canvas
      LiaisonsManager.prototype.clearCanvas =
              function (ctx) {
                 var ctx = this.canvas.get(0).getContext("2d");
                 ctx.clearRect(0, 0, this.width, this.height);
              }

      // Redessine le canvas en fonction des infos données
      LiaisonsManager.prototype.draw =
              function () {
                 var ctx = this.canvas.get(0).getContext("2d");
                 this.clearCanvas();
                 for (var annot_id in this.liaisons) {
                    // Recup des objets
                    var liaison = this.liaisons[annot_id];

                    if (typeof (liaison) != "undefined") {
                       var annotation = $("#an_" + annot_id);
                       var is_active = liaison[1];
                       var ligne = this.ol.children().eq(liaison[0] - 1);
                       var positionpre = ligne.parents("pre.geshi").position().top;

                       // Calcul des offsets
                       var offb = annotation.position().top + (ligne.get(0).offsetHeight / 2);
                       var offe = positionpre + ligne.position().top + (ligne.get(0).offsetHeight / 2);

                       // Calculs des points de contrôle de la courbe de Bézier 
                       var contbx = 20;
                       var contby = offb;
                       var contex = this.width - 20;
                       var contey = offe;

                       // Dessin
                       ctx.beginPath();
                       ctx.moveTo(0, offb);
                       ctx.bezierCurveTo(contbx, contby, contex, contey, this.width, offe);
                       if (is_active) {
                          this.crayonEpais(ctx);
                       } else {
                          this.crayonFin(ctx);
                       }
                       ctx.stroke();
                    }
                 }
              };

      LiaisonsManager.prototype.refresh =
              function () {
                 this.ajusterTaille();
                 this.draw();
              }

      /*==================
       = Initialisation =
       ================== */
      // Initialiser la hauteur du canvas
      LiaisonsManager.prototype.ajusterTaille =
              function () {
                 var annot_panel = this.annotation_panel;
                 var f_panel = this.fichier_panel;

                 // Determination de la hauteur max entre annotations et fichier
                 var annot_height = annot_panel.get(0).offsetHeight;
                 var fichier_height = f_panel.get(0).offsetHeight;

                 var max_height = 0;
                 if (annot_height > fichier_height) {
                    max_height = annot_height;
                 }
                 else {
                    max_height = fichier_height;
                 }

                 // Le canvas prend cette hauteur
                 this.height = max_height;
                 this.width = parseInt(this.canvas.css("width"));

                 this.canvas.get(0).setAttribute("width", this.width);
                 this.canvas.get(0).setAttribute("height", this.height);
              };

      LiaisonsManager.prototype.init = function (id) {
         LiaisonsManager[id] = this;
      };


      LiaisonsManager.initialized = true;
   }

   this.init(apcan_id);
   this.ajusterTaille();
}

// Recherche auto des canvas à gérer
function installLM() {
   $("canvas.apcan").each(function () {
      var $this = $(this);
      var id = $this.attr("id").substr(6);
      var lm = new LiaisonsManager(id, id, id);

      lm.draw();
   });
}

$(installLM);
 