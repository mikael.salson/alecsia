
function notConnected() {
   Errors.show("Déconnexion", "Vous semblez avoir été déconnecté. Ne faîtes aucune modification avant d'avoir rafraîchi cette page, sans quoi ces modifications seraient perdues. Ça serait dommage, non ?");
}

function checkAuth() {
   // Envoi de la requête AJAX au contrôleur
   $.post($("#check_auth_url").val(), null, null)
           .error(notConnected);
}

setInterval(checkAuth, 300000); 