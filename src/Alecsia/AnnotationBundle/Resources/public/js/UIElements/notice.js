
/* Affichage des messages d'info */
Notice = new Object();
Notice.show = function (titre, message) {

   $confirmbox = $("#modal_notice");
   $title = $("#modal_notice").find(".header > h3");
   $message = $("#modal_notice").find("p");

   $title.html(titre);
   $message.html(message);

   $confirmbox.jqmShow();
};

/* On anime d'entrée de jeu les flash messages */
$(function () {
   $("#modal_notice").modal();
});
