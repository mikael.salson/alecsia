// Permet d'activer/desactiver des objets au fil d'une selection

$(function () {

   selectionDependant = {
      init:
              function ($fire) {

                 $fire.bind("selection", function () {
                    var $this = $(this);
                    var id = $this.attr("id");

                    $('*[data-selectiondependant="' + id + '"]').removeClass("invisible");
                 });

                 $fire.bind("deselectiontotal", function () {
                    var $this = $(this);
                    var id = $this.attr("id");

                    $('*[data-selectiondependant="' + id + '"]').addClass("invisible");
                 });
              }
   };


   $(".selectionFire").each(function () {
      selectionDependant.init($(this));
   });
});