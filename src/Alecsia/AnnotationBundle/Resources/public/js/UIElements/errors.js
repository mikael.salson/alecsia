
/* Affichage des messages d'erreur */
Errors = new Object();
Errors.show = function (titre, message) {

   $errorbox = $("#modal_error");
   $title = $("#modal_error").find(".header > h2");
   $message = $("#modal_error").find("p");

   $title.html(titre);
   $message.html(message);

   $errorbox.jqmShow();
};

/* On anime d'entrée de jeu les flash messages */
$(function () {
   $("#modal_error").modal();
});
