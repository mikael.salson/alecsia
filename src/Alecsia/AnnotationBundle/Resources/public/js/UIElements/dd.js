// dd.js
// Gestionnaire de "dropdown panels", panels apparaissant au clic
// sur un élément, et disparaissant au clic sur l'exterieur du panel.

function DDLink(bouton, panel, right) {
   this.bouton = bouton;
   this.panel = panel;
   this.right = right;

   // Coordonnées du bouton
   this.btop = this.bouton.offsetTop;
   this.bleft = this.bouton.offsetLeft;
   this.bwidth = this.bouton.offsetWidth;
   this.bheight = this.bouton.offsetHeight;

   // Coordonnées du panel
   this.pwidth = this.panel.offsetWidth;
   this.pheight = this.panel.offsetHeight;

   if (typeof DDLink.initialized == "undefined") {

      // -----------------------------
      //  Initialisation
      // -----------------------------

      DDLink.prototype.placerPanel =
              function () {
                 this.btop = this.bouton.offsetTop;
                 this.bleft = this.bouton.offsetLeft;
                 this.bwidth = this.bouton.offsetWidth;
                 this.bheight = this.bouton.offsetHeight;
                 this.pwidth = this.panel.offsetWidth;
                 this.pheight = this.panel.offsetHeight;

                 // Calcul des coordonnées du panel
                 var ptop = this.btop + this.bheight - 1;
                 var pleft = 0;

                 if (this.right) {
                    pleft = this.bleft - (this.pwidth - this.bwidth);
                 } else {
                    pleft = this.bleft;
                 }

                 // Application des changements
                 $(this.panel).css({"top": ptop, "left": pleft});
              }

      DDLink.placerTousPanel =
              function () {
                 $(".action_dd").each(function () {
                    $(this).data("ddlink").placerPanel();
                 })
              }

      DDLink.prototype.addFix =
              function () {
                 var $panel = $(this.panel)
                 $panel.prepend('<div class="fix"></div>');
                 $fix = $panel.find(".fix");

                 $fix.css("width", this.bwidth - 3);

                 if (this.right) {
                    $fix.css("right", 0);
                 } else {
                    $fix.css("left", 2);
                 }
              }

      DDLink.prototype.addEventsListener =
              function () {
                 // Toggle simple sur le bouton
                 $(this.bouton).click(this.panel, this.togglePanel);

                 // Disparition sur clic exterieur
                 $(document).click(this.hideAllPanel);

                 // On empêche l'evt "clic exterieur" de se déclencher
                 // lors d'un clic à l'intérieur du panel.
                 $(this.panel).click(function (e) {
                    e.stopPropagation();
                 });
              }

      // This = Bouton
      DDLink.prototype.togglePanel =
              function (e) {
                 e.preventDefault();
                 e.stopPropagation();
                 $this = $(this);

                 if ($this.hasClass("active")) {
                    DDLink.prototype.hideAllPanel();
                 }

                 else {
                    DDLink.prototype.hideAllPanel();
                    $this.addClass("active");
                    $(e.data).addClass("active");
                 }
              }

      DDLink.prototype.hideAllPanel =
              function (evt) {
                 $(".panel_dd.active").removeClass("active").prev().removeClass("active");
              };

      DDLink.prototype.init =
              function (bouton) {
                 // Esthétique & Placement
                 this.placerPanel();
                 this.addFix();
                 this.addEventsListener();
                 $(bouton).data("ddlink", this);
              }

      DDLink.initialized = true;
   }

   this.init(bouton);
}

// Initialisation de dd.
function installDD() {
   $(".action_dd").each(function () {
      $this = $(this);
      var bouton = $this.get(0);

      var $panel = $this.next();
      var panel = $panel.get(0);

      var right = $panel.hasClass("right");

      new DDLink(bouton, panel, right);
   });

   $(window).resize(DDLink.placerTousPanel);
}

$(installDD);
