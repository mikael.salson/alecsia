function ActivatorAll($obj) {

   this.state = 0; // 0 - Tout décoché, 1 - Partiel, 2 - Tout coché
   this.$obj = $obj;
   this.$table = $("#" + $obj.attr("data-table"));

   if (typeof ActivatorAll.initialized == "undefined") {

      ActivatorAll.handlers = new Object();

      ActivatorAll.handlers.click = function (e) {
         var $this = $(this);
         var manager = $this.data("manager");


         switch (manager.state) {
            case 0:
            case 1:
               tableLignes.activerTout(manager.$table);
               break;

            case 2:
               tableLignes.desactiverTout(manager.$table);
               break;

            default:
               console.error("Etat invalide");

         }
      }

      ActivatorAll.handlers.tablePartiel = function (e) {
         var $this = $(this);
         var $activator_all = $(".activator_all[data-table='" + $this.attr("id") + "']");
         var manager = $activator_all.data("manager");

         manager.changeState(1);
      }

      ActivatorAll.handlers.tableTout = function (e) {
         var $this = $(this);
         var $activator_all = $(".activator_all[data-table='" + $this.attr("id") + "']");
         var manager = $activator_all.data("manager");

         manager.changeState(2);
      }

      ActivatorAll.handlers.tableVide = function (e) {
         var $this = $(this);
         var $activator_all = $(".activator_all[data-table='" + $this.attr("id") + "']");
         var manager = $activator_all.data("manager");

         manager.changeState(0);
      }

      ActivatorAll.prototype.changeState = function (state) {
         this.state = state;
         var new_class;
         switch (state) {
            case 0:
               new_class = "ico_cochevide";
               break;

            case 1:
               new_class = "ico_cochepartiel";
               break;

            case 2:
               new_class = "ico_cochepleine";
               break;

            default:
               console.error("Etat invalide");
         }

         this.$obj.find("span").attr("class", new_class);
      }

      ActivatorAll.prototype.init = function () {
         this.$obj.data("manager", this);
         this.$obj.click(ActivatorAll.handlers.click);

         this.$table.bind("selection", ActivatorAll.handlers.tablePartiel);
         this.$table.bind("deselection", ActivatorAll.handlers.tablePartiel);
         this.$table.bind("selectiontotal", ActivatorAll.handlers.tableTout);
         this.$table.bind("deselectiontotal", ActivatorAll.handlers.tableVide);
      };

      ActivatorAll.initialized = true;
   }

   this.init();
}

// Recherche auto des boutons activator
function installAA() {
   $("button.activator_all").each(function () {
      var $this = $(this);
      new ActivatorAll($this);
   });

   $(".activator").change();
}

$(installAA);