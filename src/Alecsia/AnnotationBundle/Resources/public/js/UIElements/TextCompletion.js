$("form *[autocomplete-path]").autocomplete({
   source: function (requete, reponse) {
      for (i = 0; i < $(this).size(); i++) {
         var nom = $(this)[i].element.val();
         var DATA = 'nom=' + nom;
         $.ajax({
            type: "POST",
            url: $(this)[i].element.attr("autocomplete-path"),
            dataType: 'json',
            data: DATA,
            success: function (donnee) {
               reponse($.map(donnee, function (objet) {
                  return objet;
               }));
            }
         });
      }
   }
});


$("[autocomplete-student]")
        .autocomplete({
           open: function () {
              $(this).data("autocomplete").menu.element.addClass("student_chooser");
           },
           source: function (request, response) {
              $.ajax({
                 url: $(this)[0].element.attr("autocomplete-student") + request.term,
                 dataType: "json",
                 success: function (donnee) {
                    response($.map(donnee, function (objet) {
                       return {
                          label: (objet.lastName != null) ? (objet.lastName + ", " + objet.firstName) : objet.login,
                          value: objet.login
                       }
                    }));
                 },
              })
           },
           focus: function (event, ui) {
              $("#studentName").val(ui.item.label);
              return false;
           },
           select: function (event, ui) {
              $("#_switch_user").val(ui.item.value);
              return false;
           }
        });
