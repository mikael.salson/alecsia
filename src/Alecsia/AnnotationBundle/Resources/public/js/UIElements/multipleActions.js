// Permet d'activer/desactiver des objets au fil d'une selection

$(function () {

   multipleActions = {
      handler:
              function (e) {
                 $this = $(this);
                 $form = $this.parents("form").eq(0);
                 $action = $form.find("input[name='action']");
                 $action.val($this.attr("data-action"));
                 $form.submit();
                 e.preventDefault();
              }
   };


   $("form *[data-action]").click(multipleActions.handler);
});