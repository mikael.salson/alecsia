function TabbedWidget($widget) {

   this.$widget = $widget;

   if (typeof TabbedWidget.initialized == "undefined") {

      TabbedWidget.prototype.initWidget = function () {
         // On recherche un tab actif
         var $tabs = this.$widget.children(".header").children(".tab");
         var index_actif = $tabs.index(".tab.active");

         if (index_actif != -1) {
            this.switchTo(index_actif);
         } else {
            this.switchTo(0);
         }

         // Data
         $tabs.parents(".widget").data("tabmanager", this);

         // Evenements
         $tabs.click(function () {
            $this = $(this);
            var index_tab = $this.parent().children(".tab").index($this);
            var manager = $this.parents(".widget").data("tabmanager");
            manager.switchTo(index_tab);
         });

      };

      TabbedWidget.prototype.switchTo = function (index) {
         var $tabs = this.$widget.children(".header").children(".tab");

         if (index >= $tabs.size() || index < 0) {
            return;
         } else {
            $tabs.removeClass("active")
                    .eq(index)
                    .addClass("active");

            this.$widget.children(".contenu").removeClass("active")
                    .eq(index)
                    .addClass("active");
         }
      };

      TabbedWidget.initialized = true;
   }

   this.initWidget();
}

// Recherche auto des canvas à gérer
function installTW() {
   $(".widget.tabbed").each(function () {
      var $this = $(this);
      new TabbedWidget($this);
   });
}

$(installTW);