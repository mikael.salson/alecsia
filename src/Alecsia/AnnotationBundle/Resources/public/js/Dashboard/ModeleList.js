function ModeleList(id_list) {

   this.$list = $("ul#" + id_list);
   this.$widget = this.$list.parent();

   if (typeof ModeleList.initialized == "undefined") {

      /*=====================================
       = Ajout / Suppression d'annotations =
       ===================================== */

      /* Ajoute une annotation grâce à son code HTML. */
      ModeleList.prototype.addModele = function (html, verifUnicite) {
         if (verifUnicite == null) {
            verifUnicite = true;
         }
         else {
            verifUnicite = false
         }

         $html = $(html);

         // Vérification de la validité du HTML :
         // 1 - Nombre de modeles = 1
         var nbModelesDansHtml = $html.filter("li.modelannot").size();
         if (nbModelesDansHtml != 1) {
            return false;
         }

         // 2 - Modele non existant.
         if (verifUnicite) {
            var idModele = $html.attr("id");
            var nbModeleAvecMemeId = $("#" + idModele).size();
            if (nbModeleAvecMemeId != 0) {
               return false;
            }
         }

         // Ajout d'un modele
         var ajoutEffectif = function (liste, html) {
            return function () {
               liste.append(html);
               ModeleList.eventify(html);
               html.removeClass("lookatme", 1000);
            }
         }(this.$list, $html);

         // On fait apparaitre la liste en cas de besoin
         if (this.$widget.hasClass("hidden")) {
            this.displayList(ajoutEffectif);
         } else {
            ajoutEffectif();
         }

      };

      /* Modifier un modèle grâce au code HTML de ce dernier, modifié. */
      ModeleList.prototype.updateModele = function (html) {
         $newmodele = $(html);

         // Vérification de la validité du modèle:
         // 1 - Nombre d'annotations = 1
         var nbModelesDansHtml = $newmodele.filter("li.modelannot").size();
         if (nbModelesDansHtml != 1) {
            return false;
         }

         // 2 - Modele existant?
         var idModele = $newmodele.attr("id");
         var $modeleAModifier = $("#" + idModele);
         if ($modeleAModifier.size() == 0) {
            return false;
         }

         // On vérifie que le modèle va aller dans le même exercice.
         var ex_newmodele = $newmodele.children(".infos").children(".ex").text();
         var ex_modeleamodif = $modeleAModifier.children(".infos").children(".ex").text();

         if (ex_newmodele != ex_modeleamodif) {
            ModeleList[ex_modeleamodif].deleteModele(idModele.substr(3));
            ModeleList[ex_newmodele].addModele(html, false);
         }
         else {
            // Modification du modèle
            $modeleAModifier.html($newmodele.html());

            // Animation
            $modeleAModifier.addClass("lookatme");
            $modeleAModifier.removeClass("lookatme", 1000);
         }
      };

      /* Supprimer un modèle */
      ModeleList.prototype.deleteModele = function (id) {
         var modele_id = "mo_" + id;
         var $modele = $("#" + modele_id);

         if ($modele.size() != 0) {

            // On fait disparaître la liste si elle n'a plus raison d'être
            if (this.$list.children().size() == 1) {
               this.hideList();
               $modele.remove();
            }
            // Ou uniquement l'annotation.
            else {
               $modele.slideUp(400, "easeOutQuad", function () {
                  var $this = $(this);
                  $this.remove();
               });
            }
         }
      };

      /*====================================
       = Affichage / Masquage de la liste =
       ==================================== */
      ModeleList.prototype.displayList = function ($callback) {
         this.$widget.slideDown(200, "easeOutQuad", $callback);
         this.$widget.removeClass("hidden");
      };

      ModeleList.prototype.hideList = function () {
         this.$widget.slideUp(200, "easeOutQuad", function () {
            $(this).addClass("hidden");
         });
      };

      /* ==================
       = Event Handlers =
       ================== */
      ModeleList.handlers = new Object();

      // Appelé lorsque la souris entre sur une annot.
      ModeleList.handlers.mouseInAModele = function (evt) {
         $this = $(this);
         $this.addClass("hover");
      };

      // Appelé lorsque la souris sort d'une annot.
      ModeleList.handlers.mouseOutAModele = function (evt) {
         $this = $(this);
         $this.removeClass("hover");
      };

      // Appelé lors d'un clic sur une annotation
      ModeleList.handlers.clickOnAModele = function (evt) {
         $this = $(this);

         var idModele = $this.attr("id");

         ModeleFormManager.get.initFormModif(idModele);
         $("#modal_add_modele").jqmShow();
      };


      // Ajout des comportements sur une annotations
      ModeleList.eventify = function ($annot) {
         $annot.hover(
                 ModeleList.handlers.mouseInAModele,
                 ModeleList.handlers.mouseOutAModele
                 );

         $annot.click(
                 ModeleList.handlers.clickOnAModele
                 );
      };



      /* ==================
       = Initialisation =
       ================== */
      ModeleList.prototype.init = function (id_list) {

         // Référencement des objets ModeleList
         ModeleList[id_list.substr(6)] = this;


         // Association des comportements aux annotations
         this.$list.children(".modelannot").each(function () {
            $this = $(this);
            ModeleList.eventify($this);
         });

      };


      ModeleList.initialized = true;
   }

   this.init(id_list);
}

// Recherche auto des listes à gérer
function installModeleList() {
   $(".listemodeles").each(function () {
      $this = $(this);
      new ModeleList($this.attr("id"));
   });
}

$(installModeleList);