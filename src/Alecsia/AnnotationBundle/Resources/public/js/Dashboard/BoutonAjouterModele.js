// Clic sur bouton Annoter
$(function () {
   $("#bouton_ajouter_modele").click(function () {
      ModeleFormManager.get.initFormAjout();
      $("#modal_add_modele").jqmShow();
   });
});

// Association à la touche 'a'
$(function () {
   $(document).keydown(function (e) {
      if ($("#modal_add_modele:visible").size() == 0 && $("#bouton_ajouter_modele:visible").size() != 0) {
         if (e.which == 65 && getTopModal().length == 0) {
            $("#bouton_ajouter_modele").addClass("active");
         }
      }
   });

   $(document).keyup(function (e) {
      if ($("#modal_add_modele:visible").size() == 0 && $("#bouton_ajouter_modele:visible").size() != 0) {
         if (e.which == 65 && getTopModal().length == 0) {
            $("#bouton_ajouter_modele").removeClass("active");
            $("#bouton_ajouter_modele").click();
         }
      }
   })
});
