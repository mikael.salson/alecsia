/**
 * Created by root on 23/04/15.
 */

$s1 = "Tous les modèles";

$s2 = "Modèles populaires";

$("#bouton_most_popular_models").click(function(){//Un clic sur le bouton provoque:

    if($(this).attr('class').contains('mo_pop')){//si le bouton contient la classe mo_pop (=> on veut afficher les modèles populaires
        //on retire la classe mo_pop
        $(this).removeClass('mo_pop');
        //on ajoute la classe all_mo (=> pour le prochain click)
        $(this).addClass('all_mo');
        //on change le texte du bouton (voir variables au-dessus de la fonction)
        $(this).text($s1);
        //on affiche le header
        //$(".header_mo_pop").css("display", "block");
        //on enlève les modèles "non-populaires"
        /*$("li.notpopular").each(function(){
            $(this).css("display", "none");
        });*/
        $("li.notpopular").each(function(){
           $(this).animate({
               height: "toggle"
           }, 400, function() {
               // Animation complete.
           });
        });
        //on affiche le nombre d'occurrences des modèles populaires
        //$("li.modelannot.popular").find(".occurrence").css("display","block");

    }
    else if($(this).attr('class').contains("all_mo")){//si le bouton contient la classe all_mo (=> on veut afficher tous les modèles)
        //on enlève la classe all_mo
        $(this).removeClass('all_mo');
        //on ajoute la classe mo_pop pour le prochain click
        $(this).addClass('mo_pop');
        //on change le texte du bouton
        $(this).text($s2);
        //on affiche tous les modèles "non-populaires"
        /*$("li.notpopular").each(function(){
            $(this).css("display", "block");
        });*/
        $("li.notpopular").each(function(){
            $(this).animate({
                height: "toggle"
            }, 400, function() {
                // Animation complete.
            });
        });
        //$(".header_mo_pop").css("display", "none");
        //$("li.modelannot.popular").find(".occurrence").css("display","none");
    }
});
