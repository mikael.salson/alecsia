function addExerciseToGrading() {
    row = $("#form_manage_grading table.tableLignes tr:nth(1)").clone()
    row.find("input").each(function() {
        $(this).attr('value', '');
    }).end()
    select = row.find('select')[0]
    select.selectedOptions[0].removeAttribute('selected')
    row.find('input:first').attr('value', -1)
    $('#form_manage_grading table.tableLignes tr:last').after(row)
}
