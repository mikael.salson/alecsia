function ModeleFormManager(id_form) {

   this.$modele_form = $("#" + id_form);
   this.$window = this.$modele_form.parents(".jqmWindow");

   this.$bouton_delete = $("#fma_delete");
   this.$bouton_submit = $("#fma_submit");

   // Bouton desactivable
   this.$bouton_desactive = null;
   this.bouton_label = null;

   // Mémoire du dernier exercice sélectionné
   this.lastex = 0;

   this.$mid = this.$modele_form.find("input#fma_modele_id");
   this.$lid = this.$modele_form.find("input#fma_liste_id");
   this.$nom = this.$modele_form.find("input#fma_nom");
   this.$comm = this.$modele_form.find("textarea#fma_commentaire");
   this.$ex = this.$modele_form.find("select#fma_exercice");
   this.$val = this.$modele_form.find("input#fma_valeur");

   // Propagation
   this.$suppr = this.$modele_form.find("input#fma_suppr");

   // Dialogue "supprimer annotations liées?"
   this.$ask_suppr = $("#modal_ask_suppr");

   if (typeof ModeleFormManager.initialized == "undefined") {

      /*============================
       = Soumission du formulaire =
       ============================ */
      ModeleFormManager.prototype.submitForm = function () {

         // Préparation des données (valeurs de champs...)
         var URL_controleur = this.$modele_form.attr("action");

         var data = new Object();
         data["modele_id"] = $("input#fma_modele_id").val();
         data["liste_id"] = $("input#fma_liste_id").val();
         data["nom"] = $("input#fma_nom").val();
         data["commentaire"] = $("textarea#fma_commentaire").val();
         data["valeur"] = $("input#fma_valeur").val();
         data["exercice"] = $("select#fma_exercice").val();

         this.lastex = data["exercice"];

         // Envoi de la requête AJAX au contrôleur
         $.post(URL_controleur, data, ModeleFormManager.traiterReponse, "json")
                 .error(ModeleFormManager.traiterErreurServeur);
         this.disable(this.$bouton_submit);
      }

      /*====================
       = Bouton supprimer =
       ==================== */
      ModeleFormManager.prototype.warnForSuppr = function (evt) {
         ModeleFormManager.get.$ask_suppr.jqmShow();
         evt.preventDefault();
      }

      ModeleFormManager.prototype.submitDelete = function () {
         // URL du controlleur
         var URL_controleur = this.$modele_form.attr("data-delaction");

         // Données
         var data = new Object();
         data["modele_id"] = $("input#fma_modele_id").val();
         data["suppr_annots"] = this.$suppr.val();

         // Envoi requête AJAX
         $.post(URL_controleur, data, ModeleFormManager.traiterReponse, "json")
                 .error(ModeleFormManager.traiterErreurServeur);
         this.disable(this.$bouton_delete);
      }


      /*==========================
       = Traitement de réponses =
       ========================== */
      ModeleFormManager.traiterReponse = function (data, textStatus, jqXHR) {
         var reponse = data;

         // traitement des ajouts
         for (var i = 0; i < reponse.ajouts.length; i++) {
            var html = reponse.ajouts[i];
            ModeleFormManager.traiterReponseAjout(html);
         }

         // traitement des modifs
         for (var i = 0; i < reponse.modifs.length; i++) {
            var html = reponse.modifs[i];
            ModeleFormManager.traiterReponseModif(html);
         }

         // traitement des suppressions
         for (var i = 0; i < reponse.supprs.length; i++) {
            var id = reponse.supprs[i];
            ModeleFormManager.traiterReponseSuppression(id);
         }

         // Disparition de la modale
         $("#modal_add_modele").jqmHide();
         ModeleFormManager.get.clean();

         // traitement de l'erreur
         if (reponse.notification != null) {
            if (reponse.notification.erreur) {
               Errors.show(reponse.notification.titre, reponse.notification.contenu);
            } else {
               Notice.show(reponse.notification.titre, reponse.notification.contenu);
            }
         }

      }

      // REPONSE RECUE 
      // Ajout :
      ModeleFormManager.traiterReponseAjout = function (html) {

         // Ajout du modèle
         var ml_id = $(html).find(".infos > .ex").text();
         ModeleList[ml_id].addModele(html);
         $("#lm_vide").css("display", "none");
      }

      // Modif : 
      ModeleFormManager.traiterReponseModif = function (html) {

         // Modif du modèle
         var ml_id = $(html).find(".infos > .ex").text();
         ModeleList[ml_id].updateModele(html);

         $("body").trigger("notes_changees");

      }

      // Suppression : 
      ModeleFormManager.traiterReponseSuppression = function (mod_id) {

         // Suppression de l'annotation
         var ml_id = $("#mo_" + mod_id).find(".infos > .ex").text();
         ModeleList[ml_id].deleteModele(mod_id);

         // Affichage état vide
         var nbmodeles = 0;
         for (var i = 0; i < $(".listemodeles").size(); i++) {
            nbmodeles += $(".listemodeles").eq(i).children().size();
         }

         if (nbmodeles == 0) {
            $("#lm_vide").css("display", "block");
         }

         $("body").trigger("notes_changees");

      }

      // Erreur (du serveur, type 500) 
      ModeleFormManager.traiterErreurServeur = function () {
         Errors.show("Oh, non...", "Pour une raison inconnue, le serveur a refusé cette action.");
         $("#modal_add_modele").jqmHide();
         ModeleFormManager.get.clean();
      }

      /*=========
       = Etats =
       ========= */
      ModeleFormManager.prototype.disable = function ($bouton) {
         this.$bouton_desactive = $bouton;
         this.bouton_label = $bouton.text();
         this.$modele_form.find("input,textarea,select,button").attr("disabled", "disabled");
         $bouton.text("Veuillez patienter...");
      };

      ModeleFormManager.prototype.enable = function () {
         this.$modele_form.find("input,textarea,select,button").removeAttr("disabled");
         this.$bouton_desactive.text(this.bouton_label);
      };

      ModeleFormManager.prototype.clean = function () {
         this.$modele_form.find("form").each(function () {
            this.reset();
         });
         this.enable();
      };


      /*===================
       = Bouton "Submit" =
       =================== */
      ModeleFormManager.submitHandlers = new Object();

      ModeleFormManager.submitHandlers.justSubmitForm = function (evt) {
         ModeleFormManager.get.submitForm();
         evt.preventDefault();
      }


      /*==============================
       = Construction du formulaire =
       ============================== */
      // AJOUT
      ModeleFormManager.prototype.initFormAjout = function () {

         // Comportement bouton submit
         this.$modele_form.unbind("submit");
         this.$modele_form.submit(ModeleFormManager.submitHandlers.justSubmitForm);

         // Champ d'action
         var actionURL = this.$modele_form.attr("data-addaction");
         this.$modele_form.attr("action", actionURL);

         // Classe CSS
         this.$window.removeClass("tomodif");
         this.$window.addClass("toadd");

         // Label
         this.bouton_label = "Ajouter";
         this.$bouton_submit.text(this.bouton_label);

         // Retour au dernier exercice ajouté
         this.$ex.val(this.lastex);

      };

      // MODIF
      ModeleFormManager.prototype.initFormModif = function (modele_id) {
         var $annot = $("#" + modele_id);

         // Comportement bouton submit 
         this.$modele_form.unbind("submit");
         this.$modele_form.submit(ModeleFormManager.submitHandlers.justSubmitForm);


         // Récup infos de l'annotation à modifier
         var $annot_infos = $annot.children(".infos");
         var ex = $annot_infos.children(".ex").text();

         var modele_id = modele_id.substr(3);

         var nom = $annot.find(".raw_title").text();
         var comm = $annot.find(".raw_comment").text();
         var valeur = $annot.find(".valeur").text();


         // Remplissage champs invisibles
         this.$mid.val(modele_id);

         // Remplissage champs visibles
         this.$nom.val(nom);
         this.$comm.val(comm);
         this.$val.val(valeur);
         this.$ex.val(ex);


         // Champ d'action
         var actionURL = this.$modele_form.attr("data-modifaction");
         this.$modele_form.attr("action", actionURL);

         // Classe CSS
         this.$window.removeClass("toadd");
         this.$window.addClass("tomodif");

         // Label
         this.bouton_label = "Modifier";
         this.$bouton_submit.text(this.bouton_label);

      };



      /*==================
       = Initialisation =
       ================== */
      ModeleFormManager.prototype.init = function (id_form) {
         ModeleFormManager.get = this;
         this.$bouton_delete.click(ModeleFormManager.get.warnForSuppr);
         this.initAskSuppr();
      }

      ModeleFormManager.prototype.initAskSuppr = function () {
         this.$ask_suppr.modal();
         $("#suppr_no").click(function () {
            ModeleFormManager.get.$suppr.val("0");
            ModeleFormManager.get.$ask_suppr.jqmHide();
            ModeleFormManager.get.submitDelete();
         });

         $("#suppr_yes").click(function () {
            ModeleFormManager.get.$suppr.val("1");
            ModeleFormManager.get.$ask_suppr.jqmHide();
            ModeleFormManager.get.submitDelete();
         });
      }

      ModeleFormManager.initialized = true;
   }

   this.init(id_form);
}


// Recherche auto des forms à gérer
function installMFM() {
   new ModeleFormManager("form_manage_modele");
}

$(installMFM);
