<?php

namespace Alecsia\AnnotationBundle\Twig\Extension;

use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Bundle\TwigBundle\Loader\FilesystemLoader;

class Utils extends \Twig_Extension {

   public function getFilters() {
      return array(
          'addslashes' => new \Twig_Filter_Method($this, 'twig_addslashes'),
          'ord' => new \Twig_Filter_Method($this, 'twig_ord'),
          'chr' => new \Twig_Filter_Method($this, 'twig_chr'),
      );
   }

   public function getName() {
      return 'utils';
   }

   public function twig_addslashes($value) {
      return addslashes($value);
   }

   public function twig_ord($value) {
      return ord($value);
   }

   public function twig_chr($value) {
      return chr($value);
   }

}
