<?php

namespace Alecsia\AnnotationBundle\Twig\Extension;

use Alecsia\AnnotationBundle\Service\Markdown;

class MD2Html extends \Twig_Extension
{
    private $parser;

    public function __construct(Markdown $parser)
    {
        $this->parser = $parser;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter(
                'md2html',
                array($this, 'markdownToHtml'),
                array('is_safe' => array('html'))
            ),
            new \Twig_SimpleFilter(
                'md2htmlline',
                array($this, 'markdownToHtmlLine'),
                array('is_safe' => array('html'))
            ),
        );
    }

    public function markdownToHtml($content)
    {
        return $this->parser->toHtml($content);
    }

    public function markdownToHtmlLine($content)
    {
        return $this->parser->toHtmlOneLine($content);
    }

    public function getName()
    {
        return 'MD2Html';
    }
}


?>