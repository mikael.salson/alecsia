<?php

namespace Alecsia\AnnotationBundle\Twig\Extension;

use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Bundle\TwigBundle\Loader\FilesystemLoader;

function basic_sort($key, $comparison) {
   return function ($first, $second) use ($key, $comparison) {
      if (is_array($key)) {
         for ($i = 0; $i < count($key); $i++) {
            $member_function = $key[$i];
            $result = $comparison($first->$member_function(), $second->$member_function());
            if ($result != 0)
               return $result;
         }
         return 0;
      }
      return $comparison($first->$key(), $second->$key());
   };
}

function intcmp($a, $b) {
   return (int) $a - (int) $b;
}

function int_sort($key) {
   return basic_sort($key, '\Alecsia\AnnotationBundle\Twig\Extension\intcmp');
}

function str_sort($key) {
   return basic_sort($key, 'strcmp');
}

class ObjectSort extends \Twig_Extension {

   public function getFilters() {
      return array(
          'objectsort' => new \Twig_Filter_Method($this, 'twig_objectsort'),
          'strobjectsort' => new \Twig_Filter_Method($this, 'twig_strobjectsort'),
          'strarraysort' => new \Twig_Filter_Method($this, 'twig_strarraysort'),
          'arraysort' => new \Twig_Filter_Method($this, 'twig_arraysort'),
      );
   }

   public function getName() {
      return 'objectsort';
   }

   public function twig_arraysort($value, $key) {
      uasort($value, int_sort($key));
      return $value;
   }

   public function twig_strarraysort($value, $key) {
      uasort($value, str_sort($key));
      return $value;
   }

   /**
    * Sort an ArrayCollection of objects
    */
   public function twig_objectsort($value, $key) {
      $iterator = $value->getIterator();
      $iterator->uasort(int_sort($key));
      return $iterator;
   }

   /**
    * Sort an array collection of objects whose method returns a string
    */
   public function twig_strobjectsort($value, $key) {
      $iterator = $value->getIterator();
      $iterator->uasort(str_sort($key));
      return $iterator;
   }

}
