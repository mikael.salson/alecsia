<?php

namespace Alecsia\AnnotationBundle\Controller\Admin;

use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Entity\Fichier;
use Alecsia\AnnotationBundle\Entity\Sujet;

class CleanController extends AlecsiaController {

    public function listOldUnusedFilesAction() {
        $old_files = $this->getFileService()->getOldUnusedFiles();
        return $this->render('AnnotationBundle:Admin:Clean/list_old_files.html.twig', array('files' => $old_files));
    }

    public function removeAllUnusedFilesAction() {
        $params = $this->getGetParameters();
        if (empty($params['sure'])) {
            return $this->redirectAndSendError($this->generateUrl('AlecsiaTeacherHome'),
            "Sure ?", "Tell me that you're sure!");
        }
        $old_files = $this->getFileService()->getOldUnusedFiles();
        foreach($old_files as $file) {
            unlink($file->getCheminAbsolu());
        }
      return $this->redirect($this->generateUrl('AlecsiaTeacherHome'));
    }

    public function removeFilesAction() {
      $params = $this->getPostParameters();
      if (empty($params['file'])) {
          return $this->redirectAndSendError($this->generateUrl('AlecsiaAdmin_ListUnused'),
          'No file', 'Please select the files to remove');
      }
      foreach ($params['file'] as $fileId => $status) {
          $file = $this->getAlecsiaRepository('Fichier')->findOneById($fileId);
          if ($file != null && $status == 'on')
              unlink($file->getCheminAbsolu());
      }
      return $this->redirect($this->generateUrl('AlecsiaTeacherHome'));
    }

   private function getFileService() {
      return $this->get("alecsia.FileService");
   }    
}