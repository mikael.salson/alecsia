<?php

namespace Alecsia\AnnotationBundle\Controller\Sujet;

use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Entity\Sujet;
use Alecsia\AnnotationBundle\Entity\UE;
use Alecsia\AnnotationBundle\Entity\Group;
use Alecsia\AnnotationBundle\Entity\Rendu;
use Alecsia\AnnotationBundle\Entity\ListeModeles;

class GererSujetsController extends AlecsiaController {

   // A partir d'une liste d'ids retourne les sujets
   private function getSujetsFromIds($sujets_id) {
      $em = $this->getDoctrine()->getManager();
      $repo = $em->getRepository("AnnotationBundle:Sujet");
      $sujets = array();

      foreach ($sujets_id as $sujet_id) {
         $sujet = $repo->findOneById($sujet_id);
         if ($sujet != null) {
            $sujets[] = $sujet;
         }
      }

      return $sujets;
   }

   public function creerSujetAction() {
      $r = $this->getRequest();
      $em = $this->getDoctrine()->getManager();
      $uerepo = $em->getRepository('AnnotationBundle:UE');

      // Recup de l'UE
      $ue_id = $r->get("ue_id");
      $ue = $uerepo->findOneById($ue_id);

      if ($ue == null) {
         $this->setError('UE introuvable', "L'UE à laquelle ajouter le sujet n'a pas été trouvée.");

         $home_url = $this->generateUrl('AnnotationBundle_home');
         return $this->redirect($home_url);
      }

      // Recup des infos
      $is_td = $r->get("is_td");

      if ($is_td) {
         $nom = $r->get("nom_td");
         $note = $r->get("notesur_td");
         $noteInitiale = $r->get("noteInitiale_td");
      } else {
         $nom = $r->get("nom_tp");
         $note = $r->get("notesur_tp");
         $noteInitiale = $r->get("noteInitiale_tp");
      }

      $promo_url = $this->generateUrl('AlecsiaShowActiveUEs');
      // Refus des noms vide
      if (empty($nom)) {
         $this->setError('Impossible de créer le sujet.', "Un sujet doit comporter un nom.");

         return $this->redirect($promo_url);
      }

      //refus de commancer a noter d'un valeur plus grand que la note maximal
     /* if ($noteInitiale > $note) {
         $this->setError('Impossible de créer le sujet.', "vous noté à partir d'une note plus grande que la note maximal");

         return $this->redirect($promo_url);
      }*/
      if($noteInitiale=="max"){
         $noteInitiale=$note;
      }else{
         $noteInitiale=0;
      }


      $group = $this->GroupService()->getDefaultGroupFrom($ue, $this->getUser());
      if (is_null($group))
         return $this->redirectAndSendError($promo_url, "Impossible de trouver un groupe", "Le sujet n'a pas été créé");


      // Ajout du sujet
      $sujet = new Sujet($nom, $is_td, Sujet::STATUS_WAIT, $note, $noteInitiale);
      $ue->addSujet($sujet);

      $lm = new ListeModeles();
      $sujet->setListeModeles($lm);

      $em->persist($lm);
      $em->persist($sujet);
      $em->flush();


      /* Written work: must create all the empty works for each student */
      if ($is_td) {
         $this->createEmptyRendusForStudents($sujet);
      }

      // Succès
      $this->setSuccess('Sujet créé');

      $sujet_url = $this->generateUrl('AnnotationBundle_sujet', array("sujet_id" => $sujet->getId(), 'group_id' => $group->getId()));
      return $this->redirect($sujet_url);
   }

   public function modifierSujetAction() {

      $r = $this->getRequest();
      $em = $this->getDoctrine()->getManager();
      $s = $this->get('session');
      $repo = $em->getRepository("AnnotationBundle:Sujet");

      // Recup du sujet
      $sujet_id = $r->get("sujet_id");
      $sujet = $repo->findOneById($sujet_id);

      if ($sujet == null) {
         $this->setError('Sujet introuvable', "Le sujet que vous souhaitez renommer n'a pas été trouvé.");

         $home_url = $this->generateUrl('AnnotationBundle_home');

         return $this->redirect($home_url);
      }

      $sujet_url = $this->generateUrl('AnnotationBundle_sujet', array("sujet_id" => $sujet->getId(),
          'group_id' => $this->GroupService()->getDefaultGroupFrom($sujet->getUE(), $this->getUser())->getId()));

      // Validation
      $nom = $r->get("nom");
      if (empty($nom)) {
         $this->setError("Impossible de renommer le sujet", "Un sujet doit comporter un nom.");
         return $this->redirect($sujet_url);
      }

      // Modifs
      $sujet->setNom($nom);
      $em->flush();

      // Succès
      $this->setSuccess('Sujet renommé');
      return $this->redirect($sujet_url);
   }

   public static function supprimerSujet($em, $sujet) {

      // Suppression des exercices
      $exs = $sujet->getExercices();
      foreach ($exs as $ex) {
         $em->remove($ex);
      }

      // Suppression des modèles
      $lm = $sujet->getListeModeles();
      $modeles = $lm->getModeles();
      foreach ($modeles as $modele) {
         $em->remove($modele);
      }

      // Suppression de la liste
      $em->remove($lm);

      // Suppression des rendus
      $rendus = $sujet->getRendus();
      foreach ($rendus as $rendu) {
         GererRendusController::supprimerRendu($em, $rendu);
      }

      // Enfin, suppression du sujet
      $em->remove($sujet);
   }

   public function supprimerSujetsAction() {

      $r = $this->getRequest();
      $em = $this->getDoctrine()->getManager();
      $s = $this->get('session');

      // Recup des sujets
      $sujets_id = $r->get("sujet");
      $sujets_id = array_keys($sujets_id);
      $sujets = $this->getSujetsFromIds($sujets_id);

      // Suppression des sujets
      foreach ($sujets as $sujet) {
         self::supprimerSujet($em, $sujet);
      }

      $em->flush();
      $this->setSuccess('Sujet(s) supprimé(s)');

      $promo_url = $this->generateUrl('AlecsiaShowActiveUEs');
      return $this->redirect($promo_url);
   }

   public function supprimerSujetAction($sujet_id) {

      $r = $this->getRequest();
      $em = $this->getDoctrine()->getManager();
      $s = $this->get('session');
      $repo = $em->getRepository("AnnotationBundle:Sujet");

      // Recup du sujet
      $sujet = $repo->findOneById($sujet_id);

      if ($sujet == null) {
         $this->setError('Sujet introuvable', "Le sujet que vous souhaitez supprimer n'a pas été trouvé.");

         $home_url = $this->generateUrl('AnnotationBundle_home');

         return $this->redirect($home_url);
      }

      $promo_url = $this->generateUrl('AlecsiaShowActiveUEs');

      // Suppression du sujet
      self::supprimerSujet($em, $sujet);
      $em->flush();
      $this->setSuccess('Sujet supprimé');

      return $this->redirect($promo_url);
   }

   public function setConsultableAction($sujet_id, $group_id) {
      $sujet = $this->SubjectService()->get($sujet_id);
      $sujet->setConsultable();
      $this->getDoctrine()->getManager()->flush();

      return $this->redirect($this->generateUrl('AnnotationBundle_sujet', array('sujet_id' => $sujet_id, 'group_id' => $group_id)));
   }

   protected function createEmptyRendusForStudents(Sujet $sujet) {
      /* Traverse groups */
      foreach ($sujet->getUE()->getGroups() as $group) {
         foreach ($group->getStudents() as $student) {
            /* Create a new empty Work for that student */
            $rendu = new Rendu($student->getAbsoluteDisplayName(), $sujet, $group);
            $rendu->setDossier('');
            $rendu->addStudent($student);
            $this->WorkService()->add($rendu, $this->getUser());
         }
      }
   }

   private function SubjectService() {
      return $this->get('alecsia.SubjectService');
   }

   private function GroupService() {
      return $this->get('alecsia.GroupService');
   }

   private function WorkService() {
      return $this->get('alecsia.workService');
   }

}
