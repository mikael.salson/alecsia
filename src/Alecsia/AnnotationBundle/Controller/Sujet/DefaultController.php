<?php

namespace Alecsia\AnnotationBundle\Controller\Sujet;

use Alecsia\AnnotationBundle\Lib\AlecsiaGlobals;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Alecsia\AnnotationBundle\Entity\Exceptions\ArchiveException;
use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Entity\Annotation;
use Alecsia\AnnotationBundle\Entity\Exercice;
use Alecsia\AnnotationBundle\Entity\Rendu;
use Alecsia\AnnotationBundle\Entity\Classe;
use Symfony\Component\Security\Acl\Exception\Exception;


class DefaultController extends AlecsiaController {
   /* ===============
     = Traitements =
     =============== */

   // A partir d'un fichier/une archive envoyé avec HTTP, crée un nouveau rendu
   public function nouveauRenduAction($base_folder) {
      $em = $this->getDoctrine()->getManager();

      // Recupération de l'archive
      $request = $this->getRequest();
      $file = $request->files->get("archive_path");

      // Récupération du sujet
      $sujet_id = $request->get("sujet_id");
      $group_id = $request->get("group_id");

      if (is_null($file) || empty($file)) {
         return $this->redirectAndSendError($this->generateUrl('AnnotationBundle_sujet', array('sujet_id' => $sujet_id, 'group_id' => $group_id)), "Impossible de téléverser", "Avez-vous vraiment téléversé un fichier ? Je n'ai rien vu passer.");
      }
      if ($file->getSize() == 0) {
         return $this->redirectAndSendError($this->generateUrl('AnnotationBundle_sujet', array('sujet_id' => $sujet_id, 'group_id' => $group_id)), "Impossible de téléverser", "Le téléversement du fichier a lamentablement échoué. Trop gros peut-être ? (pas vous, le fichier évidemment)");
      }


      $sujet = $em->getRepository("AnnotationBundle:Sujet")->findOneById($sujet_id);
      $group = $em->getRepository("AnnotationBundle:Group")->findOneById($group_id);

      // Récupération du type du rendu
      $sujet_type = $request->get("archive_type");
      try {
         switch ($sujet_type) {
            case "archive":
               // Création du rendu
               $fi = new FileImporter($this->container, $base_folder);
               $fi->importArchive($sujet, $group, $file);
               break;

            case "simple":
            default:
               // Création du rendu
               $fi = new FileImporter($this->container, $base_folder);
               $fi->importRendu($sujet, $group, $file);
               break;
         }
      } catch (\Exception $e) {
         $this->setError('Rendus mal importés', $e->getMessage());
         error_log('Rendus : ' . __FILE__ . ':' . __LINE__ . "\n" . $e->getTraceAsString());
         if ($e instanceof ArchiveException) {
            error_log('Problem with: ' . $e->getFullErrorMessage());
         }
         return $this->redirect($this->generateUrl('AnnotationBundle_sujet', array("sujet_id" => $sujet_id, 'group_id' => $group_id)));
      }

      $this->setSuccess('Rendu(s) réussi(s)');

      return $this->redirect($this->generateUrl('AnnotationBundle_sujet', array("sujet_id" => $sujet_id, 'group_id' => $group_id)));
   }

   /* =======================
     = Gérer les exercices =
     ======================= */

   /**
    * 
    * @param type $sujet 
    * @param type $num_ligne
    * @param type $coef c'est le coeficient pour ajuster les valeur des exercices à la valeur du sujet
    * @param type $valeurs
    * @param type $coefInitialMark c'est le coeficient pour ajuster les valeur initiale des exercices au valeur du sujet
    * @param type $bonus_malus
    */
   private function nouvelExercice($sujet, $num_ligne, $coef, $valeurs, $bonus_malus, $exercicesguts) {
      $em = $this->getDoctrine()->getManager();
      $request = $this->getRequest();

      // Récup infos de l'exercice

      $noms = $request->get("nom");

      $numero = $num_ligne+1;
      $nom = $noms[$num_ligne];
      $valeur = $valeurs[$num_ligne];
      $bonusMalus = $bonus_malus[$num_ligne];
      $guts = $exercicesguts[$num_ligne];
      $initialMark = $sujet->getNoteInitiale() * 1. / $sujet->getNoteSur();
      // Ajout ssi tous les champs sont précisés
      if ($numero != "" && $nom != "" && $valeur != "") {
         $new_ex = new Exercice($sujet, $numero, $nom, $valeur * $coef, $valeur * $coef * $initialMark, $bonusMalus, $guts);
         $sujet->addExercice($new_ex);
         $em->persist($new_ex);
      }
   }

   private function exerciceExistant($sujet, $num_ligne, $coef, $valeurs, $bonus_malus, $exercicesguts) {
      $em = $this->getDoctrine()->getManager();
      $request = $this->getRequest();

      // Récup infos de l'exercice
      $noms = $request->get("nom");

      $numero = $num_ligne+1;
      $nom = $noms[$num_ligne];
      $valeur = $valeurs[$num_ligne];
      $bonusMalus = $bonus_malus[$num_ligne];
      $guts = $exercicesguts[$num_ligne];
      // Recup de l'exercice
      $exercice_ids = $request->get("exercice");
      $exercice_id = $exercice_ids[$num_ligne];
      $exercice = $em->getRepository("AnnotationBundle:Exercice")->findOneById($exercice_id);
      $initialMark = $sujet->getNoteInitiale() * 1. / $sujet->getNoteSur();

      if ($exercice == null) {
         return;
      }

      // Champs tous vide = suppression de l'exo
      if ($nom == "" && $valeur == "") {
         $em->remove($exercice);
      } else {
         if ($numero != "") {
            $exercice->setNumero($numero);
         }
         if ($nom != "") {
            $exercice->setNom($nom);
         }
         if ($valeur != "") {
            $exercice->setValeur($valeur * $coef);
         }
         if ($bonusMalus !== "") {
            $exercice->setBonusMalus($bonusMalus);
         }
         //if ($coefInitialMark != "") {
         $exercice->setInitialMark($coef * $valeur * $initialMark);
         // }
         if ($guts !== "") {
            $exercice->setExerciceGuts($guts);
         }
      }
   }

   private function arrondirExercices($sujet) {
      $an = $this->get("annotation.arrondirnotes");

      $exercices = $sujet->getExercices();
      $reste_initialMarks = 0;
      $reste = 0;
      foreach ($exercices as $exercice) {
         $val_ex = $exercice->getValeur() + $reste;
         $val_arrondie = $an->arrondirNote($val_ex, $an::ARR_QUART, $an::ARR_PROCHE);
         $reste = $val_ex - $val_arrondie;
         $exercice->setValeur($val_arrondie);

         //arrondir valeur initiale
         $execrciceInitialMark = $exercice->getInitialMark() + $reste_initialMarks;
         $RoundedValueinitialMark = $an->arrondirNote($execrciceInitialMark, $an::ARR_QUART, $an::ARR_PROCHE);
         $reste_initialMarks = $execrciceInitialMark - $RoundedValueinitialMark;
         $exercice->setInitialMark($RoundedValueinitialMark);
      }
   }

   public function gererExercicesAction() {

      $em = $this->getDoctrine()->getManager();
      $an = $this->get("annotation.arrondirnotes");

      $request = $this->getRequest();

      // Récup du sujet
      $sujet_id = $request->get("sujet_id");
      $sujet = $this->SubjectService()->get($sujet_id, $this->getUser());
      $sujet = $em->getRepository("AnnotationBundle:Sujet")->findOneById($sujet_id);
      if (!$sujet) {
         throw new NotFoundHttpException(sprintf('Sujet #%s introuvable.', $sujet_id));
      }

      $group_id = $request->get("group_id");
      $group = $this->GroupService()->get($group_id, $this->getUser());
      if (!$group) {
         throw new NotFoundHttpException(sprintf('Groupe #%s introuvable.', $group_id));
      }

      // Traitement des valeurs entrées
      $valeurs = array_map(function($str) {
         return abs(floatval(str_replace(",", ".", $str)));
      }, $request->get("valeur"));

      $bonus_malus = array_map(function($st) {
         return floatval(str_replace(",", ".", $st));
      }, $request->get("bonus_malus"));

      $guts = array_map(function($s) {
         return (floatval(str_replace(",", ".", $s)));
      }, $request->get("exercice_guts"));



      $notesur = abs(floatval(str_replace(",", ".", $request->get("notesur"))));
      if (empty($notesur)) {
         $notesur = $sujet->getNoteSur();
      }
      $notesur = $an->arrondirNote($notesur, $an::ARR_UNITE, $an::ARR_PROCHE);

      $nombreDeCran = abs(floatval(str_replace(",", ".", $request->get("guts"))));
      if (empty($nombreDeCran) && $nombreDeCran != 0) {
         $nombreDeCran = $sujet->getSubjectGuts();
      }
      round($nombreDeCran);


      // Calcul du coefficient multiplicateur pour ajuster les notes
      $somme_valeurs = 0;
      foreach ($valeurs as $valeur) {
         $somme_valeurs += $valeur;
      }
      if ($somme_valeurs != 0 && $request->get('arrondirNotes')) {
         $coefval = $notesur / $somme_valeurs;
      } else {
         $coefval = 1;
      }


      $noteInitiale = $sujet->getNoteInitiale();
      if ($noteInitiale != 0) {
          $noteInitiale = ($notesur / $sujet->getNoteSur()) * $noteInitiale;
          $noteInitiale = $an->arrondirNote($noteInitiale, $an::ARR_UNITE, $an::ARR_PROCHE);
          $sujet->setNoteInitiale($noteInitiale);
      }
      if ($notesur !== "") {
         $sujet->setNoteSur($notesur);
      }

      if ($nombreDeCran !== "") {
         if ($nombreDeCran < 0) {
            $nombreDeCran = 0;
         } else {
            if ($nombreDeCran > 26) {
               $nombreDeCran = 26;
            }
         }
         $sujet->setSubjectGuts($nombreDeCran);
      }


      // Ajout Modif des exercices
      $exercices = $request->get("exercice");
      foreach ($exercices as $i => $exercice) {
         if (($guts[$i] < 0) || (empty($guts) && $guts != 0)) {
            $guts[$i] = $sujet->getSubjectGuts();
         }
         if ($exercice == "-1") {
            $this->nouvelExercice($sujet, $i, $coefval, $valeurs, $bonus_malus, $guts);
         } else {
            $this->exerciceExistant($sujet, $i, $coefval, $valeurs, $bonus_malus, $guts);
         }
      }




      // Arrondissement des notes

      $this->arrondirExercices($sujet);
      $em->flush();


      $this->get("annotation.calculnotes")->rafraichirNotes($sujet);
      $this->setSuccess('Barème modifié');
      return $this->redirect($this->generateUrl('AnnotationBundle_sujet', array("sujet_id" => $sujet_id, 'group_id' => $group_id)));
   }

   /* ========
     = Vues =
     ======== */

   public function listerRendusAction($sujet_id, $group_id) {
      $em = $this->getDoctrine()->getManager();

      // Recup du sujet
      $sujet = $this->SubjectService()->get($sujet_id);
      if (!$sujet) {
         throw new NotFoundHttpException(sprintf('Sujet #%s introuvable.', $sujet_id));
      }

      // Recup des rendus
      $rendus = $em->getRepository("AnnotationBundle:Rendu")
              ->findBy(array("sujet" => $sujet->getId(), 'group' => $group_id), array("termine" => "asc"));

      return $this->render('AnnotationBundle:Dashboard:Sujet/listerendus.html.twig', array(
                  'rendus' => $rendus,
                  'sujet' => $sujet,
                  'lectureseule' => false
      ));
   }

   public function shortSubjectAction($subject_id, $group_id) {
      $em = $this->getDoctrine()->getManager();

      $subject = $this->SubjectService()->get($subject_id, $this->getUser());
      if (!$subject) {
         throw new NotFoundHttpException(sprintf('Sujet #%s introuvable.', $subject_id));
      }
      $group = $this->GroupService()->get($group_id, $this->getUser());
      if (!$group) {
         throw new NotFoundHttpException(sprintf('Groupe #%s introuvable.', $group_id));
      }

      $average = $em->getRepository('AnnotationBundle:Sujet')->getMoyenne($subject, $group);
      $nbRemainingWorks = $em->getRepository('AnnotationBundle:Sujet')->getNbWorksNotCorrected($subject, $group);
      $nbWorks = $em->getRepository('AnnotationBundle:Sujet')->getNbWorks($subject, $group);

      return $this->render('AnnotationBundle:Dashboard:Sujet/shortSubject.html.twig', array('subject' => $subject,
                  'group' => $group,
                  'average' => $average,
                  'nbRemaining' => $nbRemainingWorks,
                  'nbWorks' => $nbWorks));
   }

   public function indexAction($sujet_id, $group_id) {

      $em = $this->getDoctrine()->getManager();

      // Recup du sujet
      $sujet = $this->SubjectService()->get($sujet_id, $this->getUser());
      if (!$sujet) {
         throw new NotFoundHttpException(sprintf('Sujet #%s introuvable.', $sujet_id));
      }

      $group = $this->GroupService()->get($group_id, $this->getUser());
      if (!$group) {
         throw new NotFoundHttpException(sprintf('Groupe #%s introuvable.', $group_id));
      }

      // Recup des rendus
      $rendus = $em->getRepository("AnnotationBundle:Rendu")
              ->getWorks($sujet, $group);

      // Recup des exercices
      $exercices = $em->getRepository("AnnotationBundle:Exercice")
              ->findBy(array("sujet" => $sujet->getId()), array("numero" => "asc"));

      // Recup des modèles
      $nbmodeles = 0;

        //Récupération des modèles populaires

            //Nombre maximum de modèles populaires à afficher
        $getmp = $em -> getRepository("AnnotationBundle:Modele")
                -> getMostPopular($sujet_id, AlecsiaGlobals::$maxModelPopular, $group_id);

        $nbmodeles += count($getmp);

        $mo_pop = $em->getRepository('AnnotationBundle:Modele')
                    ->indexPopularModels($getmp);

        $modeles = $em->getRepository('AnnotationBundle:Modele')
                    ->orderPopularModelsByExo($getmp);

        //on met des tableaux vides lorsque l'exercice n'est pas annoté par un modèle (dans la vue on controlera la
        // présence de modèles pour chaque exercice)
        foreach($exercices as $v => $ex)
            if(!array_key_exists($ex->getId(), $modeles))
                $modeles[$ex->getId()] = array();

      return $this->render('AnnotationBundle:Dashboard:Sujet/sujet.html.twig', array(
                  'sujet' => $sujet,
                            'liste'     => $sujet->getListeModeles()->getId(),
                  'group' => $group,
                  'rendus' => $rendus,
                  'exercices' => $exercices,
                  'modeles' => $modeles,
                            'modelesPop'=> $mo_pop,
                  'nbmodeles' => $nbmodeles,
                  'lectureseule' => $sujet->isFrozen(),
                  'h_corriger' => 'active'
      ));
   }

   /**
    * @return GroupService
    */
   private function GroupService() {
      return $this->get('alecsia.GroupService');
   }

   /**
    * @return SubjectService
    */
   private function SubjectService() {
      return $this->get('alecsia.SubjectService');
   }

}
