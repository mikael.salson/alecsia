<?php

namespace Alecsia\AnnotationBundle\Controller\Sujet;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Alecsia\AnnotationBundle\Entity\Modele;
use Alecsia\AnnotationBundle\Controller\Communication\CRUDReponse;

class GererModelesController extends Controller {
   /* =============
     = Mutateurs =
     ============= */

   // Crée un nouveau modèle en fonction des paramètres donné
   // Renvoie un array(idModele,CRUDReponse).
   public static function nouveauModele($controller, $liste_id, $nom, $commentaire, $valeur, $exercice_id) {
      $reponse = new CRUDReponse();
      $em = $controller->getDoctrine()->getManager();

      // Refus si titre vide
      if ($nom == "") {
         $reponse->notification("Impossible d'ajouter le modèle", "Un modèle doit comporter un titre.", true);
         return array("-1", $reponse);
      }

      // Création du modèle
      $obj_liste = $em->getRepository("AnnotationBundle:ListeModeles")->findOneById($liste_id);

      if ($obj_liste == null) {
         $reponse->notification("Impossible d'ajouter le modèle", "Le sujet avec lequel l'associer est introuvable. A-t-il été supprimé ?", true);
         return array("-1", $reponse);
      }

      if ($exercice_id == "-1") {
         $obj_exercice = null;
      } else {
         $obj_exercice = $em->getRepository("AnnotationBundle:Exercice")->findOneById($exercice_id);

         if ($obj_exercice == null) {
            $reponse->notification("Impossible d'ajouter le modèle", "L'exercice précisé est introuvable. A-t-il été supprimé?", true);
            return array("-1", $reponse);
         }
      }

      // Création du modèle
      $new_modele = new Modele();
      $new_modele->setExercice($obj_exercice);
      $new_modele->setNom($nom);
      $new_modele->setListe($obj_liste);
      $new_modele->setCommentaire($commentaire);
      $new_modele->setValeurLitterale($valeur);

      // Persistence
      $em->persist($new_modele);
      $em->flush();

      // Données de rendu
      $data = array('m' => $new_modele);
      $data["lookatme"] = true;
      $html = $controller->renderView('AnnotationBundle:Dashboard:ListeModeles/modele.html.twig', $data);

      // CRUD en guise de réponse
      $reponse->ajout($html);
      return array($new_modele->getId(), $reponse);
   }

   /* ===========
     = Actions =
     =========== */

   // Ajouter une annotation (réclamé avec POST)
   public function ajouterModeleAction() {
      $request = $this->getRequest();

      // Champs formulaire
      $liste_id = $request->request->get('liste_id');
      $nom = $request->request->get('nom');
      $commentaire = $request->request->get('commentaire');
      $valeur = $request->request->get('valeur');
      $exercice_id = $request->request->get('exercice');


      $reponse = self::nouveauModele($this, $liste_id, $nom, $commentaire, $valeur, $exercice_id);
      return new Response($reponse[1]->getJSON());
   }

   // Modifier une annotation (réclamé avec POST)
   public function modifierModeleAction() {
      $reponse = new CRUDReponse();

      $request = $this->getRequest();

      // Champs formulaire
      $modele_id = $request->request->get('modele_id');
      $liste_id = $request->request->get('liste_id');
      $nom = $request->request->get('nom');
      $commentaire = $request->request->get('commentaire');
      $valeur = $request->request->get('valeur');
      $exercice_id = $request->request->get('exercice');

      // Refus si titre vide
      if ($nom == "") {
         $reponse->notification("Impossible de modifier le modèle", "Un modèle doit comporter un titre.", true);
         return new Response($reponse->getJSON());
      }

      // Recup du modèle : envoi d'une erreur si introuvable
      $em = $this->getDoctrine()->getManager();
      $modele_to_modif = $em->getRepository("AnnotationBundle:Modele")->findOneById($modele_id);
      if ($modele_to_modif == null) {
         $reponse->suppr($modele_id);
         $reponse->notification("Impossible de modifier le modèle", "Le modèle n'existe plus.", true);
         return new Response($reponse->getJSON());
      }

      if ($exercice_id == "-1") {
         $obj_exercice = null;
      } else {
         $obj_exercice = $em->getRepository("AnnotationBundle:Exercice")->findOneById($exercice_id);

         if ($obj_exercice == null) {
            $reponse->notification("Impossible de modifier le modèle", "L'exercice précisé est introuvable. A-t-il été supprimé?", true);
            return new Response($reponse->getJSON());
         }
      }

      // Modif du modèle
      $modele_to_modif->setNom($nom);
      $modele_to_modif->setCommentaire($commentaire);
      $modele_to_modif->setValeurLitterale($valeur);
      $modele_to_modif->setExercice($obj_exercice);


      // Persistence
      $em->flush();

      // Données de rendu
      $data = array('m' => $modele_to_modif);
      if ($request->isXmlHttpRequest()) {
         $data["lookatme"] = true;
      }

      $html = $this->renderView('AnnotationBundle:Dashboard:ListeModeles/modele.html.twig', $data);
      $reponse->modif($html);

      $cn = $this->get('annotation.calculnotes');
      $cn->modeleModifie($modele_to_modif);

      return new Response($reponse->getJSON());
   }

   // Supprimer une annotation (réclamé avec POST)
   public function supprimerModeleAction() {
      $reponse = new CRUDReponse();

      $request = $this->getRequest();

      // Champs formulaire
      $modele_id = $request->request->get("modele_id");
      $suppr_annots = $request->request->get("suppr_annots");

      // Recup du modèle à supprimer
      $em = $this->getDoctrine()->getManager();
      $modele_to_delete = $em->getRepository("AnnotationBundle:Modele")->findOneById($modele_id);

      // Récup des rendus concernés par le modèle
      $cn = $this->get('annotation.calculnotes');
      $rendus_to_refresh = $em->getRepository("AnnotationBundle:Rendu")
              ->findByModele($modele_to_delete);

      // Suppression
      if ($modele_to_delete != null) {

         $annots = $em->getRepository("AnnotationBundle:Annotation")
                 ->findByModele($modele_to_delete->getId());

         if ($suppr_annots) {
            $nbannots = count($annots);

            foreach ($annots as $annot) {
               $em->remove($annot);
            }

            if ($nbannots > 0) {
               if ($nbannots == 1) {
                  $reponse->notification("Annotation supprimée", "Une annotation était liée à ce modèle et a été supprimée.");
               } else {
                  $reponse->notification("Annotations supprimées", "$nbannots annotations étaient liées à ce modèle et ont été supprimées.");
               }
            }
         } else {
            foreach ($annots as $annot) {
               $annot->desolidariser();
            }
         }

         $em->remove($modele_to_delete);
         $em->flush();
      }

      $reponse->suppr($modele_id);

      // Mise à jour des notes
      foreach ($rendus_to_refresh as $rendu) {
         $cn->rafraichirNote($rendu);
      }

      // Rendu en guise de réponse
      return new Response($reponse->getJSON());
   }

}
