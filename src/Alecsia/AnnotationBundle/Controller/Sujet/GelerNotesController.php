<?php

namespace Alecsia\AnnotationBundle\Controller\Sujet;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityNotFoundException;
use Alecsia\AnnotationBundle\Entity\Modele;
use Alecsia\AnnotationBundle\Controller\Communication\CRUDReponse;
use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Entity\Exceptions\RenduNonTermineException;

class GelerNotesController extends AlecsiaController {
   /* =============
     = Mutateurs =
     ============= */

   // Recalcule toutes les notes
   // throw EntityNotFoundException
   public static function recalculerNotesSujet($controller, $sujet_id) {
      $em = $controller->getDoctrine()->getManager();

      // Recupération du sujet
      $sujet = $em->getRepository('AnnotationBundle:Sujet')
              ->findOneById($sujet_id);
      if (is_null($sujet)) {
         throw new EntityNotFoundException("Sujet introuvable");
      }

      // Recalcul de toutes les notes
      $cn = $controller->get('annotation.calculnotes');
      $cn->rafraichirNotes($sujet);

      return true;
   }

   // Arrondit toutes les notes
   public static function arrondirNotesSujet($controller, $sujet_id, $precision, $methode) {
      $em = $controller->getDoctrine()->getManager();
      $an = $controller->get('annotation.arrondirnotes');

      // Recupération du sujet
      $sujet = $em->getRepository('AnnotationBundle:Sujet')
              ->findOneById($sujet_id);
      if (is_null($sujet)) {
         throw new EntityNotFoundException("Sujet introuvable");
      }

      foreach ($sujet->getRendus() as $rendu) {
         $anciennenote = $rendu->getNote();
         $nouvellenote = $an->arrondirNote($anciennenote, $precision, $methode);
         $annotajust = $rendu->ajusterNote($nouvellenote);

         if (!is_null($annotajust)) {
            $annotajust->setNom("Arrondissement de la note");
            $em->persist($annotajust);
         }
      }

      $em->flush();

      return true;
   }

   // Gèle le sujet.
   // On suppose les notes bien calculées.
   // Retourne un booleen correspondant à la réussite de l'opération
   // throws EntityNotFoundException
   public static function gelerSujet($controller, $sujet_id) {

      $em = $controller->getDoctrine()->getManager();

      // Recupération du sujet
      $sujetrepo = $em->getRepository('AnnotationBundle:Sujet');
      $sujet = $sujetrepo->findOneById($sujet_id);
      if (is_null($sujet)) {
         throw new EntityNotFoundException("Sujet introuvable");
      }

      // Gel du sujet
      try {
         $sujet->geler();
      } catch (RenduNonTermineException $e) {
         $controller->setError('Impossible de geler les notes', 'Certains rendus ne sont pas marqués comme terminés.');
         return false;
      }

      // Calcul de la moyenne
      $moyenne = round($sujetrepo->getMoyenne($sujet, null), 2);
      $sujet->setMoyenne($moyenne);

      $em->flush();

      return true;
   }

   /* ===========
     = Actions =
     =========== */

   // Renvoie simplement un booléen disant si oui ou non tous les rendus sont traités
   public function rendusTousTerminesAction($sujet_id) {
      $em = $this->getDoctrine()->getManager();
      $sujet = $em->getRepository('AnnotationBundle:Sujet')
              ->findOneById($sujet_id);

      if (is_null($sujet)) {
         throw new EntityNotFoundException("Sujet introuvable");
      }

      if ($sujet->tousTermines()) {
         return new Response("1");
      } else {
         return new Response("0");
      }
   }

   // Terminer tous les rendus (ie. ils sont gelés)
   public function terminerToutAction($sujet_id, $group_id) {
      // On recalcule toutes les notes
      self::recalculerNotesSujet($this, $sujet_id);

      // On termine tout.
      $em = $this->getDoctrine()->getManager();
      $sujet = $em->getRepository('AnnotationBundle:Sujet')
              ->findOneById($sujet_id);

      if (is_null($sujet)) {
         throw new EntityNotFoundException("Sujet introuvable");
      }

      $sujet->toutTerminer();
      $em->flush();

      $this->setSuccess('Rendus marqués comme terminés');
      return $this->redirect(
                      $this->generateUrl('AnnotationBundle_sujet', array("sujet_id" => $sujet_id,
                          'group_id' => $this->GroupService()->getDefaultGroupFrom($sujet->getUE(), $this->getUser())->getId())));
   }

   // Effectue le traitement du gel puis redirige vers la page du sujet
   public function gelerSujetAction() {

      $r = $this->getRequest();
      $s = $this->get('session');

      // Recup des arguments
      $sujet_id = $r->get("sujet_id");
      $group_id = $r->get("group_id");
      $geler = $r->get("geler");
      $methode_arrondi = $r->get("methode");
      $precision = $r->get("precision");
      $methodes_possibles = array("arrondirSup", "arrondirInf", "arrondirProche");
      $precisions_possibles = array("quartsProches", "demisProches", "unitesProches");

      try {
         if ($geler) {

            // Recalcul de toutes les notes
            self::recalculerNotesSujet($this, $sujet_id);

            // Arrondissement des notes
            if (in_array($methode_arrondi, $methodes_possibles) &&
                    in_array($precision, $precisions_possibles)) {
               self::arrondirNotesSujet($this, $sujet_id, $precision, $methode_arrondi);
            }

            // Gel
            $reussi = self::gelerSujet($this, $sujet_id);

            if ($reussi) {
               $this->setSuccess('Notes gelées');
            }
         }

         return $this->redirect($this->generateUrl('AnnotationBundle_sujet', array("sujet_id" => $sujet_id, 'group_id' => $group_id)));
      } catch (Exception $e) {
         $this->setError('Impossible de geler les notes', $e->getMessage());

         $home_url = $this->generateUrl('AnnotationBundle_home');
         return $this->redirect($home_url);
      }
   }

   private function GroupService() {
      return $this->get('alecsia.GroupService');
   }

}
