<?php

namespace Alecsia\AnnotationBundle\Controller\Sujet;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\File\File;
use Alecsia\AnnotationBundle\Entity\Exceptions\ArchiveException;
use Alecsia\AnnotationBundle\Entity\Fichier;
use Alecsia\AnnotationBundle\Entity\Rendu;
use Alecsia\AnnotationBundle\Entity\Group;
use Alecsia\AnnotationBundle\Entity\Sujet;
use Alecsia\AnnotationBundle\Service\AlecsiaUtils;

class FileImporter extends ContainerAware {

   private $doctrine = null;
   private $base_folder = null;
   // Types MIME correspondant à des archives
   static private $are_archives = array(
       "application/x-tar",
       "application/zip",
       "application/x-gzip",
       "application/x-bzip2",
       "application/x-gtar",
       "application/x-rar",
       'zip', 'tar.gz', 'tar.bz2', 'bz2', 'gz', 'rar'
   );

   function __construct($container, $base_folder) {
      if ($container != null) {
         $this->setContainer($container);
         $this->doctrine = $this->container->get('doctrine');
      }
      $this->base_folder = $base_folder;
   }

   public function importArchive(Sujet $sujet, Group $group, $file) {
      // Le fichier DOIT être une archive
      if (!($this->isAnArchive($file))) {
         return $this->importRendu($sujet, $group, $file);
      }

      // Extraction de l'archive
      $filename = $file->getClientOriginalName();
      $dossier = tempnam($this->base_folder, "temp_");
      unlink($dossier); // We just want a temporary name, not a temp file
      $dossier = basename($dossier);
      $this->importFile($file, $filename, $dossier);

      // Import de chaque archive
      $path = $this->base_folder . "/" . $dossier;

      $errors = $this->importArchiveInFolders($sujet, $group, $path);

      // Suppression du dossier temporaire
      system('rm -rf "$path"');

      if (!empty($errors)) {
         throw new ArchiveException($errors);
      }
   }

   // Importe le rendu passé en paramètre
   public function importRendu(Sujet $sujet, Group $group, $file) {
      $em = $this->doctrine->getManager();

      // Création du rendu
      if (is_uploaded_file($file)) {
         $filename = $file->getClientOriginalName();
      } else {
         $filename = $file->getFilename();
      }

      $new_rendu = new Rendu($filename, $sujet, $group);


      // Map des étudiants
      $em = $this->doctrine->getManager();
      $repo = $this->doctrine->getRepository('AnnotationBundle:Student');
      $potentialStudents = $repo->findByWork($new_rendu);

      $workService = $this->container->get("alecsia.workService");
      try {
         $workService->addStudentsToWorkIfNecessaryWithoutFlush($new_rendu, $potentialStudents);
      } catch (\Exception $e) {
         // Nevermind in this case : student has surely already given a work in this assignment.
      }

      $em->persist($new_rendu);

      try {
         // Import des fichiers dans le rendu
         $dossier = $this->importFile($file, $filename);
         $new_rendu->setDossier($dossier);
         $this->indexeDossier($dossier, $new_rendu);
      } catch (\Exception $e) {
         /* When having an exception we must remove the persisted entity
          * so that it is not in an unstable stage. */
         $em->remove($new_rendu);
         $em->flush();
         throw $e;
      }

      $em->flush();
      return $dossier;
   }

   /* ====================================
     = Etapes de l'import d'une archive =
     ==================================== */

   // 1 - Importe les fichiers de $file dans les dossiers de l'appli
   // Si c'est une archive, la décompresse.
   // @return basename of created folder.
   private function importFile($file, $filename, $newFolder = null) {

      // 0 - Purge de $filename des caractères spéciaux
        $filename = AlecsiaUtils::cleanFileName($filename);

      // 1 - Création du dossier
      $newFolder = $newFolder ? : $this->getNextFolder();
      $all_path = $this->base_folder . '/' . $newFolder;
      system('mkdir "' . $all_path . '"');

      // 2 - Remplissage du dossier
      $file = $file->move($all_path, $filename);

      // 3 - Si c'est une archive on la décompresse
      if ($this->isAnArchive($file)) {
         $this->unarchive($file);
      }

      return $newFolder;
   }

   // 2 - Indexe les fichiers du dossier de chemin passé en paramètre en BDD.
   private function indexeDossier($dossier, $rendu) {
      $em = $this->doctrine->getManager();
      $fg = $this->container->get('annotation.fileguess');

      $path = $this->base_folder . '/' . $dossier;
      $files = \opendir($path);
      while ($entry = readdir($files)) {
         if ($entry != '.' && $entry != '..') {
                if(is_dir($path.'/'.$entry)) {
                    if (! mb_detect_encoding($entry)) {
                        $new_entry = preg_replace('/[[:^print:]]/', '_', $entry);
                        rename($path.'/'.$entry, $path.'/'.$new_entry);
                        $entry = $new_entry;
                    }
                    $this->indexeDossier($dossier.'/'.$entry,$rendu);
                }
                else if (is_file($path.'/'.$entry)) {
                    $entry = AlecsiaUtils::cleanRename($dossier,$entry, $this->base_folder);
                    $user = $this->container->get('security.context')->getToken()->getUser();
                    $language = $fg->guessLanguage($path,$entry, $user);
                    $fichier = new Fichier($dossier,$entry,$rendu,$language ,$fg->guessExclus($path,$entry, $language,$user));
               $em->persist($fichier);
            }
         }
      }
      $em->flush();
      \closedir($files);
   }

   /**
    * Import archives recursively (in all subdirectories)
    */
   private function importArchiveInFolders(Sujet $sujet, Group $group, $path) {
      $files = \opendir($path);
      $errors = array();
      while ($entry = readdir($files)) {
         $file_path = $path . '/' . $entry;
         if (!is_dir($file_path)) {
            $file = new File($file_path);
            try {
               $this->importRendu($sujet, $group, $file);
            } catch (\Exception $e) {
               $errors[] = array('path' => basename($file_path),
                   'error' => $e->getMessage());
            }
         } else {
             if ($entry != '.' && $entry != '..')
                 $errors = $this->importArchiveInFolders($sujet, $group, $file_path);
         }
      }
      return $errors;
   }

   /* ===========
     = Helpers =
     =========== */

   // D'un fichier passé en paramètre, retourne s'il correspond ou non
   // à une archive à décompacter
   private function isAnArchive($file) {
      $fileMimeType = $file->getMimeType();
      if (! \in_array($fileMimeType, self::$are_archives)) {
          return \in_array($file->getExtension(), self::$are_archives);
      }
      return true;
   }

   // Retourne le nom du futur dossier à créer
   private function getNextFolder() {

      // On tente d'ouvrir le cache du nom du futur folder
      $next = $this->base_folder . "/nextfolder";
      if (is_file($next)) {
         $fichier = fopen($next, "r+");
         $nextfolder = intval(fgets($fichier));
      } else {
         $fichier = fopen($next, "a");

         // On recalcule le nom du futur folder
         $dirs = scandir($this->base_folder);
         $nextfolder = -1;
         foreach ($dirs as $dir) {
            if (preg_match("/[0-9]+/", $dir)) {
               $val = intval($dir);
               if ($val > $nextfolder) {
                  $nextfolder = $val;
               }
            }
         }
      }

      fseek($fichier, 0);
      fputs($fichier, $nextfolder + 1);
      fclose($fichier);

      return $nextfolder + 1;
   }

   // Décompresse une archive
   private function unarchive($file) {
      $filepath = $file->getPathname();
      $folderpath = $file->getPath();

      $lang = '';
      if (isset($_SERVER['LANG'])) {
         $lang = 'LANG=' . $_SERVER['LANG'];
      }
      if ($file->getMimeType() != "application/x-rar"
      && $file->getExtension() != 'rar') {
         system($lang . ' bsdtar xf "' . $filepath . '" -C "' . $folderpath . '"', $retval);
         system('chmod -R 755 "' . $folderpath . '"');
         system('rm -f "' . $filepath . '"');
      } else {
         system($lang . ' unrar x -y "' . $filepath . '" "' . $folderpath . '"', $retval);
         system('chmod -R 755 "' . $folderpath . '"');
         system('rm -f "' . $filepath . '"');
      }
   }


    /*===============
     = Suppression =
     =============== */

   // Suppression recursive dossier
   private static function rrmdir($dir) {
      $fp = opendir($dir);
      if ($fp) {
         while ($f = readdir($fp)) {
            $file = $dir . "/" . $f;
            if ($f == "." || $f == "..") {
               continue;
            } else if (is_dir($file) && !is_link($file)) {
               self :: rrmdir($file);
            } else {
               unlink($file);
            }
         }
         closedir($fp);
         rmdir($dir);
      }
   }

   public function supprimerDossier($dossier) {
      if (!empty($dossier)) {
         $path = $this->base_folder . "/" . $dossier;
         self :: rrmdir($path);
      }
   }

}
