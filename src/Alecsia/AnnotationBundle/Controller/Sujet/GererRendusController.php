<?php

namespace Alecsia\AnnotationBundle\Controller\Sujet;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityNotFoundException;
use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Lib\AlecsiaGlobals;

class GererRendusController extends AlecsiaController {
   /* =============
     = Actions   =
     ============= */

   // A partir d'une liste d'ids retourne les rendus
   private function getRendus($rendus_id) {
      $em = $this->getDoctrine()->getManager();
      $repo = $em->getRepository("AnnotationBundle:Rendu");
      $rendus = array();

      foreach ($rendus_id as $rendu_id) {
         $rendu = $repo->findOneById($rendu_id);
         if ($rendu != null) {
            $rendus[] = $rendu;
         }
      }

      return $rendus;
   }

   public function envoyerMailRendus($rendus) {
      $r = $this->getRequest();
      $em = $this->getDoctrine()->getManager();
      $to = $r->get('to');
      $envoi_rendu = $r->get('envoi_rendu');

      $options = array('subject' => $r->get('subject'),
          'body' => $r->get('body'));
      if (empty($envoi_rendu)) {
         $options['empeche_rendu'] = true;
      }

      $success = array();
      $fails = array();

      foreach ($rendus as $rendu) {
         $mails = array();
         foreach ($rendu->getEtudiants() as $etu) {
            $mails[] = $etu->getMail();
         }
         $options['to'] = join(', ', $mails) . ', ' . $to;
         $options['rendu'] = $rendu->getId();

         $response = $this->forward('AnnotationBundle:Export\Default:sendEmail', $options);
         $json = json_decode($response->getContent());

         if (empty($json) || $json->{'notification'}->{'erreur'}) {
            $fails[] = $options['to'];
         } else {
            $success[] = $options['to'];
         }
      }
      return array('failures' => $fails, 'success' => $success);
   }

   public static function supprimerRendu($em, $rendu) {

      // Suppresion des dossiers
      $fi = new FileImporter(null, AlecsiaGlobals::getUploadDir());

      $dossier_a_supprimer = $rendu->getDossier();
      if (!empty($dossier_a_supprimer)) {
         $fi->supprimerDossier($dossier_a_supprimer);
      }

      // Désindexage des fichiers
      $fichiers = $rendu->getFichiers();
      foreach ($fichiers as $fichier) {
         $em->remove($fichier);
      }

      // Suppressions des annotations
      $annnots = $rendu->getAnnotations();
      foreach ($annnots as $annot) {
         $em->remove($annot);
      }

      $em->remove($rendu);
   }

   // Supprime les rendus donnés
   private function supprimerRendus($rendus) {
      $em = $this->getDoctrine()->getManager();

      foreach ($rendus as $rendu) {
         self::supprimerRendu($em, $rendu);
      }
   }

   // Marque tout comme terminé (ou non)
   private function setTermine($rendus, $fini = true) {
      $em = $this->getDoctrine()->getManager();
      $cn = $this->get('annotation.calculnotes');

      foreach ($rendus as $rendu) {
         $cn->rafraichirNote($rendu);
         $rendu->setTermine($fini);
      }
   }

   /* ================
     = Multiplexeur =
     ================ */

   // Effectue un traitement sur des rendus cochés
   public function effectuerRendusAction() {
      $r = $this->getRequest();
      $em = $this->getDoctrine()->getManager();

      // Recup des rendus
      $rendus_id = $r->get("rendu");
      $rendus_id = array_keys($rendus_id);
      $rendus = $this->getRendus($rendus_id);
      $sujet_id = $r->get("sujet");
      $group_id = $r->get("group_id");

      // Recup de l'action
      $action = $r->get("action");

      // Action
      switch ($action) {
         case "supprimer":
            $this->supprimerRendus($rendus);
            $this->setSuccess('Rendu(s) supprimé(s)');
            break;
         case "marquerfini":
            $this->setTermine($rendus, true);
            $this->setSuccess('Rendu(s) marqué(s) comme terminé(s)');
            break;
         case "marquernonfini":
            $this->setTermine($rendus, false);
            $this->setSuccess('Rendu(s) marqué(s) comme en cours de correction');
            break;
         case "envoyermailrendus":
            $results = $this->envoyerMailRendus($rendus);
            if (count($results['failures'])) {
               $this->setError('Problème dans l\'envoi de mails', sprintf("%d mails n'ont pas été correctement envoyés. Ceux pour %s\n", count($results['failures']), join(' et ', $results['failures'])));
            } else {
               $this->setSuccess(sprintf("%d mails envoyés avec succès", count($results['success'])));
            }
            break;
      }

      $em->flush();


      return $this->redirect($this->generateUrl('AnnotationBundle_sujet', array("sujet_id" => $sujet_id, 'group_id' => $group_id)));
   }

}
