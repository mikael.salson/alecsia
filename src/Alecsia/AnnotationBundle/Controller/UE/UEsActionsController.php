<?php

namespace Alecsia\AnnotationBundle\Controller\UE;

use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Lib\AlecsiaGlobals;
use Alecsia\AnnotationBundle\Service\EntityServices\UEService;
use Alecsia\AnnotationBundle\Service\EntityServices\StudentService;
use Alecsia\AnnotationBundle\Entity\UE;
use Symfony\Component\Security\Acl\Exception\Exception;

/**
 * Author: Ludovic Loridan
 *
 * UEsActionsController.php
 * Manage all the requests that do modifications to the Ues, by doing the actions
 * and doing the correct redirections & flash messages.
 */
class UEsActionsController extends AlecsiaController {
   /* ============================== */
   /* Actions CRUD                   */
   /* ============================== */

   public function addUEAction() {
      $params = $this->getPostParameters();
      if ($params["choix_methode"] == "creer_ue") {
         return $this->addUEFromScratchAction();
      } elseif ($params["choix_methode"] == "copier_ue") {
         return $this->addUEFromCopyAction();
      } else {
         $this->createNotFoundException("Method not found");
      }
   }

   public function addUEFromCopyAction() {
      $targetUrl = $this->generateUrl('AlecsiaShowActiveUEs');

      $params = $this->getPostParameters();
      $ue = $this->UEService()->get($params["ue_a_copier"]);
      if (is_null($ue))
         return $this->redirectAndSendError($targetUrl, "Impossible de copier l'UE", "L'UE à copier n'a pas été trouvée");
      if (empty($params['year_copie'])) {
         return $this->redirectAndSendError($targetUrl, "Impossible de créer l'UE", "Il faut préciser une année");
      }

      $newUE = $ue->cloner();
      $newUE->setYear($params['year_copie']);
      $ueFromService = $this->UEService()->add($newUE, $this->getUser());
      $this->setFlashMessageFromReturnedEntity($ueFromService, "UE copiée", null);
      return $this->redirect($targetUrl);
   }

   public function addUEFromScratchAction() {
      $targetUrl = $this->generateUrl('AlecsiaShowActiveUEs');

      $params = $this->getPostParameters();
      $ue = UE::UEFromArray($params, $this->getUser());
      $ueFromService = $this->UEService()->add($ue, $this->getUser());
      $this->setFlashMessageFromReturnedEntity($ueFromService, "UE Créée", "Impossible de créer l'UE", "Tous les champs doivent être remplis.");

      return $this->redirect($targetUrl);
   }

   public function updateUEAction() {

      $targetURL = $this->generateUrl('AlecsiaShowActiveUEs');

      $params = $this->getPostParameters();
      $ue = $this->UEService()->get($params["ue_id"]);
      if (is_null($ue))
         return $this->redirectAndSendError($targetURL, "Impossible de modifier l'UE", "Elle n'a pas été trouvée");

      $ue->setParamsFromArray($params);
      $ueFromService = $this->UEService()->update($ue, $this->getUser());

      $this->setFlashMessageFromReturnedEntity($ueFromService, "UE modifiée", "Impossible de créer l'UE", "Tous les champs doivent être remplis.");

      return $this->redirect($targetURL);
   }

   public function removeUEAction($ue_id) {
      $targetURL = $this->generateUrl('AlecsiaShowActiveUEs');

      $ue = $this->UEService()->get($ue_id, $this->getUser());
      if (is_null($ue))
         return $this->redirect($targetURL);

      $targetURL = $this->getTargetURLFromUE($ue);
      $this->UEService()->remove($ue, $this->getUser());
      $this->setSuccess("UE supprimée");

      return $this->redirect($targetURL);
   }

   /* =============================== */
   /* Target URL                      */
   /* =============================== */

   // Returns the "active UE" page if the ue is active, else the "archived ue"
   private function getTargetURLFromUE(UE $ue) {
      if ($ue->getArchived()) {
         return $this->generateUrl('AlecsiaShowArchivedUEs');
      } else {
         return $this->generateUrl('AlecsiaShowActiveUEs');
      }
   }

   /* ============================== */
   /* Archivage                      */
   /* ============================== */

   public function archiveUEAction($ue_id) {
      return $this->setArchiveUEAction($ue_id, true, "UE archivée", "Impossible d'archiver l'UE");
   }

   public function unarchiveUEAction($ue_id) {
      return $this->setArchiveUEAction($ue_id, false, "UE désarchivée", "Impossible de désarchiver l'UE");
   }

   private function setArchiveUEAction($ue_id, $archiveState, $success, $error) {

      $targetURL = $this->generateUrl('AlecsiaShowActiveUEs');

      $ue = $this->UEService()->get($ue_id, $this->getUser());
      if (is_null($ue))
         return $this->redirectAndSendError($targetURL, $error, "Elle n'a pas été trouvée");

      $ue->setArchived($archiveState);
      $ueFromService = $this->UEService()->update($ue, $this->getUser());

      $this->setFlashMessageFromReturnedEntity($ueFromService, $success);
      return $this->redirect($targetURL);
   }

    /* ============================== */
    /* Modèles populaires de l'UE     */
    /* ============================== */

    public function listPopularModelsUEAction($ue_id){
        $em = $this ->getDoctrine() -> getManager();

        $targetURL = $this->generateUrl("AlecsiaShowActiveUEs");

        $error = "Impossible d'accéder aux modèles";

        if(is_null($ue_id))
            return $this ->redirectAndSendError($targetURL, $error, "L'UE n'a pas été trouvée");

        $liste_globale = $this->ModelListService()->getGlobalList($this->getUser());
        $ue = $em -> getRepository("AnnotationBundle:UE")
                ->findOneById($ue_id);

        $nbmodeles = 0;

        $exercices = array();

        $sujets = $em->getRepository("AnnotationBundle:Sujet")
                    ->findBy(array("UE" => $ue));

        $getmpue = $em -> getRepository("AnnotationBundle:Modele")
                    ->getMostPopularByUE($ue_id, AlecsiaGlobals::$maxModelPopular);

        $nbmodeles += count($getmpue);

        $modeles = $em->getRepository('AnnotationBundle:Modele')
                    ->orderPopularModelsByListe($getmpue);

        //On met les modèles Bonus/Malus à l'index 0 dans le tableau
        $modeles[0] = array();
        if(!empty($modeles[$liste_globale->getId()])){
            $modeles[0] = $modeles[$liste_globale->getId()];
            unset($modeles[$liste_globale->getId()]);
        }

        foreach($sujets as $v)
            if(!array_key_exists($v->getListeModeles()->getId(), $modeles))
                $modeles[$v->getListeModeles()->getId()] = array();

        $modelesPop = $em->getRepository('AnnotationBundle:Modele')
                        ->indexPopularModels($getmpue);

        return $this->render('AnnotationBundle:Dashboard:Preferences/gestionModeles.html.twig',
                            array("listemodeles" => $liste_globale,
                                "modeles" => $modeles,
                                "exercices" => $exercices,
                                "sujets" => $sujets,
                                "nbmodeles" => $nbmodeles,
                                "modelesPop" => $modelesPop,
                                "ue" => $ue,
                                "modeUE" => true));
    }


   /* ================================== */
   /* Link to services                   */
   /* ================================== */

   /**
    * @return UEService
    */
   private function UEService() {
      return $this->get('alecsia.UEService');
   }

   /**
    * @return studentService
    */
   private function studentService() {
      return $this->get('alecsia.studentService');
   }

    /**
     * @return modelListService
     */
    private function ModelListService() {
        return $this->get('alecsia.modelListService');
    }
}
