<?php

namespace Alecsia\AnnotationBundle\Controller\UE;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Alecsia\AnnotationBundle\Service\EntityServices\UEService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Alecsia\AnnotationBundle\Controller\AlecsiaController;

/**
 * Author: Ludovic Loridan
 *
 * UEsListController.php
 * Manages the different lists of UE that Alecsia can show.
 */
class UEsListController extends AlecsiaController {
   /* ============================== */
   /* Actions                        */
   /* ============================== */

   /**
    * @Template("AnnotationBundle:Screenfuls:activeUEs.html.twig")
    */
   public function showActiveUEsAction() {
      return array(
          'ues' => $this->getListOfActiveUsersUEs(),
          'archivedUes' => $this->getListOfArchivedUsersUEs(),
          'h_corriger' => 'active'
      );
   }

   /**
    * @Template("AnnotationBundle:Screenfuls:archivedUEs.html.twig")
    */
   public function showArchivedUEsAction() {
      return array(
          'ues' => $this->getListOfArchivedUsersUEs(),
          'h_archives' => 'active'
      );
   }

   /* ============================== */
   /* Shortcuts                      */
   /* ============================== */

   private function getListOfArchivedUsersUEs() {
      $teacher = $this->getUser();
      $archivedUEs = $this->UEService()->getListOfArchivedUEs($teacher);
      return $archivedUEs;
   }

   private function getListOfActiveUsersUEs() {
      $teacher = $this->getUser();
      $activeUEs = $this->UEService()->getListOfActiveUEs($teacher);
      return $activeUEs;
   }

   /* ================================== */
   /* Link to services                   */
   /* ================================== */

   /**
    * @return UEService
    */
   private function UEService() {
      return $this->get('alecsia.UEService');
   }

}
