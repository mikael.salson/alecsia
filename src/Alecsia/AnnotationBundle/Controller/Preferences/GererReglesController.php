<?php

namespace Alecsia\AnnotationBundle\Controller\Preferences;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityNotFoundException;
use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Entity\RegleExclusionDossier;
use Alecsia\AnnotationBundle\Entity\RegleExclusionFichier;
use Alecsia\AnnotationBundle\Entity\RegleTypeFichier;

class GererReglesController extends AlecsiaController {
   /* ===========================
     = Création / Modification =
     =========================== */

   public function remplirRegle($regle) {
      $em = $this->getDoctrine()->getManager();
      $r = $this->getRequest();

      $regex = $r->get("regle");
      $regle->setRegex($regex);
      $regle->setUser($this->getUser());

      return $regle;
   }

   public function remplirRegleLangage($regle) {
      $em = $this->getDoctrine()->getManager();
      $r = $this->getRequest();

      $regex = $r->get("regle");
      $regle->setRegex($regex);
      $regle->setUser($this->getUser());

      // Recup langage
      $lang = $em->getRepository("AnnotationBundle:Langage")
              ->findOneById($r->get("langage"));

      if (empty($lang)) {
         $this->setError("Impossible de modifier la règle", "Le langage associé n'a pu être trouvé.");

         $gerer_url = $this->generateUrl('AnnotationBundle_gererRegles');
         return $this->redirect($gerer_url);
      }

      $regle->setLangage($lang);


      return $regle;
   }

   public function creerModifRegleAction() {
      $em = $this->getDoctrine()->getManager();
      $r = $this->getRequest();
      $s = $this->get("session");
      $gerer_url = $this->generateUrl('AnnotationBundle_gererRegles');

      // Validation
      $repo_txt = $r->get("regle_repo");
      if ($repo_txt != "RegleExclusionFichier" && $repo_txt != "RegleExclusionDossier" && $repo_txt != "RegleTypeFichier") {
         die("Repository imprévu");
      }

      $regle_txt = $r->get("regle");
      if (empty($regle_txt)) {
         $this->setError("Impossible de modifier la règle", "Une règle ne peut être vide."
         );

         return $this->redirect($gerer_url);
      }

      $repo = $em->getRepository("AnnotationBundle:" . $repo_txt);

      // Recup de l'entité à modifier
      $regle_id = $r->get("regle_id");
      $regle = $repo->findOneById($regle_id);
      $creation = false;

      if ($regle == null) {
         $regle_class = 'Alecsia\AnnotationBundle\Entity\\' . $repo_txt;
         $regle = new $regle_class();
         $creation = true;
      }

      // Modif de l'entité
      if ($repo_txt == "RegleTypeFichier") {
         $this->remplirRegleLangage($regle);
      } else {
         $this->remplirRegle($regle);
      }

      if ($creation) {
         $em->persist($regle);
         $this->setSuccess("Règle créée");
      } else {
         $this->setSuccess("Règle modifiée");
      }

      $em->flush();


      return $this->redirect($gerer_url);
   }

   /* ===============
     = Suppression =
     =============== */

   private function getReglesFromIds($regles_id, $repo) {
      $em = $this->getDoctrine()->getManager();
      $repo = $em->getRepository("AnnotationBundle:" . $repo);
      $regles = array();

      foreach ($regles_id as $regle_id) {
         $regle = $repo->findOneById($regle_id);
         if ($regle != null) {
            $regles[] = $regle;
         }
      }

      return $regles;
   }

   public static function supprimerRegle($em, $regle) {
      $em->remove($regle);
   }

   public function supprimerReglesAction() {
      $em = $this->getDoctrine()->getManager();
      $r = $this->getRequest();
      $s = $this->get("session");

      // Validation
      $repo = $r->get("repository");
      if ($repo != "RegleExclusionFichier" && $repo != "RegleExclusionDossier" && $repo != "RegleTypeFichier") {
         die("Repository imprévu");
      }

      // Recup des règles
      $regles_id = $r->get("regle");
      $regles_id = array_keys($regles_id);
      $regles = $this->getReglesFromIds($regles_id, $repo);

      // Suppression des règles
      foreach ($regles as $regle) {
         self::supprimerRegle($em, $regle);
      }

      $em->flush();
      $this->setSuccess('Règle(s) supprimée(s)');

      $gerer_url = $this->generateUrl('AnnotationBundle_gererRegles');
      return $this->redirect($gerer_url);
   }

   public function supprimerRegleAction($repo, $regle_id) {
      $em = $this->getDoctrine()->getManager();
      $s = $this->get("session");

      $gerer_url = $this->generateUrl('AnnotationBundle_gererRegles');

      // Validation
      if ($repo != "RegleExclusionFichier" && $repo != "RegleExclusionDossier" && $repo != "RegleTypeFichier") {
         die("Repository imprévu");
      }

      // Recup de la règle
      $repo_obj = $em->getRepository("AnnotationBundle:" . $repo);
      $regle = $repo_obj->findOneById($regle_id);

      if ($regle == null) {
         $s->getFlashBag()->set("notice", array(
             "titre" => "Aucune règle n'a été supprimée",
             "contenu" => "La règle que vous souhaitiez supprimer était déjà inexistante."
         ));

         return $this->redirect($gerer_url);
      }

      // Suppression de la règle
      self::supprimerRegle($em, $regle);

      $em->flush();
      $this->setSuccess('Règle supprimée');


      return $this->redirect($gerer_url);
   }

   /* ========
     = Page =
     ======== */

   public function indexAction() {

      $em = $this->getDoctrine()->getManager();

      $reporef = $em->getRepository("AnnotationBundle:RegleExclusionFichier");
      $repored = $em->getRepository("AnnotationBundle:RegleExclusionDossier");
      $reportf = $em->getRepository("AnnotationBundle:RegleTypeFichier");
      $repolang = $em->getRepository("AnnotationBundle:Langage");

      $ref = $reporef->findByUser($this->getUser()->getId());
      $red = $repored->findByUser($this->getUser()->getId());
      $rtf = $reportf->findByUser($this->getUser()->getId());
      $lang = $repolang->findBy(array(), array("nom" => "asc"));

      return $this->render('AnnotationBundle:Dashboard:Preferences\gestionRegles.html.twig', array(
                  "ref" => $ref,
                  "red" => $red,
                  "rtf" => $rtf,
                  "langages" => $lang
      ));
   }

}
