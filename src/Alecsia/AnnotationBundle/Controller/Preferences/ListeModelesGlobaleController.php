<?php

namespace Alecsia\AnnotationBundle\Controller\Preferences;

use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Alecsia\AnnotationBundle\Entity\ListeModeles;
use Alecsia\AnnotationBundle\Entity\Preference;
use Alecsia\AnnotationBundle\Entity\Modele;

class ListeModelesGlobaleController extends AlecsiaController {

   public function gererListeAction() {

      $em = $this->getDoctrine()->getManager();
      $liste_globale = $this->ModelListService()->getGlobalList($this->getUser());

      // Récupération des données nécessaires à la vue
      $exercices = array();

      $modeles = array();
      $modeles[0] = $em->getRepository("AnnotationBundle:Modele")
              ->findBy(array("liste" => $liste_globale->getId(), "exercice" => null));

      $nbmodeles = count($modeles[0]);


      return $this->render('AnnotationBundle:Dashboard:Preferences\gestionModeles.html.twig', array(
                  'listemodeles' => $liste_globale,
                  'exercices' => $exercices,
                  'modeles' => $modeles,
                  'nbmodeles' => $nbmodeles
      ));
   }

   private function ModelListService() {
      return $this->get('alecsia.modelListService');
   }

}
