<?php
namespace Alecsia\AnnotationBundle\Controller\EventListener;
use \Symfony\Component\HttpKernel\Exception\FlattenException;
class ExceptionListener{
  /**
   * @var \Symfony\Component\HttpKernel\Log\LoggerInterface
   */
  private $logger =null;

  /**
   * @param null|\Symfony\Component\HttpKernel\Log\LoggerInterface $logger
   */
  public function __construct(\Symfony\Component\HttpKernel\Log\LoggerInterface $logger = null)
  {
    $this->logger = $logger;
  }

  /**
   * @param \Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event
   */
  public function onKernelException(\Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event)
  {
    if($this->logger === null)
      return;
    $exception = $event->getException();
    $flattenException = FlattenException::create($exception);
    $this->logger->err('Stack trace');
    foreach ($flattenException->getTrace() as $trace) {
      $traceMessage = sprintf('  at %s line %s', $trace['file'], $trace['line']);
      $this->logger->err($traceMessage);
    }
  }
}
?>
