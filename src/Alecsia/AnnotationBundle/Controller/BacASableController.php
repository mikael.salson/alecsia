<?php

namespace Alecsia\AnnotationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;

class BacASableController extends Controller {

   public function indexAction() {

      return $this->render('AnnotationBundle:BacASable:bacasable.html.twig');
   }

}
