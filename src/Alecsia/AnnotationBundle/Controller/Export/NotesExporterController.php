<?php

namespace Alecsia\AnnotationBundle\Controller\Export;

use Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle;
use Symfony\Component\HttpFoundation\Response;
use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Controller\Export\HTMLExport;
use Alecsia\AnnotationBundle\Controller\Communication\CRUDReponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

class NotesExporterController extends AlecsiaController {

   private function createCSVFile($twigFile, $csvFilename, $data) {
      $response = $this->render('AnnotationBundle:Notes:' . $twigFile, $data);

      $response->headers->set('Content-Type', 'text/csv');

      $response->headers->set('Content-Disposition', 'attachment; filename="' . $csvFilename);

      return $response;
   }

   private function noNoteToExport() {
      $this->setError("Impossible d'exporter les notes", "Il n'y a aucune note à exporter.");
      return new RedirectResponse($this->getRequest()->headers->get('referer'));
   }

   public function createCSVFileForSujetGroupAction($sujet_id, $group_id) {
      $em = $this->getDoctrine()->getManager();
      $request = $this->getRequest();

      $sujet_obj = $em->getRepository('AnnotationBundle:Sujet')->findOneById($sujet_id);
      $group = $em->getRepository('AnnotationBundle:Group')->findOneById($group_id);
      if (!$sujet_obj || !$sujet_obj->isFrozen()) {
         $this->setError("Impossible d'exporter les notes", "Le rendu n'a pas été trouvé ou n'a pas été gelé");
         return new RedirectResponse($this->getRequest()->headers->get('referer'));
      }

      $notes = $sujet_obj->getNotes(array($group_id));

      $shortSujetName = preg_split('/[\s\/|!]+/', $sujet_obj->getNom());
      $shortSujetName = $shortSujetName[0];

      if (empty($notes)) {
         return $this->noNoteToExport();
      }

      return $this->createCSVFile('notes.csv.twig', 'notes_' . $sujet_obj->getUE()->getNomCourt() . '_' . $shortSujetName . (empty($group->getName()) ? '' : '_' . $group->getName()) . '.csv', array('notes' => $notes, 'subject' => $sujet_obj));
   }

   public function createCSVFileForUEAction($ue_id) {
      $em = $this->getDoctrine()->getManager();
      $request = $this->getRequest();

      $ue_obj = $em->getRepository('AnnotationBundle:UE')->findOneById($ue_id);
      if (!$ue_obj) {
         $this->setError("Impossible d'exporter les notes", "L'UE n'a pas été trouvée");
         return new RedirectResponse($this->getRequest()->headers->get('referer'));
      }

      //Meme les si les notes sont pas gelé on prend toutes les notes (true)
      $notes = $ue_obj->getNotes(true);
       

      if (empty($notes['student'])) {
         return $this->noNoteToExport();
      }
      return $this->createCSVFile('notes_ue.csv.twig', 'notes_' . $ue_obj->getNomCourt() . '.csv', array('notes' => $notes));
   }

}
