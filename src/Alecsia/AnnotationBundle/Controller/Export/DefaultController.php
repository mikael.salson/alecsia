<?php

namespace Alecsia\AnnotationBundle\Controller\Export;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle;
use Symfony\Component\HttpFoundation\Response;
use Alecsia\AnnotationBundle\Controller\Export\HTMLExport;
use Alecsia\AnnotationBundle\Controller\Communication\CRUDReponse;

class DefaultController extends Controller {

   public function sendEmailAction() {
      $em = $this->getDoctrine()->getManager();
      $request = $this->getRequest();

      if (!$this->container->hasParameter('teacher_mail')) {
         $reponse->notification("Impossible d'envoyer le message", "La configuration teacher_mail n'a pas été définie", true);
         return new Response($reponse->getJSON());
      }
      $from = $this->container->getParameter('teacher_mail'); // TODO : Gérer les profs
      $bcc = $from; // TODO : Gérer les profs
      $reponse = new CRUDReponse();
      $subject = $request->get("subject");
      $to = $request->get("to");
      $rendu = $request->get("rendu");
      $empeche_rendu = $request->get("empeche_rendu");
      $mails_to = EmailProcessing::getArrayOfEmails($to);

      if (empty($mails_to)) {
         $reponse->notification("Impossible d'envoyer le message", "Aucun étudiant avec une adresse mail trouvé", true);
         return new Response($reponse->getJSON());
      }

      $body = $request->get("body");

      /* Replace <rendu> by link to the rendu */
      if (strpos($body, '<rendu>') !== FALSE) {
         /* Get the rendu */
         $rendu_obj = $em->getRepository('AnnotationBundle:Rendu')->findOneById($rendu);
         if (!$rendu_obj) {
            $reponse->notification("Impossible d'envoyer le message", "Le rendu n'a pas été trouvé", true);
            return new Response($reponse->getJSON());
         }
         $body = str_replace('<rendu>', $this->generateUrl('AnnotationBundle_annotations', array('camouflaging' => $rendu_obj->getCamouflaging()), true), $body);
      }

      $attachments = array();

      if (!empty($rendu) && !$empeche_rendu) {
         /* Get the HTML of the correction */
         $correction = $this->forward("AnnotationBundle:Correction\Default:index", array('rendu_id' => $rendu, 'lecture_etudiant' => true));
         $html = $correction->getContent();

         $export = new HTMLExport($this, $html);
         $zipFilename = $export->getZipArchive('rendu.html');
         if ($zipFilename) {
            /* Add to attachments */
            $attachments[] = \Swift_Attachment::fromPath($zipFilename)
                    ->setFilename('rendu.zip');
         } else {
            $reponse->notification("Impossible d'envoyer le message", "Il n'a pas été possible de créer l'archive ZIP.", true);
            return new Response($reponse->getJSON());
         }
      }

      $this->get('mailer')
              ->send(EmailProcessing::getEmail($from, $mails_to, '', $bcc, $subject, $body, $attachments));

      $reponse->notification("Envoi réussi", "Le message a bien été envoyé.");
      return new Response($reponse->getJSON());
   }

}
