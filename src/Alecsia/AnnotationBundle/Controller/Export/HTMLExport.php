<?php

namespace Alecsia\AnnotationBundle\Controller\Export;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Routing\Router;
use Symfony\Component\HttpFoundation\Request;

class HTMLExport {

   private $controller;
   private $html;

   public function __construct($c, $html) {
      $this->controller = $c;
      $this->html = $html;
   }

   /**
    * @return the filename of the created archive. The archive contains
    * the exported HTML.
    */
   public function getZipArchive($html_name) {
      /* Create a ZIP archive for storing HTML and JS */
      $zip = new \ZipArchive();
      $zipFilename = tempnam(sys_get_temp_dir(), 'arc');

      if ($zip->open($zipFilename, \ZipArchive::CREATE) == true) {
         /* Replace CSS by their contents in the HTML */
         $this->html = $this->replaceStyleSheets($this->html);
         /* Replace JS folder hierarchy by a simpler one (-> simpler
           hierarchy in the archive).
           Put the JS in the archive.
          */
         $this->html = $this->replaceJS($this->html, $zip);
         /* Add the HTML in the archive */
         $zip->addFromString($html_name, $this->html);
         $zip->close();
      } else {
         return null;
      }
      return $zipFilename;
   }

   /**
    * Parse an HTML content and replace all the references to stylesheets by
    * their content.
    * @param string html: HTML content
    * @return The modified HTML content with embedded CSS
    */
   public function replaceStyleSheets($html) {
      $crawler = $this->getCrawler($html);
      /* Get tags caring about CSS */
      $links = $crawler->filter('link[href][type="text/css"]');

      foreach ($links as $domElement) {
         /* This is the CSS content that will be included in the HTML */
         $content = "\n" . '<!--' . "\n" .
                 $this->getFileContent($domElement->getAttribute('href'))
                 . "\n" . '-->' . "\n";

         /* Create a new tag that will contain the CSS content */
         $style = $domElement->ownerDocument->createElement('style', $content);
         $style->setAttribute('type', 'text/css');
         $parent = $domElement->parentNode;
         /* Replace the <link> tag by the <style> one. */
         $parent->replaceChild($style, $domElement);
      }
      /* Return the modified HTML */
      return $this->getWholeHTMLFromNewNode($parent);
   }

   /**
    * Parse an HTML content and replace all the src attribute of script tags by $folder
    * to have a much simpler folder hierarchy (and to avoid that src contain
    * absolute paths).
    * The files are also included in the ZIP archive.
    *
    * @param string html: HTML content
    * @param ZipArchive zip: Zip archive
    * @param string folder: (default js/) Default folder where we should store the JS
    *                       (can be empty).
    * @return The modified HTML content
    */
   public function replaceJS($html, $zip, $folder = 'js/') {
      $crawler = $this->getCrawler($html);
      /* Get the interesting tags */
      $links = $crawler->filter('script[src]');

      foreach ($links as $domElement) {
         $filename = $domElement->getAttribute('src');
         /* Retrieve the JS content */
         $content = $this->getFileContent($filename);
         /* Add the content to the ZIP archive */
         $zip->addFromString($folder . basename($filename), $content);
         /* Change the src attribute */
         $domElement->setAttribute('src', $folder . basename($filename));

         $parent = $domElement->parentNode;
      }
      if (isset($parent))
      /* Return the modified HTML */
         return $this->getWholeHTMLFromNewNode($parent);
      else
         return $html;
   }

   private function getFileContent($path) {
      /* TODO: /web/* doit probablement pouvoir être changé par quelque
       * chose de plus générique... */
      $content = '';
      /* Either it is a real path */
      if (strpos($path, '/web/bundles') !== FALSE)
         $content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . $path);
      else {
         /* Or it is a path that must be given to the Symfony router */
         $possible_paths = array('/web/app_dev.php', '/web/app.php');
         foreach ($possible_paths as $possible) {
            if (strpos($path, $possible) !== FALSE) {
               $path = substr($path, strlen($possible));
               /* We use the router to know the real "path" */
               $path = $this->controller->get('router')->match($path);
               /* We retrieve the content */
               $content = $this->controller->forward($path['_controller'], $path)->getContent();
               break;
            }
         }
      }
      return $content;
   }

   /* Get a crawler on the HTML content. */

   private function getCrawler($html) {
      $dom = new \DOMDocument('1.0', 'utf-8');
      $dom->validateOnParse = true;

      /* HTML entities are converted to avoid encoding issues with DOMDocument. */
      $html = mb_convert_encoding($html, 'html-entities', 'utf-8');

      /* @ to avoid warnings because of unknown tags (eg. canvas) */
      @$dom->loadHTML($html);

      return new Crawler($dom);
   }

   /* The parameter is a new/modified node, we get to the "highest" parent
    * and then retrieve the whole HTML code of the document. */

   private function getWholeHTMLFromNewNode($parent) {
      $root = $parent;
      while ($root->tagName != 'html') {
         $root = $root->parentNode;
      }
      /* Get the top of the top (otherwise we don't have the HTML tag included) */
      $root = $root->parentNode;

      $innerHTML = "";
      $children = $root->childNodes;
      foreach ($children as $child) {
         $tmp_dom = new \DOMDocument();
         $tmp_dom->appendChild($tmp_dom->importNode($child, true));
         $innerHTML.=trim($tmp_dom->saveHTML());
      }

      return $innerHTML;
   }

}
