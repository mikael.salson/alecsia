<?php

namespace Alecsia\AnnotationBundle\Controller\Export;

class EmailProcessing {

   /**
    * From a list of comma-separated values, return an array of emails.
    * Value that do not look like an email address are filtered out.
    */
   static public function getArrayOfEmails($list) {
      $mails = array();
      if (empty($list))
         return $mails;
      $list = preg_split('/\s*,\s*/', $list);
      foreach ($list as $mail) {
         if (!empty($mail) && !(strpos($mail, '@') === FALSE)) {
            $mails[] = $mail;
         }
      }
      return $mails;
   }

   /**
    * Return an email (a Swift_message) which may contain atachments (if specified).
    * @param from: Sender
    * @param to: Recipients (string for one address or array of string for one
    *            or many addresses; same for cc and bcc)
    * @param cc: Other recipients (may be empty)
    * @param bcc: Hidden recipients (may be empty)
    * @param subject: message title
    * @param body: content of the email
    * @param attachments: array of Swift_Attachment, may be empty.
    */
   static public function getEmail($from, $to, $cc, $bcc, $subject, $body, $attachments = array()) {
      $mail = \Swift_Message::newInstance()
              ->setSubject($subject)
              ->setFrom($from)
              ->setBody($body);
      if (!empty($to))
         $mail = $mail->setTo($to);
      if (!empty($cc))
         $mail = $mail->setCC($cc);
      if (!empty($bcc))
         $mail = $mail->setBCC($bcc);

      foreach ($attachments as $attachment) {
         $mail = $mail->attach($attachment);
      }

      return $mail;
   }

}
