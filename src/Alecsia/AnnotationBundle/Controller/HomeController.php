<?php

namespace Alecsia\AnnotationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Alecsia\AnnotationBundle\Entity\Teacher;
use Alecsia\AnnotationBundle\Entity\Student;
use Alecsia\AnnotationBundle\Entity\AlecsiaUser;

class HomeController extends AlecsiaController {

   public function indexAction() {
      $activeUser = $this->getUser();
      if ($activeUser instanceof Teacher) {
         $home_url = $this->generateUrl("AlecsiaTeacherHome");
      } elseif ($activeUser instanceof Student) {
         $home_url = $this->generateUrl("AlecsiaStudentHome");
      } else {
         $home_url = $this->generateUrl("AlecsiaStudentHome");
      }
      return $this->redirect($home_url);
   }

}
