<?php

/**
 * Author: Ludovic Loridan
 * Date: 25/02/13
 * Time: 03:29
 *
 * GroupController.php
 *
 */

namespace Alecsia\AnnotationBundle\Controller\StudentZone;

use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Service\EntityServices\GroupService;
use Alecsia\AnnotationBundle\Entity\Student;

class GroupController extends AlecsiaController {

   public function joinGroupAction() {
      $targetUrl = $this->generateUrl("AlecsiaStudentHome");
      if (isset($this->getGetParameters()['shareCode'])) {
         $shareCode = $this->getGetParameters()['shareCode'];
         try {
            $group = $this->getGroupService()->getFromShareCode($shareCode, $this->getUser());
         } catch (\Exception $e) {
            error_log(__FILE__ . ':' . __LINE__ . ' User : ' . $this->getUser() . ' shareCode = ' . $shareCode . "\n" . $e->getTraceAsString());
            $this->setError('Problème de droit', "Il n'est pas possible de rejoindre cette UE : vous n'avez pas les droits.");
            return $this->redirect($targetUrl);
         }
      } else
         $group = null;
      if (is_null($group)) {
         $this->setError("UE injoignable", "Aucune UE n'a été trouvée avec cette clé de partage.");
         return $this->redirect($targetUrl);
      }

      $this->getGroupService()->addStudentInGroup($group, $this->getUser(), $this->getUser());
      $this->setSuccess("Bienvenue en " . $group->getUE()->getNomCourt() . " "
              . (!empty($group->getName()) ? '(groupe ' . $group->getName() . ')' : '')
              . ' !');
      return $this->redirect($targetUrl);
   }

   /** @return GroupService */
   private function getGroupService() {
      return $this->get('alecsia.GroupService');
   }

}
