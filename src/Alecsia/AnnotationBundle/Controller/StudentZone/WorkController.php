<?php

/**
 * Author: Ludovic Loridan
 * Date: 24/02/13
 * Time: 20:40
 *
 * WorkController.php
 *
 */

namespace Alecsia\AnnotationBundle\Controller\StudentZone;

use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Entity\Student;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class WorkController extends AlecsiaController {

   /**
    * @Template("AnnotationBundle:Screenfuls:studentWorks.html.twig")
    */
   public function showStudentWorksAction() {
      return array(
          'h_works' => "active"
      );
   }

   /**
    * @Template("AnnotationBundle:Correction:correction.html.twig")
    */
   public function showWork($work_id) {
      $this->getWorkService()->get($work_id);
   }

}
