<?php

namespace Alecsia\AnnotationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Author: Ludovic Loridan
 *
 * AlecsiaController.php
 * Base class with utilities.
 */
abstract class AlecsiaController extends Controller {
   /* ================================== */
   /* Link to services                   */
   /* ================================== */

   protected function session() {
      return $this->get('session');
   }

   /* ============================== */
   /* Shortcuts                      */
   /* ============================== */

   protected function getAlecsiaRepository($entityName) {
       return $this->getDoctrine()->getManager()->getRepository('AnnotationBundle:'.$entityName);
   }

   protected function getPostParameters() {
      return $this->getRequest()->request->all();
   }

   protected function getGetParameters() {
      return $this->getRequest()->query->all();
   }

   protected function requestIsGET() {
      return $this->getRequest()->getMethod() == 'GET';
   }

   protected function requestIsPOST() {
      return $this->getRequest()->getMethod() == 'POST';
   }

   /* ============================== */
   /* Utilities for errors           */
   /* ============================== */

   // Set a flash message for success
   protected function setSuccess($message) {
      $this->session()->getFlashBag()->set('success', $message);
   }

   // Set a flash message for error
   protected function setError($title, $message) {
      $error = array("titre" => $title,
          "contenu" => $message);
      $this->session()->getFlashBag()->set('error', $error);
   }

   // Same, but also send the error given by $error parameters
   protected function redirectAndSendError($redirectionUrl, $errorTitle, $errorMessage) {
      $this->setError($errorTitle, $errorMessage);
      return $this->redirect($redirectionUrl);
   }

   // If $returnedEntity is an entity, set the $success message, else, set the $error message.
   protected function setFlashMessageFromReturnedEntity($returnedEntity, $success, $errorTitle = null, $errorMessage = null) {
      if (is_null($returnedEntity) || ($returnedEntity instanceof ConstraintViolationList)) {
         if (is_null($errorTitle)) {
            $this->setError("Erreur", $returnedEntity);
         } else {
            $this->setError($errorTitle, $errorMessage);
         }
      } else {
         $this->setSuccess($success);
      }
   }

}
