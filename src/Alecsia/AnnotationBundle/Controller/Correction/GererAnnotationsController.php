<?php

namespace Alecsia\AnnotationBundle\Controller\Correction;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Alecsia\AnnotationBundle\Entity\Annotation;
use Alecsia\AnnotationBundle\Controller\Communication\CRUDReponse;
use Alecsia\AnnotationBundle\Controller\Sujet\GererModelesController;

class GererAnnotationsController extends Controller {
   /* =============
     = Mutateurs =
     ============= */

   // Crée une nouvelle annotation en fonction des paramètres donnés
   // Renvoie un CRUD Reponse
   public static function nouvelleAnnotation($controller, $rendu, $debutligne, $debutcolonne, $finligne, $fincolonne, $fichier_id, $nom, $commentaire, $valeur, $exercice_id, $modele_id) {

      $em = $controller->getDoctrine()->getManager();
      $reponse = new CRUDReponse();

      // RECUP OBJETS LIES...
      // > Rendu
      $obj_rendu = $em->getRepository("AnnotationBundle:Rendu")->findOneById($rendu);

      if ($obj_rendu == null) {
         $reponse->notification("Impossible d'ajouter l'annotation", "Le rendu est introuvable. A-t-il été supprimé?", true);
         return $reponse;
      }

      // > Exercice
      if ($exercice_id == "-1") {
         $obj_exercice = null;
      } else {
         $obj_exercice = $em->getRepository("AnnotationBundle:Exercice")->findOneById($exercice_id);

         if ($obj_exercice == null) {
            $reponse->notification("Impossible d'ajouter l'annotation", "L'exercice précisé est introuvable. A-t-il été supprimé?", true);
            return $reponse;
         }
      }

      // > Fichier
      if ($fichier_id != null) {
         $obj_fichier = $em->getRepository("AnnotationBundle:Fichier")->findOneById($fichier_id);

         if ($obj_fichier == null) {
            $reponse->notification("Impossible d'ajouter l'annotation", "Le fichier à annoter n'existe plus.", true);
            return $reponse;
         }
      } else {
         $obj_fichier = null;
      }

      // > Modele
      if ($modele_id == "-1") {
         $obj_modele = null;
      } else {
         $obj_modele = $em->getRepository("AnnotationBundle:Modele")->findOneById($modele_id);

         if ($obj_modele == null) {
            $reponse->notification("Impossible d'ajouter l'annotation", "Le modèle d'annotation choisi est introuvable.", true);
            return $reponse;
         }
      }

      // CREATION DE L'ANNOTATION
      $new_annot = new Annotation();
      $new_annot->setModele(null);
      $new_annot->setFichier($obj_fichier);
      $new_annot->setExercice($obj_exercice);
      $new_annot->setRendu($obj_rendu);
      $new_annot->setPosDebut($debutligne, $debutcolonne);
      $new_annot->setPosFin($finligne, $fincolonne);
      $new_annot->setNom($nom);
      $new_annot->setCommentaire($commentaire);
      $new_annot->setValeurLitterale($valeur);
      $new_annot->setModele($obj_modele);

      /* Subject is marked as STATUS_WAIT ? -> switch to STATUS_MARK */
      if ($obj_rendu->getSujet()->isWaiting()) {
         $obj_rendu->getSujet()->setMarking();
      }

      // PERSISTENCE
      $em->persist($new_annot);
      $em->flush();

      // Données de rendu
      $data = array('a' => $new_annot);
      $data["lookatme"] = true;
      $html = $controller->renderView('AnnotationBundle:Correction:Fichier/annotation.html.twig', $data);

      // CRUD en guise de réponse
      $reponse->ajout($html);
      return $reponse;
   }

   /* ===========
     = Actions =
     =========== */

   // Ajouter une annotation (réclamé avec POST)
   public function ajouterAnnotationAction() {
      $request = $this->getRequest();

      // Champs formulaire
      $rendu = $request->request->get('rendu_id');
      $debutligne = $request->request->get('debutligne');
      $debutcolonne = $request->request->get('debutcolonne');
      $finligne = $request->request->get('finligne');
      $fincolonne = $request->request->get('fincolonne');
      $fichier_id = $request->request->get('fichier_id');
      $nom = $request->request->get('nom');
      $commentaire = $request->request->get('commentaire');
      $valeur = $request->request->get('valeur');
      $exercice_id = $request->request->get('exercice');
      $modele_id = $request->request->get('modele_id');
      $makemodelein = $request->request->get('makemodelein');

      // Une création de modèle a été demandée : c'est parti
      if ($makemodelein != -1) {
         $reponseModele = GererModelesController::nouveauModele($this, $makemodelein, $nom, $commentaire, $valeur, $exercice_id);

         if ($reponseModele[1]->aUneErreur()) {
            return new Response($reponseModele[1]->getJSON());
         } else {
            $modele_id = $reponseModele[0];
         }
      }

      // Retour d'une réponse
      $reponse = self::nouvelleAnnotation($this, $rendu, $debutligne, $debutcolonne, $finligne, $fincolonne, $fichier_id, $nom, $commentaire, $valeur, $exercice_id, $modele_id);

      return new Response($reponse->getJSON());
   }

   // Modifier une annotation (réclamé avec POST)
   public function modifierAnnotationAction() {
      $reponse = new CRUDReponse();

      $request = $this->getRequest();

      // Champs formulaire
      $annot_id = $request->request->get('annot_id');
      $rendu_id = $request->request->get('rendu_id');
      $debutligne = $request->request->get('debutligne');
      $debutcolonne = $request->request->get('debutcolonne');
      $finligne = $request->request->get('finligne');
      $fincolonne = $request->request->get('fincolonne');
      $nom = $request->request->get('nom');
      $commentaire = $request->request->get('commentaire');
      $valeur = $request->request->get('valeur');
      $exercice_id = $request->request->get('exercice');
      $modifPropagee = $request->request->get('propager');

      // Recup de l'annotation : envoi d'une erreur si introuvable
      $em = $this->getDoctrine()->getManager();
      $annot_to_modif = $em->getRepository("AnnotationBundle:Annotation")->findOneById($annot_id);
      if ($annot_to_modif == null) {
         $reponse->suppr($annot_id);
         $reponse->notification("Impossible de modifier l'annotation", "L'annotation n'existe plus.", true);
         return new Response($reponse->getJSON());
      }
      $annot_modele = $annot_to_modif->getModele();

      if ($exercice_id == "-1") {
         $obj_exercice = null;
      } else {
         $obj_exercice = $em->getRepository("AnnotationBundle:Exercice")->findOneById($exercice_id);

         if ($obj_exercice == null) {
            $reponse->notification("Impossible de modifier l'annotation", "L'exercice précisé est introuvable. A-t-il été supprimé?", true);
            return new Response($reponse->getJSON());
         }
      }

      // Modif de l'annot ou du modèle, en fonction
      if (!$modifPropagee || is_null($annot_modele)) {
         $annot_to_modif->setPosDebut($debutligne, $debutcolonne);
         $annot_to_modif->setPosFin($finligne, $fincolonne);
         $annot_to_modif->setNom($nom);
         $annot_to_modif->setCommentaire($commentaire);
         $annot_to_modif->setValeurLitterale($valeur);
         $annot_to_modif->setExercice($obj_exercice);
      } else {
         $annot_to_modif->setPosDebut($debutligne, $debutcolonne);
         $annot_to_modif->setPosFin($finligne, $fincolonne);
         $annot_modele->setNom($nom);
         $annot_modele->setCommentaire($commentaire);
         $annot_modele->setValeurLitterale($valeur);
         $annot_modele->setExercice($obj_exercice);
         $annot_to_modif->setNom($nom);
         $annot_to_modif->setCommentaire($commentaire);
         $annot_to_modif->setValeurLitterale($valeur);
         $annot_to_modif->setExercice($obj_exercice);
      }

      // Persistence
      $em->flush();

      // Rafraichissement de la note de tous les rendus concernés par le modèle
      if ($modifPropagee && !is_null($annot_modele)) {
         $cn = $this->get('annotation.calculnotes');
         $cn->modeleModifie($annot_modele);
      }

      // Données de rendu
      if (!$modifPropagee || is_null($annot_modele)) {
         $annot_modifiees = array($annot_to_modif);
      } else {
         $annot_modifiees = $em->getRepository("AnnotationBundle:Annotation")->findBy(array("modele" => $annot_modele->getId(), "rendu" => $rendu_id));
      }

      foreach ($annot_modifiees as $annot_modifiee) {
         $data = array('a' => $annot_modifiee);
         if ($request->isXmlHttpRequest()) {
            $data["lookatme"] = true;
         }

         $html = $this->renderView('AnnotationBundle:Correction:Fichier/annotation.html.twig', $data);
         $reponse->modif($html);
      }

      return new Response($reponse->getJSON());
   }

   // Supprimer une annotation (réclamé avec POST)
   public function supprimerAnnotationAction() {
      $reponse = new CRUDReponse();

      $request = $this->getRequest();

      // Champs formulaire
      $annot_id = $request->request->get("annot_id");

      // Recup de l'annotation à supprimer
      $em = $this->getDoctrine()->getManager();
      $annot_to_delete = $em->getRepository("AnnotationBundle:Annotation")->findOneById($annot_id);

      // Suppression
      if ($annot_to_delete != null) {
         $em->remove($annot_to_delete);
         $em->flush();
      }

      $reponse->suppr($annot_id);

      // Rendu en guise de réponse
      return new Response($reponse->getJSON());
   }

}
