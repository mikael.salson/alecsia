<?php

namespace Alecsia\AnnotationBundle\Controller\Correction;

use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Alecsia\AnnotationBundle\Entity\Annotation;

class DefaultController extends AlecsiaController {
   /* Marque un rendu comme terminé et redirige vers la page du sujet */

   public function terminerRenduAction($rendu_id) {

      // Recup BDD des infos rendu, ses fichiers, leurs annotations
      $em = $this->getDoctrine()->getManager();
      $rendu = $em->getRepository("AnnotationBundle:Rendu")->findOneById($rendu_id);

      if (!$rendu) {
         throw new NotFoundHttpException(sprintf('Rendu #%s introuvable.', $rendu_id));
      } else {
         $cn = $this->get('annotation.calculnotes');
         $cn->rafraichirNote($rendu);

         $rendu->setTermine(true);
         $em->flush();
         $this->setSuccess('Rendu marqué comme terminé');
         return $this
                         ->redirect($this
                                 ->generateUrl('AnnotationBundle_sujet', array("sujet_id" => $rendu->getSujet()->getId(),
                                     'group_id' => $rendu->getGroup()->getId())));
      }
   }

   /* Rend le détail des notes d'un rendu d'id précisé en param. */

   public function rafraichirBaremeAction() {

      // Recup des services
      $calculeur = $this->get('annotation.calculnotes');
      $em = $this->getDoctrine()->getManager();

      $request = $this->getRequest();

      $rendu_id = $request->get("rendu_id");

      // Recup du rendu
      $rendu = $em->getRepository('AnnotationBundle:Rendu')->findOneById($rendu_id);
      if (!$rendu) {
         throw new NotFoundHttpException(sprintf('Rendu #%s introuvable.', $rendu_id));
      }

      // Calcul du détail + Rendu
      $bareme = $calculeur->getBareme($rendu);

      $data = array();
      $data["bareme"] = $bareme;

      // Comparaison des deux notes
      $ancienne_note = $request->get("ancienne_note");
      $nouvelle_note = $bareme->getNoteFinale();

      $diff_note = $nouvelle_note - $ancienne_note;

      if ($request->isXmlHttpRequest()) {
         if ($diff_note < 0) {
            $data["lookatme"] = "negatif";
         } else if ($diff_note > 0) {
            $data["lookatme"] = "";
         }
      }

      return $this->render('AnnotationBundle:Correction:Bareme/bareme.html.twig', $data);
   }

   /* Répond à la requête de gestion des fichiers (paramètres en POST) */

   public function gererFichiersAction() {
      $request = $this->getRequest();
      $em = $this->getDoctrine()->getManager();

      // Récup des arguments
      $fichiers_rendu = $request->get("fichier") ? : array();
      $fichiers_actifs = $request->get("actif") ? : array();
      $langages = $request->get("lang") ? : array();
      $rendu_id = $request->get("rendu");

      $fichiers = array();

      // // On vérifie que l'on ne tente pas de faire un rendu vide
      $nbfichiers = count($fichiers_actifs);
      // if ($nbfichiers == 0) {
      //     $this->->setError("Oups...",
      //         "Il est impossible de créer un rendu vide."
      //     ));
      //     return $this->redirect($this->generateUrl('AnnotationBundle_corriger',array("rendu_id" => $rendu_id)));
      // }
      // Récup des fichiers et exclusion de tous
      foreach ($fichiers_rendu as $key => $fichier_rendu) {
         $fichiers[$fichier_rendu] = $em->getRepository("AnnotationBundle:Fichier")->findOneById($fichier_rendu);
         $fichiers[$fichier_rendu]->setExclus(true);
      }

      // Récup des langages
      foreach ($langages as $key => $langage) {
         $langages[$key] = $em->getRepository("AnnotationBundle:Langage")->findOneById($langage);
      }

      // Reactivation des fichiers adéquats + set langages
      foreach ($fichiers_actifs as $key => $on) {
         $fichiers[$key]->setExclus(false);
         $fichiers[$key]->setLangage($langages[$key]);
      }

      // Persistence
      $em->flush();

      // Flash Message
      $this->setSuccess('Changements enregistrés');

      return $this->redirect($this->generateUrl('AnnotationBundle_corriger', array("rendu_id" => $rendu_id)));
   }

   /* Rend le rendu d'id donné en paramètre */

   public function indexAction($rendu_id, $lecture_etudiant = false) {

      // Recup BDD des infos rendu, ses fichiers, leurs annotations
      $em = $this->getDoctrine()->getManager();
      $rendu = $this->get('alecsia.workService')->get($rendu_id, $this->getUser());

      if (!$rendu) {
         throw new NotFoundHttpException(sprintf('Rendu #%s introuvable.', $rendu_id));
      }

      $fichiers = $em->getRepository("AnnotationBundle:Fichier")->findBy(
              array("rendu" => $rendu->getId()), array("dossier" => "asc",
          "nom_fichier" => "asc")
      );
      $annotations = $em->getRepository("AnnotationBundle:Annotation")->findByRendu($rendu->getId());

      $exercices = $em->getRepository("AnnotationBundle:Exercice")->findBySujet($rendu->getSujet()->getId());
      $langages = $em->getRepository("AnnotationBundle:Langage")->findBy(array(), array("nom" => "asc"));



      // Classement des annotations par fichier
      // TODO Peut peut être être optimisé en faisant faire ce tri à la BDD
      $annotArray = array();

      foreach ($fichiers as $f) {
         $annotArray[(string) $f->getId()] = array();
      }

      foreach ($annotations as $a) {
         $fichier = $a->getFichier();
         if ($fichier != null) {
            $id_fichier = (string) $fichier->getId();
         } else {
            $id_fichier = "entete";
         }
         $annotArray[$id_fichier][] = $a;
      }

      // Calcul du barème
      $calculeur = $this->get('annotation.calculnotes');
      $bareme = $calculeur->getBareme($rendu, true);

      return $this->render('AnnotationBundle:Correction:correction.html.twig', array(
                  'rendu' => $rendu,
                  'fichiers' => $fichiers,
                  'liste_annotations' => $annotArray,
                  'exercices' => $exercices,
                  'langages' => $langages,
                  'bareme' => $bareme,
                  'lectureseule' => $rendu->getSujet()->isFrozen() || $lecture_etudiant,
                  'lectureetudiant' => $lecture_etudiant
      ));
   }

   public function decamouflageAction($camouflaging) {
      $em = $this->getDoctrine()->getManager();
      $rendu = $em->getRepository("AnnotationBundle:Rendu")->findOneByCamouflaging($camouflaging);
      if (!$rendu) {
         throw $this->createNotFoundException(sprintf('Rendu %s introuvable. Vérifiez l\'adresse entrée', $camouflaging));
      }

      return $this->forward("AnnotationBundle:Correction\Default:index", array('rendu_id' => $rendu->getId(), 'lecture_etudiant' => true));
   }

}
