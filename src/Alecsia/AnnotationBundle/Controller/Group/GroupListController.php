<?php

namespace Alecsia\AnnotationBundle\Controller\Group;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Alecsia\AnnotationBundle\Service\EntityServices\GroupService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Alecsia\AnnotationBundle\Controller\AlecsiaController;

/**
 * Author: Ludovic Loridan
 *
 * GroupListController.php
 * Manages the different lists of Group that Alecsia can show.
 */
class GroupListController extends AlecsiaController {
   // TODO : Remove

   /* /\* ============================== *\/ */
   /* /\* Actions                        *\/ */
   /* /\* ============================== *\/ */

   /* /\** */
   /*    * @Template("AnnotationBundle:Screenfuls:activeGroups.html.twig") */
   /*    * \/ */
   /* public function showActiveGroupsAction() { */
   /*     return array( */
   /*         'groups'         => $this -> getListOfActiveUsersGroups(), */
   /*         'archivedGroups' => $this -> getListOfArchivedUsersGroups(), */
   /*         'h_corriger'     => 'active' */
   /*     ); */
   /* } */

   /* /\** */
   /*    * @Template("AnnotationBundle:Screenfuls:archivedGroups.html.twig") */
   /*    * \/ */
   /* public function showArchivedGroupsAction() { */
   /*     return array( */
   /*         'groups'        => $this -> getListOfArchivedUsersGroups(), */
   /*         'h_archives' => 'active' */
   /*     ); */
   /* } */

   /* /\* ============================== *\/ */
   /* /\* Shortcuts                      *\/ */
   /* /\* ============================== *\/ */
   /* private function getListOfArchivedUsersGroups() { */
   /*     $teacher = $this->getUser(); */
   /*     $archivedGroups = $this->GroupService()->getListOfArchivedGroups($teacher); */
   /*     return $archivedGroups; */
   /* } */

   /* private function getListOfActiveUsersGroups() { */
   /*     $teacher = $this->getUser(); */
   /*     $activeGroups = $this->GroupService()->getListOfActiveGroups($teacher); */
   /*     return $activeGroups; */
   /* } */

   /* /\* ================================== *\/ */
   /* /\* Link to services                   *\/ */
   /* /\* ================================== *\/ */

   /* /\** */
   /*    * @return UEService */
   /*    * \/ */
   /* private function GroupService() { */
   /*     return $this->get('alecsia.GroupService'); */
   /* } */
}
