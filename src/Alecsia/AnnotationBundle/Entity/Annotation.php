<?php

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alecsia\AnnotationBundle\Entity\Annotation
 *
 * @ORM\Table(name="Annotation")
 * @ORM\Entity(repositoryClass="Alecsia\AnnotationBundle\Entity\AnnotationRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Annotation {

   /**
    * @var integer $id
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   protected $id;

   /**
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\Fichier")
    */
   protected $fichier;

   /**
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\Rendu", inversedBy="annotations")
    */
   protected $rendu;

   /**
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\Modele")
    */
   protected $modele;

   /**
    * @var integer $debut_ligne
    *
    * @ORM\Column(name="debut_ligne", type="integer")
    */
   protected $debut_ligne = 0;

   /**
    * @var integer $debut_col
    *
    * @ORM\Column(name="debut_col", type="integer")
    */
   protected $debut_col = 0;

   /**
    * @var integer $fin_ligne
    *
    * @ORM\Column(name="fin_ligne", type="integer")
    */
   protected $fin_ligne = 0;

   /**
    * @var integer $fin_col
    *
    * @ORM\Column(name="fin_col", type="integer")
    */
   protected $fin_col = 0;
//
// Champs dépendant du modèle
//

   protected static $modele_dependant = array("nom", "commentaire", "valeur", "relatif", "exercice");

   /**
    * @var string $nom
    *
    * @ORM\Column(name="nom", type="string", length=255, nullable=true)
    */
   protected $nom;

   /**
    * @var text $commentaire
    *
    * @ORM\Column(name="commentaire", type="text", nullable=true)
    */
   protected $commentaire;

   /**
    * @var float $valeur
    *
    * @ORM\Column(name="valeur", type="float", nullable=true)
    */
   protected $valeur;

   /**
    * @var boolean $relatif
    *
    * @ORM\Column(name="relatif", type="boolean", nullable=true)
    */
   protected $relatif;

   /**
    * @var boolean $letter
    *
    * @ORM\Column(name="letter", type="boolean", nullable=true)
    */
   protected $letter;

   /**
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\Exercice", inversedBy="annotations")
    */
   protected $exercice;

// VARIABLES DE DELEGUATION

   /**
    * @ORM\Column(name="deleguer_nom", type="boolean")
    */
   protected $deleguer_nom = false;

   /**
    * @ORM\Column(name="deleguer_commentaire", type="boolean")
    */
   protected $deleguer_commentaire = false;

   /**
    * @ORM\Column(name="deleguer_valeur", type="boolean")
    */
   protected $deleguer_valeur = false;

   /**
    * @ORM\Column(name="deleguer_letter", type="boolean")
    */
   protected $deleguer_letter = false;

   /**
    * @ORM\Column(name="deleguer_relatif", type="boolean")
    */
   protected $deleguer_relatif = false;

   /**
    * @ORM\Column(name="deleguer_exercice", type="boolean")
    */
   protected $deleguer_exercice = false;

   /* ================
     = Constructeur =
     ================ */

// function __construct($modele=null,$fichier=null,$exercice=null,$rendu=null,$dl=null,$dc=null,$fl=null,$fc=null,$nom=null,$comm=null,$val=null) {
//     $this->modele = $modele;
//     $this->fichier = $fichier;
//     $this->exercice = $exercice;
//     $this->rendu = $rendu;
//     $this->debut_ligne = $dl;
//     $this->debut_col = $dc;
//     $this->fin_ligne = $fl;
//     $this->fin_col = $fc;
//     $this->nom = $nom;
//     $this->commentaire = $comm;
//     $this->setValeurLitterale($val);
// }

   /* ===========================
     = Traitements prePersists =
     =========================== */

   /**
    * Permet de purger les infos doublons entre modèle et annotation, afin
    * d'éviter les surchages inutiles.
    * @ORM\PrePersist()
    * @ORM\PreUpdate()
    */
   public function lierAuModele() {
      if ($this->getModele() != null) {
         $annot_vars = get_object_vars($this);

         foreach (self::$modele_dependant as $nom_champ) {
            $champ_delegation = "deleguer_" . $nom_champ;
            $getChamp = "get" . ucwords($nom_champ);

            $donnee_annot = $annot_vars[$nom_champ];
            $donnee_modele = $this->getModele()->$getChamp();

            if ($donnee_annot == $donnee_modele) {
               $this->$nom_champ = null;
               $this->$champ_delegation = true;
            } else {
               $this->$champ_delegation = false;
            }
         }
      } else {
         foreach (self::$modele_dependant as $nom_champ) {
            $champ_delegation = "deleguer_" . $nom_champ;
            $this->$champ_delegation = false;
         }
      }
   }

   /* ===========
     = Helpers =
     =========== */

   public function getPole() {
      if ($this->getValeur() > 0) {
         return "positif";
      } else if ($this->getValeur() < 0) {
         return "negatif";
      } else {
         return "neutre";
      }
   }

// Détache cette annotation de son modèle.
// La rend totalement indépendante.
   public function desolidariser() {
      foreach (self::$modele_dependant as $nom_champ) {
         $deleguer_champ = "deleguer_" . $nom_champ;
         $getChamp = "get" . ucwords($nom_champ);
         $setChamp = "set" . ucwords($nom_champ);

         if ($this->$deleguer_champ) {
            $donnee_modele = $this->getModele()->$getChamp();
            $this->$setChamp($donnee_modele);
         }
      }

      $this->modele = null;
   }

   /* =====================
     = Getters & Setters =
     ===================== */

//
// Indépendants du modèle
//
   public function getId() {
      return $this->id;
   }

   public function setPosDebut($ligne, $colonne) {
      $this->debut_ligne = $ligne;
      $this->debut_col = $colonne;
   }

   public function getDebutLigne() {
      return $this->debut_ligne;
   }

   public function getDebutColonne() {
      return $this->debut_col;
   }

   public function setPosFin($ligne, $colonne) {
      $this->fin_ligne = $ligne;
      $this->fin_col = $colonne;
   }

   public function getFinLigne() {
      return $this->fin_ligne;
   }

   public function getFinColonne() {
      return $this->fin_col;
   }

   public function getFichier() {
      return $this->fichier;
   }

   public function setFichier($fichier) {
      $this->fichier = $fichier;
   }

   public function getRendu() {
      return $this->rendu;
   }

   public function setRendu(Rendu $rendu) {
      $this->rendu = $rendu;
   }

   public function getModele() {
      return $this->modele;
   }

   public function setModele($modele) {
      $this->modele = $modele;
   }

//
// Dépendants du modèle
//

   public function getDeleguerNom() {
      return $this->deleguer_nom;
   }

   public function setDeleguerNom($deleguer_nom) {
      $this->deleguer_nom = $deleguer_nom;
   }

   public function getDeleguerValeur() {
      return $this->deleguer_valeur;
   }

   public function setDeleguerValeur($deleguer_valeur) {
      $this->deleguer_valeur = $deleguer_valeur;
   }

   public function getDeleguerLetter() {
      return $this->deleguer_letter;
   }

   public function setDeleguerLetter($deleguer_letter) {
      $this->deleguer_letter = $deleguer_letter;
   }

   public function getDeleguerRelatif() {
      return $this->deleguer_relatif;
   }

   public function setDeleguerRelatif($deleguer_relatif) {
      $this->deleguer_relatif = $deleguer_relatif;
   }

   public function getDeleguerCommentaire() {
      return $this->deleguer_commentaire;
   }

   public function setDeleguerCommentaire($deleguer_commentaire) {
      $this->deleguer_commentaire = $deleguer_commentaire;
   }

   public function getDeleguerExercice() {
      return $this->deleguer_exercice;
   }

   public function setDeleguerExercice($deleguer_exercice) {
      $this->deleguer_exercice = $deleguer_exercice;
   }

// Renvoie la propriété "$nom_prop" de l'annotation.
// Va vérifier dans le modèle le cas échéant.
   public function getMux($nom_prop) {

      $annot_vars = get_object_vars($this);
      $annot_prop = $annot_vars[$nom_prop];
      $prop_deleguee = $annot_vars["deleguer_" . $nom_prop];

// Check : Existence du modèle
      if (is_null($this->getModele())) {
         return $annot_prop;
      }

// Check : Surcharge de la propriété
      if (!$prop_deleguee) {
         return $annot_prop;
      }

// On doit appeler le getter du modèle :
      $nom_methode = "get" . ucwords($nom_prop);
      return $this->getModele()->$nom_methode();
   }

   public function getNom() {
      return $this->getMux("nom");
   }

   public function setNom($nom) {
      $this->nom = $nom;
   }

   public function getCommentaire() {
      return $this->getMux("commentaire");
   }

   public function setCommentaire($commentaire) {
      $this->commentaire = $commentaire;
   }

   public function getExercice() {
      return $this->getMux("exercice");
   }

   public function setExercice($exercice) {
      $this->exercice = $exercice;
   }

   public function getValeur() {
      return $this->getMux("valeur");
   }

   protected function setValeur($valeur) {
      return $this->valeur = $valeur;
   }

   public function getRelatif() {
      return $this->getMux("relatif");
   }

   protected function setRelatif($relatif) {
      return $this->relatif = $relatif;
   }

   public function getLetter() {
      return $this->getMux("letter");
   }

   protected function setLetter($letter) {
      $this->letter = $letter;
   }

// Retourne une chaine représentant la valeur de l'annotation
   public function getValeurFormatee() {
      $valeur = $this->getValeur();
      $exercise = $this->getExercice();
      $prefixe = "";
      $suffixe = "";
      if ($valeur > 0) {
         $prefixe = "+";
      }

      if ($this->getRelatif() == true) {
         if (($this->getLetter() == true) && ($exercise != null)) {
            $guts = $exercise->getExerciceGuts() - 1;
            if ($guts !== 0) {

               if ($exercise->getInitialMark() == 0) {

                  $lettreValue = ($valeur * $guts);
                  round($lettreValue);
               } else {
                  if ($exercise->getInitialMark() !== 0) {
                     if($valeur==0){
                     $lettreValue = ((1 + $valeur) * ($guts));
                     }else{
                        $lettreValue = ((1 + $valeur) * ($guts-1));
                     }
                     round($lettreValue);
                  }
               }
               return chr(65 + $guts - $lettreValue);
            }
         }
         $suffixe = " %";
         $valeur = $valeur * 100;
      }

      return $prefixe . $valeur . $suffixe;
   }

   public function recupererLettre($litterale) {
      $exercise = $this->getExercice();
      $lettreValue = ord($litterale);
      if ($exercise == null) {
         return;
      } else {
         $exerciseini = $this->getExercice()->getInitialMark();
         $guts = $this->getExercice()->getExerciceGuts() - 1;
         $this->setLetter(true);

//enlever le valeur de A si maj sinon de a
         if (($lettreValue > 64) && ($lettreValue < 91)) {
            $lettreValue = $lettreValue - ord("A");
         } else {
            $lettreValue = $lettreValue - ord("a");
         }
// inverser l'ordre pour que A sois la plus grande valeur 
         $lettreValue = $guts - $lettreValue;

         if ($lettreValue < 0 || $lettreValue > $guts) {
            return "-100000000"; //
         } else {
            $lettreValue = (100 * $lettreValue) / ($guts);
            if ($exerciseini == 0) {
               $litterale = '+' . $lettreValue . '%';
            } else {
               $litterale = '-' . (100 - $lettreValue) . '%';
            }
         }
      }
      return $litterale;
   }

// Partant d'une chaîne de type "- 30%" ou "+ 2.3", calcule la valeur et le caractère
// relatif de l'annotation et appelle les setters adéquats.
   public function setValeurLitterale($litterale) {

// Valeur littérale non affectée
      if ($litterale == null) {
         if ($this->getModele() == null) {
            $this->valeur = 0;
            $this->relatif = false;
            return;
         } else {
            $this->valeur = null;
            $this->relatif = null;
            return;
         }
      }

// Passage Virgules => Points
      $litterale = preg_replace("/,/", ".", $litterale);

// Retrait des espaces
      $litterale = preg_replace("/ /", "", $litterale);
      if ($this->getExercice() != null) {
         $isletter = $this->getExercice()->isLettre();
      } else {
         $isletter = false;
      }

      if ((strlen($litterale) == 1) && $isletter && (ctype_alpha($litterale))) {
         $litterale = $this->recupererLettre($litterale);
      } else {
         $this->setLetter(false);
      }
// Transfo "-20%" => "-0,2"
      $isRelatif = strpos($litterale, '%') !== false;
      $floatval = floatval($litterale);

      if ($isRelatif) {
         $valeur = $floatval / 100;
      } else {
         $valeur = $floatval;
      }


      $valeur = (float) number_format($valeur, 2);

// Stockage infos
      $this->valeur = $valeur;
      $this->relatif = $isRelatif;
   }

}
