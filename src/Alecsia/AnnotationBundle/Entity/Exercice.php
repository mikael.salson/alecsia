<?php

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alecsia\AnnotationBundle\Entity\Exercice
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Alecsia\AnnotationBundle\Entity\ExerciceRepository")
 */
class Exercice extends AlecsiaEntity {

   /**
    * @ORM\OneToMany(targetEntity="Alecsia\AnnotationBundle\Entity\Modele", mappedBy="exercice")
    */
   protected $modeles;

   /**
    * @ORM\OneToMany(targetEntity="Alecsia\AnnotationBundle\Entity\Annotation", mappedBy="exercice")
    */
   protected $annotations;

   /**
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\Sujet", inversedBy="exercices")
    */
   protected $sujet;

   /**
    * @var integer $numero
    *
    * @ORM\Column(name="numero", type="integer")
    */
   protected $numero;

   /**
    * @var string $nom
    *
    * @ORM\Column(name="nom", type="string", length=255)
    */
   protected $nom;

   /**
    * @var float $valeur
    *
    * @ORM\Column(name="valeur", type="float")
    */
   protected $valeur;

   /**
    * @var float $bonus_malus
    *
    * @ORM\Column(name="bonus_malus", type="float")
    */
   protected $bonus_malus;

   /**
    *
    * @var type float $initialMark
    * 
    * @ORM\Column(name="initialMark", type="float")
    */
   protected $initialmark;

   /**
    * indicates the number of guts for lettre format 
    * if 0 value is shown as number else it will be shown as lettre 
    * @var type 
    * @ORM\Column(name="guts", type="float")
    */
   protected $guts;

   /* ================
     = Constructeur =
     ================ */

   function __construct($sujet, $numero, $nom, $valeur, $initialMark, $bonus_malus = 0, $guts = 0) {
      $this->sujet = $sujet;
      $this->numero = $numero;
      $this->nom = $nom;
      $this->valeur = $valeur;
      if (! isset($initialMark)) {
          $initialMark = $valeur;
      }
      $this->initialmark = $initialMark;
      $this->bonus_malus = $bonus_malus;
      $this->guts = $guts;
   }

   /* ===================
     = PrePersist & Co =
     =================== */

   /**
    * Remplace toutes les références à cet exercice par des références à l'exercice
    * "Bonus/Malus"
    * @ORM\PreRemove
    */
   public function reassocierAnnotationsEtModeles() {
      $annots = $this->annotations;
      $modeles = $this->modeles;

      foreach ($annots as $annot) {
         $annot->setExercice(null);
      }
      foreach ($modeles as $modele) {
         $modele->setExercice(null);
      }
   }

   /* =====================
     = Getters & Setters =
     ===================== */

   public function setNom($nom) {
      $this->nom = $nom;
   }

   public function getNom() {
      return $this->nom;
   }

   public function setValeur($valeur) {
      $this->valeur = $valeur;
   }

   public function getValeur() {
      return $this->valeur;
   }

   public function getNumero() {
      return $this->numero;
   }

   public function setNumero($numero) {
      $this->numero = $numero;
   }

   public function getSujet() {
      return $this->sujet;
   }

   public function setSujet(Sujet $sujet) {
      $this->sujet = $sujet;
   }

   /**
    * 
    * @return type float : initial value to calculate note
    */
   public function getBonusMalus() {
      return $this->bonus_malus;
   }

   public function setBonusMalus($bonus_malus) {
      $this->bonus_malus = $bonus_malus;
   }

   public function getInitialMark() {
      return$this->initialmark;
   }

   public function getExerciceGuts() {
      return$this->guts;
   }

   public function setExerciceGuts($guts_number) {
      $this->guts = $guts_number;
   }

   public function setInitialMark($initialMark) {
      $this->initialmark = $initialMark;
   }

   // To String
   public function __toString() {
      return sprintf("Ex %d : %s", $this->getNumero(), $this->getNom(), $this->getValeur());
   }

   /* =========
     = Clone =
     ========= */

   public function cloner($sujet) {
      $e = new Exercice($sujet, $this->getNumero(), $this->getNom(),
      $this->getValeur(), $this->getInitialMark(), $this->getBonusMalus(),
      $this->getExerciceGuts());
      return $e;
   }

   public function isLettre() {
      return $this->getExerciceGuts() != 0;
   }

}
