<?php

/**
 * Author: Ludovic Loridan
 * Date: 27/01/13
 * Time: 14:06
 *
 * Teacher.php
 * Hold a teacher.
 */

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Role\Role;

/**
 * @ORM\Entity()
 */
class Teacher extends AlecsiaUser {

   /**
    * @ORM\Column(name="admin", type="boolean", nullable=true)
    */
   protected $admin = false;

   /* ============================== */
   /* Factories                      */
   /* ============================== */

   public static function teacherFromMap($map) {
      $teacher = new Teacher();
      $teacher->setLogin($map["login"]);
      $teacher->setFirstName($map["firstName"]);
      $teacher->setLastName($map["lastName"]);
      $teacher->setEmail($map["email"]);
      if (isset($map['admin']) && $map['admin'])
         $teacher->setAdmin(true);
      return $teacher;
   }

   /* ============================== */
   /* Roles & Auth                   */
   /* ============================== */

   /**
    * Returns the roles granted to the user.
    *
    * @return Role[] The user roles
    */
   function getRoles() {
      $result = array(new Role("ROLE_TEACHER"));
      if ($this->isAdmin()) {
         $result[] = new Role("ROLE_ADMIN");
         $result[] = new Role("ROLE_ALLOWED_TO_SWITCH");
      }
      return $result;
   }

   public function setAdmin($admin) {
      $this->admin = $admin;
   }

   public function isAdmin() {
      return $this->admin;
   }

}
