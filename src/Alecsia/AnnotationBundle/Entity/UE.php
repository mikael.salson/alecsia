<?php

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Alecsia\AnnotationBundle\Entity\ListeModeles;

/**
 * Alecsia\AnnotationBundle\Entity\UE
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Alecsia\AnnotationBundle\Entity\UERepository")
 */
class UE extends AlecsiaEntity {

   /**
    * @var string $nom_court
    * @Assert\NotBlank()
    * @ORM\Column(name="nom_court", type="string", length=10)
    */
   protected $nom_court;

   /**
    * @var string $nom
    * @Assert\NotBlank()
    * @ORM\Column(name="nom", type="string", length=255)
    */
   protected $nom;

   /**
    * @var boolean $archived
    *
    * @ORM\Column(name="archived", type="boolean")
    */
   protected $archived = false;

   /**
    * @var ArrayCollection
    * Register all the groups that exist in this UE
    * @ORM\OneToMany(targetEntity="Alecsia\AnnotationBundle\Entity\Group", mappedBy="UE", cascade={"persist","remove"})
    */
   protected $groups = NULL;

   /**
    * @ORM\Column(name="year", type="integer")
    */
   protected $year;

   /**
    * @ORM\OneToOne(targetEntity="Alecsia\AnnotationBundle\Entity\ListeModeles", cascade={"persist", "remove"})
    */
   protected $model_list;

   /**
    * @var Teacher
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\Teacher")
    * @Assert\NotBlank()
    */
   protected $owner;

   /**
    * @var ArrayCollection
    * @ORM\OneToMany(targetEntity="Alecsia\AnnotationBundle\Entity\Sujet", mappedBy="UE", cascade={"persist","remove"})
    */
   protected $sujets;

   public function __construct() {
      $this->sujets = new ArrayCollection();
      $this->groups = new ArrayCollection();
      /* Add a default group */
      $this->addGroup(Group::getDefaultGroup());
   }

    /**
     * @return marks for all the subjects
     * @param all_marks: return also marks for not frozen subjects
     */
   public function getNotes($all_marks = false) {
      $notes = array();
      $notes['student'] = array();
      $notes['group'] = array();
      foreach ($this->sujets as $sujet) {
         if ($all_marks || $sujet->isFrozen()) {
            $notes_tp = $sujet->getNotes(null, $all_marks);

            $notes['notes'][$sujet->getId()] = array();
            $notes['sujet'][] = $sujet;
            foreach ($notes_tp as $info) {
               $notes['student'] += array($info['student']->getId() => $info['student']);
               $notes['group'] += array($info['student']->getId() => $info['group']);
               $notes['notes'][$sujet->getId()] += array($info['student']->getId() => $info['note']);
            }
         }
      }
      return $notes;
   }

   //retunr boolean
   // check retun False if one subjet not frozen yet
   public function isAllSujetFrozen(){
     $frozen = true;
     foreach ($this->sujets as $sujet) {
       if (!$sujet->isFrozen()) {
         $frozen = false;
       }
     }
     return $frozen;
   }
   /* ============================== */
   /* Right managements              */
   /* ============================== */

   public function isUserAllowedToGet($user) {
      return true;
   }

   public function isUserAllowedToAdd($user) {
      return $user->isTeacher();
   }

   public function isUserAllowedToUpdate($user) {
      return $this->isUserOwner($user);
   }

   public function isUserAllowedToDelete($user) {
      return $this->isUserOwner($user);
   }

   public function isUserOwner($user) {
      return ($user instanceof AlecsiaUser) && $user->equals($this->owner);
   }

   /* ============================== */
   /* Factories                      */
   /* ============================== */

   public static function UEFromArray($params, $owner) {
      $ue = new UE();
      $ue->setParamsFromArray($params);
      $ue->setOwner($owner);
      $ue->setModelList(new ListeModeles());

      return $ue;
   }

   public function setParamsFromArray($params) {
      if (isset($params['nom_court']))
         $this->setNomCourt($params['nom_court']);
      if (isset($params['nom']))
         $this->setNom($params['nom']);
      if (isset($params['year']))
         $this->setYear($params['year']);

      return $this;
   }

   /* ============================== */
   /* Old code                       */
   /* ============================== */

   // It's old ! Symfony already knows how to clone an entity.
   // You should use cascade properties instead
   public function cloner() {
      $ue = new UE();
      $ue->setNomCourt($this->getNomCourt());
      $ue->setNom($this->getNom());
      $ue->setYear($this->getYear());
      $ue->setOwner($this->getOwner());
      $ue->setModelList(new ListeModeles());

      // Copie des sujets
      $old_sujets = $this->getSujets();
      foreach ($old_sujets as $old_sujet) {
         $new_sujet = $old_sujet->cloner($ue);
         $ue->addSujet($new_sujet);
      }

      return $ue;
   }

   /* ============================== */
   /* Properties get/set             */
   /* ============================== */

   public function getNom() {
      return $this->nom;
   }

   public function getNomCourt() {
      return $this->nom_court;
   }

   public function setNom($nom) {
      $this->nom = $nom;
   }

   public function setNomCourt($nom_court) {
      $this->nom_court = $nom_court;
   }

   public function getSujets() {
      return $this->sujets;
   }

   public function addSujet($sujet) {
      $this->sujets[] = $sujet;
      $sujet->setUE($this);
   }

   public function getGroups() {
      return $this->groups;
   }

   public function addGroup($group) {
      $this->groups[] = $group;
      $group->setUE($this);
   }

   public function setArchived($archived) {
      $this->archived = $archived;
   }

   public function getArchived() {
      return $this->archived;
   }

   public function getTPs() {
      return $this->sujets->filter(function($sujet) {
                 return !($sujet->getIsTD());
              });
   }

   public function getTDs() {
      return $this->sujets->filter(function($sujet) {
                 return $sujet->getIsTD();
              });
   }

   public function setYear($year) {
      $this->year = $year;
   }

   public function getYear() {
      return $this->year;
   }

   public function getModelList() {
      return $this->model_list;
   }

   public function setModelList($model_list) {
      if (!$this->getArchived())
         $this->model_list = $model_list;
   }

   public function setOwner($owner) {
      foreach ($this->getGroups()->getIterator() as $group) {
         if (($this->owner == NULL && $group->getOwner() == NULL) || $this->owner->equals($group->getOwner()))
            $group->setOwner($owner);
      }
      $this->owner = $owner;
   }

   /** @return Teacher */
   public function getOwner() {
      return $this->owner;
   }

}
