<?php

/**
 * Author: Ludovic Loridan
 * Date: 02/02/13
 * Time: 18:30
 *
 * AlecsiaEntity.php
 * Methods shared by every alecsia entity.
 */

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
abstract class AlecsiaEntity {
   /* ============================== */
   /* Rights management              */
   /* ============================== */

   public function isUserAllowedToGet($user) {
      return true;
   }

   public function isUserAllowedToAdd($user) {
      return true;
   }

   public function isUserAllowedToUpdate($user) {
      return true;
   }

   public function isUserAllowedToDelete($user) {
      return true;
   }

   /* ============================== */
   /* Id                             */
   /* ============================== */

   /**
    * @var integer $id
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   protected $id;

   public function getId() {
      return $this->id;
   }

   /* ============================== */
   /* Equality                       */
   /* ============================== */

   public function equals(AlecsiaEntity $another) {
      return $this->getId() == $another->getId();
   }

}
