<?php

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Alecsia\AnnotationBundle\Entity\Exceptions\RenduNonTermineException;
use Alecsia\AnnotationBundle\Entity\Exceptions\LectureSeuleException;

/**
 * Alecsia\AnnotationBundle\Entity\Rendu
 *
 * @ORM\Entity(repositoryClass="Alecsia\AnnotationBundle\Entity\RenduRepository")
 */
class Rendu extends AlecsiaEntity {

   const NO_MARK = -999;
   const MAX_EXTENSION_LENGTH = 8;

   /**
    * @var integer $id
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   protected $id;

   /**
    * @var string $nom
    *
    * @ORM\Column(name="nom", type="string", length=255)
    */
   protected $nom;

   /**
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\Sujet", inversedBy="rendus")
    */
   protected $sujet;

   /**
    * @var integer $note
    *
    * @ORM\Column(name="note", type="float", nullable=true)
    */
   protected $note;

   /**
    * @var string $dossier
    *
    * @ORM\Column(name="dossier", type="string", length=255)
    */
   protected $dossier;

   /**
    * @ORM\Column(name="termine", type="boolean")
    */
   protected $termine = false;

   /**
    * @ORM\OneToMany(targetEntity="Alecsia\AnnotationBundle\Entity\Annotation", mappedBy="rendu", cascade={"persist", "remove"})
    */
   protected $annotations;

   /**
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\Group")
    * @ORM\JoinColumn(name="group_id", referencedColumnName="id", onDelete="CASCADE")
    */
   protected $group;

   /**
    * @ORM\OneToMany(targetEntity="Alecsia\AnnotationBundle\Entity\Fichier", mappedBy="rendu", cascade={"persist", "remove"})
    */
   protected $fichiers;

   /**
    * @ORM\ManyToMany(targetEntity="Alecsia\AnnotationBundle\Entity\Student", inversedBy="works", cascade={"persist"})
    */
   protected $students;

   /**
    * @var string $camouflaging
    *
    * @ORM\Column(name="camouflaging", type="string", length=255)
    */
   protected $camouflaging;

   // Constructeur & ToString

   public function __construct($nom, $sujet, $group) {
      $this->nom = $nom;
      $this->sujet = $sujet;
      $this->group = $group;
      $this->students = new \Doctrine\Common\Collections\ArrayCollection();
      $this->camouflaging = '';
   }

   public function __toString() {
      return "(" . $this->getId() . ") " . $this->getNom();
   }

   /* ============================== */
   /* Students                       */
   /* ============================== */

   public function hasStudent(Student $student) {
      return $this->students->contains($student);
   }

   /* ============================== */
   /* Get & Set                      */
   /* ============================== */

   public function getId() {
      return $this->id;
   }

   public function getStudents() {
      return $this->students;
   }

   public function addStudent(Student $student) {
      if ($this->students->contains($student))
         return;
      $this->students[] = $student;
      $student->addWork($this);
   }

   public function removeStudent(Student $student) {
      $this->students->removeElement($student);
      $student->removeWork($this);
   }

   public function getCoworkersOf(Student $student) {
      $students = $this->getStudents()->toArray();
      $is_me = function(Student $possibleCoworker) use ($student) {
         return !$possibleCoworker->equals($student);
      };
      return array_filter($students, $is_me);
   }

   public function setNom($nom) {
      $this->assertGel();
      $this->nom = $nom;
   }

   public function getNom() {
      return $this->nom;
   }

   /**
    * Try to remove the extension from the filename.
    * There is no guarantee.
    * @return the name without extension (beware it is computed each time the function is called)
    */
   public function getNameWithoutExtension() {
      /* Remove multi-extensions  */
      $possible_extensions = array('.tar.gz', '.tar.bz2');
      $count = 0;
      $workName = str_ireplace($possible_extensions, '', $this->nom, $count);
      if ($count == 0) {
         /* Nothing replaced. Remove extension */
         /* Find the . */
         if (($pos = strrpos($workName, '.')) !== false && strlen($workName) - $pos - 1 <= self::MAX_EXTENSION_LENGTH)
            $workName = substr($workName, 0, $pos);
      }

      return $workName;
   }

   /** @return Sujet */
   public function getSujet() {
      return $this->sujet;
   }

   public function setSujet(Sujet $sujet) {
      $this->sujet = $sujet;
   }

   public function getGroup() {
      return $this->group;
   }

   public function setGroup($group) {
      $this->group = $group;
   }

   public function getTermine() {
      return $this->termine;
   }

   public function setTermine($termine) {
      $this->assertGel();
      $this->termine = $termine;
   }

   public function getNote() {
      return $this->note;
   }

   public function setNote($note) {
      $this->assertGel();
      $this->note = $note;
   }

   public function getAnnotations() {
      return $this->annotations;
   }

   public function addAnnotation($annotation) {
      $this->assertGel();
      $this->annotations[] = $annotation;
      $annotation->setRendu($this);
   }

   public function getFichiers() {
      return $this->fichiers;
   }

   public function addFichier($fichier) {
      $this->fichiers[] = $fichier;
      $fichier->setRendu($this);
   }

   public function getDossier() {
      return $this->dossier;
   }

   public function setDossier($dossier) {
      if ($this->dossier == null) {
         $this->dossier = $dossier;
      }
   }

   public function getCamouflaging() {
      return $this->camouflaging;
   }

   public function setCamouflaging($cam) {
      $this->camouflaging = $cam;
   }

   // Assure que le rendu est modifiable
   public function assertGel() {
      if ($this->getSujet()->isFrozen()) {
         throw new LectureSeuleException();
      }
   }

   // Mutateurs
   // Ajoute une annotation au rendu afin de la faire correspondre
   // à la note en paramètre.
   // Cette annotation est rendue incomplète:
   // - On doit encore lui attribuer un nom
   // - On doit la faire persister
   public function ajusterNote($nouvellenote) {
      $noteactuelle = $this->getNote();
      if ($noteactuelle == self::NO_MARK) {
         return null;
      }
      $difference = $nouvellenote - $noteactuelle;

      if ($difference != 0) {
         $this->setNote($nouvellenote);

         $annotation = new Annotation();
         $annotation->setValeurLitterale($difference);
         $this->addAnnotation($annotation);
         return $annotation;
      } else {
         return null;
      }
   }

   // Throws RenduNonTermineException
   public function geler() {
      if (!$this->getTermine()) {
         throw new RenduNonTermineException();
      }

      foreach ($this->annotations as $annotation) {
         $annotation->desolidariser();
      }
   }

   /* ============================== */
   /* Rights management              */
   /* ============================== */

   public function isUserAllowedToGet($user) {
      return ($this->isCorrectedByTeacher($user) ||
              ($user->isStudent() && $this->getSujet()->isConsultable() && $this->hasStudent($user))) && $this->getGroup()->isUserAllowedToGet($user);
   }

   public function isUserAllowedToAdd($user) {
      return $this->isCorrectedByTeacher($user);
   }

   public function isUserAllowedToUpdate($user) {
      return $this->isCorrectedByTeacher($user);
   }

   public function isUserAllowedToDelete($user) {
      return $this->isCorrectedByTeacher($user);
   }

   private function isCorrectedByTeacher($teacher) {
      return ($teacher instanceof Teacher) && ($this->getSujet()->getUE()->getOwner()->equals($teacher) || $this->getGroup()->getOwner()->equals($teacher));
   }

}
