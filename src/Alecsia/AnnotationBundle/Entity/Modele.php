<?php

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alecsia\AnnotationBundle\Entity\Modele
 *
 * @ORM\Table(name="Modele")
 * @ORM\Entity(repositoryClass="Alecsia\AnnotationBundle\Entity\ModeleRepository")
 */
class Modele {
   /* =================
     = Champs entité =
     ================= */

   /**
    * @var integer $id
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   protected $id;

   /**
    * @var string $nom
    *
    * @ORM\Column(name="nom", type="string", length=255)
    */
   protected $nom;

   /**
    * @var text $commentaire
    *
    * @ORM\Column(name="commentaire", type="text")
    */
   protected $commentaire;

   /**
    * @var float $valeur
    *
    * @ORM\Column(name="valeur", type="float")
    */
   protected $valeur;

   /**
    * @var boolean $relatif
    *
    * @ORM\Column(name="relatif", type="boolean")
    */
   protected $relatif;

   /**
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\Exercice", inversedBy="modeles")
    */
   protected $exercice;

   /**
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\ListeModeles", inversedBy="modeles")
    */
   protected $liste;

   /* ================
     = Constructeur =
     ================ */

   function __construct($nom = null, $comm = null, $val = null, $exercice = null, $liste = null) {
      $this->nom = $nom;
      $this->commentaire = $comm;
      $this->setValeurLitterale($val);
      $this->exercice = $exercice;
      $this->liste = $liste;
   }

   /* ===========
     = Helpers =
     =========== */

   public function getPole() {
      if ($this->getValeur() > 0) {
         return "positif";
      } else if ($this->getValeur() < 0) {
         return "negatif";
      } else {
         return "neutre";
      }
   }

   /* =====================
     = Getters & Setters =
     ===================== */

   public function getId() {
      return $this->id;
   }

   public function getNom() {
      return $this->nom;
   }

   public function setNom($nom) {
      $this->nom = $nom;
   }

   public function getCommentaire() {
      return $this->commentaire;
   }

   public function setCommentaire($commentaire) {
      $this->commentaire = $commentaire;
   }

   public function getExercice() {
      return $this->exercice;
   }

   public function setExercice($exercice) {
      $this->exercice = $exercice;
   }

   public function getValeur() {
      return $this->valeur;
   }

   public function getRelatif() {
      return $this->relatif;
   }

   public function getListe() {
      return $this->liste;
   }

   public function setListe($liste) {
      $this->liste = $liste;
   }

   // Retourne une chaine représentant la valeur de l'annotation
   public function getValeurFormatee() {
      $valeur = $this->getValeur();
      $prefixe = "";
      $suffixe = "";

      if ($valeur > 0) {
         $prefixe = "+";
      }

      if ($this->getRelatif() == true) {
         $suffixe = " %";
         $valeur = $valeur * 100;
      }


      return $prefixe . $valeur . $suffixe;
   }

   public function setValeurLitterale($litterale) {

      // Valeur littérale non affectée
      if ($litterale == null) {
         $this->valeur = 0;
         $this->relatif = false;
         return;
      }

      // Passage Virgules => Points
      $litterale = preg_replace("/,/", ".", $litterale);

      // Retrait des espaces
      $litterale = preg_replace("/ /", "", $litterale);

      // Transfo "-20%" => "-0,2"
      $isRelatif = strpos($litterale, '%') !== false;
      $floatval = floatval($litterale);

      if ($isRelatif) {
         $valeur = $floatval / 100;
      } else {
         $valeur = $floatval;
      }


      $valeur = (float) number_format($valeur, 2);

      // Stockage infos
      $this->valeur = $valeur;
      $this->relatif = $isRelatif;
   }

   /* =========
     = Clone =
     ========= */

   public function cloner($exercice, $liste) {
      $m = new Modele(
              $this->getNom(), $this->getCommentaire(), $this->getValeurFormatee(), $exercice, $liste
      );

      return $m;
   }

}
