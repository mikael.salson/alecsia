<?php

namespace Alecsia\AnnotationBundle\Entity;

/**
 * Author: Ludovic Loridan
 * Date: 24/12/12
 * Time: 20:16
 *
 * UERepository.php
 *
 */
use Doctrine\ORM\EntityRepository;

class UERepository extends EntityRepository {

   public function getListOfActiveUEs(Teacher $user) {
      $query = $this->listOfActiveUEsQuery($user);
      return $query->getResult();
   }

   public function getListOfArchivedUEs(Teacher $user) {
      $query = $this->listOfArchivedUEsQuery($user);
      return $query->getResult();
   }

   /**
    * @return The UE linked to this model iff it is a UE-wide model,
    *         null otherwise.
    */
   public function findOneByModel(Modele $model) {
      $query = $this->getEntityManager()
              ->createQuery("SELECT ue FROM AnnotationBundle:UE ue,
                                      AnnotationBundle:ListeModeles lm,
                                      AnnotationBundle:Modele m
                       WHERE m = :model
                       AND m.liste = lm
                       AND ue.model_list = lm")
              ->setParameter('model', $model);

      try {
         return $query->getSingleResult();
      } catch (\Doctrine\ORM\NoResultException $e) {
         return null;
      }
   }

   /* ============================== */
   /* Queries Strings                */
   /* ============================== */

   // Returns Query
   protected function listOfActiveUEsQuery(Teacher $user) {
      return $this->listOfUEsQuery(false, $user);
   }

   protected function listOfArchivedUEsQuery(Teacher $user) {
      return $this->listOfUEsQuery(true, $user);
   }

   // Returns Query
   protected function listOfUEsQuery($archived = false, Teacher $user) {
      $stringQuery = "SELECT ue
                     FROM AnnotationBundle:UE ue
                     , AnnotationBundle:Group grp
                     WHERE ue.archived = :archived
                     AND (ue.owner = :owner
                          OR (grp.UE = ue
                              AND grp.owner = :owner))";

      $query = $this->getEntityManager()->createQuery($stringQuery);
      $query->setParameter("archived", $archived);
      $query->setParameter("owner", $user);

      return $query;
   }

}
