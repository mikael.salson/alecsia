<?php

/**
 * Author: Ludovic Loridan
 * Date: 29/01/13
 * Time: 17:46
 *
 * AlecsiaUser.php
 * Any entity that can be returned by an User Provider should be an Alecsia User.
 */

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Role\Role;

/**
 * @ORM\Entity()
 * @ORM\Table(name="AlecsiaUser")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"teacher" = "Teacher", "student" = "Student"})
 */
abstract class AlecsiaUser implements UserInterface {

   /**
    * @var integer $id
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   protected $id;

   // ---- Basic infos ----

   /**
    * @var string $nom
    *
    * @ORM\Column(name="login", type="string", length=80)
    * @Assert\NotBlank()
    */
   protected $login;

   /**
    * @ORM\Column(name="firstName", type="string", length=80, nullable=true)
    */
   protected $firstName;

   /**
    * @ORM\Column(name="lastName", type="string", length=80, nullable=true)
    */
   protected $lastName;

   /**
    * @ORM\Column(name="email", type="string", length=80, nullable=true)
    */
   protected $email;
   protected $password = '';

   /* ============================== */
   /* Display name                   */
   /* ============================== */

   /**
    * A display name destined to be used by Alecsia,
    * to the user who is using it.
    * Returns the first name, or the login if not present.
    */
   public function getFriendlyDisplayName() {
      if (!empty($this->firstName)) {
         return $this->getFirstName();
      } else {
         return $this->getLogin();
      }
   }

   /**
    * A display name useful when the absolute identification
    * of the teacher is important.
    * Return FirstN + LastN or login
    */
   public function getAbsoluteDisplayName() {
      if (!(empty($this->firstName)) && !empty($this->lastName)) {
         return sprintf('%s %s', $this->getFirstName(), $this->getLastName());
      } else {
         return $this->getLogin();
      }
   }

   public function __toString() {
      return $this->getLogin();
   }

   /* ============================== */
   /* Type                           */
   /* ============================== */

   public function isTeacher() {
      return $this instanceof Teacher;
   }

   public function isStudent() {
      return $this instanceof Student;
   }

   /* ============================== */
   /* Get & Set                      */
   /* ============================== */

   /**
    * @return int
    */
   public function getId() {
      return $this->id;
   }

   /**
    * @param string $login
    */
   public function setLogin($login) {
      $this->login = $login;
   }

   /**
    * @return string
    */
   public function getLogin() {
      return $this->login;
   }

   public function setEmail($email) {
      $this->email = $email;
   }

   public function getEmail() {
      return $this->email;
   }

   public function setFirstName($firstName) {
      $this->firstName = $firstName;
   }

   public function getFirstName() {
      return $this->firstName;
   }

   public function setLastName($lastName) {
      $this->lastName = $lastName;
   }

   public function getLastName() {
      return $this->lastName;
   }

   /* ============================== */
   /* User interface                 */
   /* ============================== */

   /**
    * Returns the roles granted to the user.
    *
    * @return Role[] The user roles
    */
   function getRoles() {
      return array();
   }

   /**
    * Returns the password used to authenticate the user.
    *
    * @return string The password
    */
   function getPassword() {
      return $this->password;
   }

   /**
    * Returns the salt.
    *
    * @return string The salt
    */
   function getSalt() {
      return '';
   }

   /**
    * Returns the username used to authenticate the user.
    *
    * @return string The username
    */
   public function getUsername() {
      return $this->getLogin();
   }

   /**
    * Removes sensitive data from the user.
    *
    * @return void
    */
   function eraseCredentials() {
      
   }

   function setPassword($password) {
      $this->password = $password;
   }

   /**
    * The equality comparison should neither be done by referential equality
    * nor by comparing identities (i.e. getId() === getId()).
    *
    * However, you do not need to compare every attribute, but only those that
    * are relevant for assessing whether re-authentication is required.
    *
    * @param UserInterface $user
    *
    * @return Boolean
    */
   function equals(UserInterface $user) {
      return $user->getUsername() == $this->getUsername();
   }

}
