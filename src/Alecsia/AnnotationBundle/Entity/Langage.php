<?php

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alecsia\AnnotationBundle\Entity\Langage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Alecsia\AnnotationBundle\Entity\LangageRepository")
 */
class Langage {

   /**
    * @var integer $id
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   protected $id;

   /**
    * @var string $id_geshi
    *
    * @ORM\Column(name="id_geshi", type="string", length=255)
    */
   protected $id_geshi;

   /**
    * @var string $nom
    *
    * @ORM\Column(name="nom", type="string", length=255)
    */
   protected $nom;

   function __construct($id_geshi, $nom) {
      $this->id_geshi = $id_geshi;
      $this->nom = $nom;
   }

   /**
    * Get id
    *
    * @return integer
    */
   public function getId() {
      return $this->id;
   }

   /**
    * Set id_geshi
    *
    * @param string $idGeshi
    */
   public function setIdGeshi($idGeshi) {
      $this->id_geshi = $idGeshi;
   }

   /**
    * Get id_geshi
    *
    * @return string
    */
   public function getIdGeshi() {
      return $this->id_geshi;
   }

   /**
    * Set nom
    *
    * @param string $nom
    */
   public function setNom($nom) {
      $this->nom = $nom;
   }

   /**
    * Get nom
    *
    * @return string
    */
   public function getNom() {
      return $this->nom;
   }

}
