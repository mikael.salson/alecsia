<?php

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Alecsia\AnnotationBundle\Service\NumberEncrypter;

/**
 * Alecsia\AnnotationBundle\Entity\Group
 *
 * @ORM\Table(name="`Group`")
 * @ORM\Entity(repositoryClass="Alecsia\AnnotationBundle\Entity\GroupRepository")
 */
class Group extends AlecsiaEntity {

   /**
    * @var integer $id
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   protected $id;

   /**
    * @var string $name
    * @ORM\Column(name="`name`", type="string", length=255)
    */
   protected $name;

   /**
    * @var Teacher
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\Teacher")
    * @Assert\NotBlank()
    */
   protected $owner;
   protected $shareCode;

   /**
    * @var ArrayCollection
    * @ORM\ManyToMany(targetEntity="Alecsia\AnnotationBundle\Entity\Student", inversedBy="groups", cascade={"persist"})
    */
   protected $students;

   /**
    * @var UE $UE
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\UE", inversedBy="groups")
    * @Assert\NotBlank
    */
   protected $UE;

   /**
    * @var boolean $viewable
    * @ORM\Column(type="boolean")
    */
   protected $viewable = true;

   public function __construct($name = '', UE $ue = null, Teacher $owner = null) {
      if (!empty($name))
         $this->name = $name;
      if (!is_null($ue))
         $this->UE = $ue;
      if (!is_null($owner))
         $this->owner = $owner;
      $this->students = new ArrayCollection();
   }

   /* ============================== */
   /* Share code                     */
   /* ============================== */

   public function getShareCode() {
      if (is_null($this->shareCode)) {
         $this->generateShareCode();
      }
      return $this->shareCode;
   }

   protected function generateShareCode() {
      if (is_numeric($this->getId())) {
         $encrypter = new NumberEncrypter();
         $this->shareCode = $encrypter->encryptNumber($this->getId());
      }
   }

   /* ============================== */
   /* Properties get/set             */
   /* ============================== */

   public function getName() {
      return $this->name;
   }

   public function setName($name) {
      return $this->name = $name;
   }

   public function addStudent(Student $student) {
      $this->students[] = $student;
      $student->addGroup($this);
      return $this;
   }

   public function removeStudent(Student $student) {
      $this->students->removeElement($student);
      $student->removeGroup($this);
   }

   public function getStudents() {
      return $this->students;
   }

   public function hasStudent($student) {
      return $this->students->contains($student);
   }

   public function getUE() {
      return $this->UE;
   }

   public function setUE(UE $ue) {
      $this->UE = $ue;
   }

   public function setOwner($owner) {
      $this->owner = $owner;
   }

   /** @return Teacher */
   public function getOwner() {
      return $this->owner;
   }

   public function isViewable() {
      return $this->viewable;
   }

   public function setViewable($view) {
      return $this->viewable = $view;
   }

   /* ============================== */
   /* Right managements              */
   /* ============================== */

   public function isUserAllowedToGet($user) {
      return $this->isUserOwner($user) || $this->getUE()->isUserOwner($user) || ($user->isStudent() && $this->isViewable());
   }

   public function isUserAllowedToAdd($user) {
      return $user->isTeacher();
   }

   public function isUserAllowedToUpdate($user) {
      return $this->isUserOwner($user) || $this->getUE()->isUserOwner($user);
   }

   public function isUserAllowedToDelete($user) {
      return $this->isUserOwner($user) || $this->getUE()->isUserOwner($user);
   }

   public function isUserOwner($user) {
      return ($user instanceof AlecsiaUser) && $user->equals($this->owner);
   }

   /*    * *********************************************** */
   /* Static methods */

   public static function getDefaultGroup() {
      $group = new Group();
      $group->setName('');
      return $group;
   }

}
