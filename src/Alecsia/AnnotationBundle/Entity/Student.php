<?php

/**
 * Author: Ludovic Loridan
 * Date: 27/01/13
 * Time: 14:06
 *
 * Student.php
 * Hold a student.
 */

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Role\Role;

/**
 * @ORM\Entity(repositoryClass="Alecsia\AnnotationBundle\Entity\StudentRepository")
 */
class Student extends AlecsiaUser {

   /**
    * @var ArrayCollection
    * @ORM\ManyToMany(targetEntity="Alecsia\AnnotationBundle\Entity\Group", mappedBy="students", cascade={"persist"})
    * @ORM\OrderBy({"name" = "ASC"})
    */
   protected $groups;

   /**
    * @var ArrayCollection
    * @ORM\ManyToMany(targetEntity="Alecsia\AnnotationBundle\Entity\Rendu", mappedBy="students", cascade={"persist"})
    * @ORM\OrderBy({"nom" = "ASC"})
    */
   protected $works;

   /**
    * @ORM\Column(name="registered", type="boolean")
    */
   protected $registered = true;

   function __construct() {
      $this->groups = new ArrayCollection();
   }

   public function getWorksByUEs() {
      $works = $this->getWorks();
      $res = array();
      foreach ($works as $work) {
         $this->putWorkIntoAssociativeArray($work, $res);
      }
      return $res;
   }

   protected function putWorkIntoAssociativeArray(Rendu $work, &$array) {
      $ueId = $work->getSujet()->getUE()->getId();
      if (!isset($array[$ueId])) {
         $array[$ueId] = array();
      }
      $array[$ueId][] = $work;
   }

   /* ============================== */
   /* Factories                      */
   /* ============================== */

   /**
    * Takes an associative array whose fields login, firstName, lastName and email
    * contain the corresponding informations for the student.
    * An optional field registered can be set to a boolean true or false.
    * @return an object $o, where $o->getLogin == $map['login'],
    * $o->getFirstName() == $map['firstName'], $o->getLastName() == $map['lastName']
    * $o->getEmail() == $map['email'].
    */
   public static function studentFromMap($map) {
      $student = new Student();
      $student->setLogin($map["login"]);
      $student->setFirstName($map["firstName"]);
      $student->setLastName($map["lastName"]);
      $student->setEmail($map["email"]);
      if (isset($map['registered'])) {
         $student->setRegistered($map['registered']);
      }
      return $student;
   }

   public static function studentFromLogin($login) {
      $student = new Student();
      $student->setLogin($login);
      return $student;
   }

   /* ============================== */
   /* Roles & Auth                   */
   /* ============================== */

   /**
    * Returns the roles granted to the user.
    *
    * @return Role[] The user roles
    */
   function getRoles() {
      return array(new Role("ROLE_STUDENT"));
   }

   /* ============================== */
   /* Get/set/add                    */
   /* ============================== */

   /**
    * @return \Alecsia\AnnotationBundle\Entity\Group
    */
   public function getGroups() {
      return $this->groups;
   }

   public function addGroup(Group $group) {
      $this->groups[] = $group;
      return $this;
   }

   public function removeGroup(Group $group) {
      $this->groups->removeElement($group);
   }

   /**
    * @return \Alecsia\AnnotationBundle\Entity\Rendu
    */
   public function getWorks() {
      return $this->works;
   }

   // Use Works (Rendus) methods instead, since Work is the owner of the relationship.
   public function addWork(Rendu $work) {
      $this->works[] = $work;
      return $this;
   }

   public function removeWork(Rendu $work) {
      $this->works->removeElement($work);
      return $this;
   }

   public function isRegistered() {
      return $this->registered;
   }

   public function setRegistered($is_reg) {
      $this->registered = $is_reg;
   }

}
