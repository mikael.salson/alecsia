<?php

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alecsia\AnnotationBundle\Entity\RegleTypeFichier
 *
 * @ORM\Table(name="RegleTypeFichier")
 * @ORM\Entity
 */
class RegleTypeFichier extends AlecsiaEntity {

   /**
    * @var integer $id
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   protected $id;

   /**
    * @var string $regex
    *
    * @ORM\Column(name="regex", type="string", length=255)
    */
   protected $regex;

   /**
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\Langage")
    */
   protected $langage;

   /**
    * @var integer $priorite
    *
    * @ORM\Column(name="priorite", type="integer")
    */
   protected $priorite;

   /**
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\AlecsiaUser")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
    */
   protected $user;

   function __construct($regex = "", $langage = null, $priorite = 0, $user = NULL) {
      $this->regex = $regex;
      $this->langage = $langage;
      $this->priorite = $priorite;
      $this->user = $user;
   }

   /**
    * Get id
    *
    * @return integer
    */
   public function getId() {
      return $this->id;
   }

   public function getRegex() {
      return $this->regex;
   }

   public function setRegex($regex) {
      $this->regex = $regex;
   }

   public function getLangage() {
      return $this->langage;
   }

   public function setLangage($langage) {
      $this->langage = $langage;
   }

   public function getPriorite() {
      return $this->priorite;
   }

   public function setPriorite($priorite) {
      $this->priorite = $priorite;
   }

   public function getUser() {
      return $this->user;
   }

   public function setUser($user) {
      $this->user = $user;
   }

}
