<?php

namespace Alecsia\AnnotationBundle\Entity\Exceptions;

// Déclenché quand on tente de supprimer une promotion en cours

class PromoEnCoursException extends \Exception {

   public function __construct($message = NULL, $code = 0) {
      parent::__construct($message, $code);
   }

}

?>