<?php

namespace Alecsia\AnnotationBundle\Entity\Exceptions;

// Déclenché quand on tente de modifier une entité gelée

class LectureSeuleException extends \Exception {

   public function __construct($message = NULL, $code = 0) {
      parent::__construct($message, $code);
   }

}

?>