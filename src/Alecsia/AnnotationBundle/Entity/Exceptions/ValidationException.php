<?php

namespace Alecsia\AnnotationBundle\Entity\Exceptions;

/**
 * Thrown when an entity is not validated
 */
class ValidationException extends \Exception {

   public function __construct($validExceptions, $code = 0) {
      $message = '';
      foreach ($validExceptions as $except) {
         $message = $except->getPropertyPath() . ': ' . $except->getMessage() . "\n";
      }
      parent::__construct($message, $code);
   }

}

?>