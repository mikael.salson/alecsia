<?php

namespace Alecsia\AnnotationBundle\Entity\Exceptions;

// Déclenché quand on tente de créer une entité au nom vide.

class NomVideException extends \Exception {

   public function __construct($message = NULL, $code = 0) {
      parent::__construct($message, $code);
   }

}

?>