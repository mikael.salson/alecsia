<?php

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Alecsia\AnnotationBundle\Entity\Exceptions\RenduNonTermineException;
use Alecsia\AnnotationBundle\Entity\Exceptions\LectureSeuleException;

/**
 * Alecsia\AnnotationBundle\Entity\Sujet
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Alecsia\AnnotationBundle\Entity\SujetRepository")
 */
class Sujet {

   const STATUS_WAIT = 'wait';
   const STATUS_MARK = 'mark';
   const STATUS_CONSULT = 'cnf';
   const STATUS_FROZEN = 'frozen';

   /**
    * @var integer $id
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   protected $id;

   /**
    * @var string $nom
    *
    * @ORM\Column(name="nom", type="string", length=255)
    */
   protected $nom;

   /**
    * @var boolean $is_TD
    *
    * @ORM\Column(name="is_TD", type="boolean")
    */
   protected $is_TD;

   /**
    * @var integer $note_sur
    *
    * @ORM\Column(name="note_sur", type="integer")
    */
   protected $note_sur;

   /**
    * @var integer $startAt
    * What should be the initial mark for the works
    * This is generally useful either to start from 0 or from
    * the maximal mark (ie. note_sur).
    * @ORM\Column(name="noteInitiale", type="integer")
    */
   protected $noteInitiale;

   /**
    * @var float $moyenne
    *
    * @ORM\Column(name="moyenne", type="float", nullable=true)
    */
   protected $moyenne;

   /**
    * @var enum $status
    *
    * status can have four different values
    * wait: waiting for all the works to be included, and waiting to start marking
    *       process;
    * mark: starting to mark works;
    * cnf:  consultable but not frozen yet -> works have been marked but can still
    *       be modified, works are consultable by students
    * frozen: marks are frozen: works can be consulted by students but can't be
    *         modified anymore
    *
    * @ORM\Column(name="status", type="string", columnDefinition="ENUM('wait','mark','cnf','frozen')")
    */
   protected $status = self::STATUS_WAIT;

   /**
    * @ORM\OneToOne(targetEntity="Alecsia\AnnotationBundle\Entity\ListeModeles", cascade={"persist", "remove"})
    */
   protected $liste_modeles;

   /**
    * @var UE
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\UE", cascade={"persist"}, inversedBy="sujets")
    */
   protected $UE;

   /**
    * @ORM\OneToMany(targetEntity="Alecsia\AnnotationBundle\Entity\Rendu", mappedBy="sujet", cascade={"persist", "remove"})
    */
   protected $rendus;

   /**
    * @ORM\OneToMany(targetEntity="Alecsia\AnnotationBundle\Entity\Exercice", mappedBy="sujet", cascade={"persist", "remove"})
    */
   protected $exercices;

   /**
    * indicates the number of guts for lettre format 
    * if 0 value is shown as number else it will be shown as lettre 
    * @var type 
    * @ORM\Column(name="guts", type="integer")
    */
   protected $guts;

   function __construct($nom, $isTD = false, $status = self::STATUS_WAIT, $note_sur = 20, $noteInitiale = 0, $guts = 0) {
      $this->nom = $nom;
      $this->is_TD = $isTD;
      $this->status = $status;
      $this->note_sur = $note_sur;
      $this->noteInitiale = $noteInitiale;
      $this->guts = $guts;
   }

   /* ============================== */
   /* Students                       */
   /* ============================== */

   public function hasStudentWorkedOnIt(Student $student) {
      $workIsFromStudent = function($key, $work) use ($student) {
         return ($work instanceof Rendu) && ($work->hasStudent($student));
      };

      return $this->rendus->exists($workIsFromStudent);
   }

   // Getters & Setters
   public function getId() {
      return $this->id;
   }

   public function setNom($nom) {
      $this->assertGel();
      $this->nom = $nom;
   }

   public function getNom() {
      return $this->nom;
   }

   public function setIsTD($isTD) {
      $this->assertGel();
      $this->is_TD = $isTD;
   }

   public function getIsTD() {
      return $this->is_TD;
   }

   protected function setStatus($status) {
      $this->assertGel();
      $this->status = $status;
   }

   protected function getStatus() {
      return $this->status;
   }

   public function getNotesur() {
      return $this->note_sur;
   }

   public function setNotesur($note_sur) {
      $this->assertGel();
      $this->note_sur = $note_sur;
   }

   public function getListeModeles() {
      return $this->liste_modeles;
   }

   public function setListeModeles($liste_modeles) {
      $this->assertGel();
      $this->liste_modeles = $liste_modeles;
   }

   public function getRendus() {
      return $this->rendus;
   }

   public function getRendusRestants() {
      return $this->rendus->filter(function($rendu) {
                 return !($rendu->getTermine());
              });
   }

   public function getExercices() {
      return $this->exercices;
   }

   public function addExercice($exercice) {
      $this->assertGel();
      $this->exercices[] = $exercice;
      $exercice->setSujet($this);
   }

   /** @return UE */
   public function getUE() {
      return $this->UE;
   }

   public function setUE($UE) {
      $this->UE = $UE;
   }

   public function getMoyenne() {
      return $this->moyenne;
   }

   public function setMoyenne($moyenne) {
      $this->moyenne = $moyenne;
   }

   public function getNoteInitiale() {
      return $this->noteInitiale;
   }

   public function setNoteInitiale($noteInitiale) {
      $this->assertGel();
      $this->noteInitiale = $noteInitiale;
   }

   /*
    * Return an associative array whose key is a student identifier
    * and the value is the mark obtained.
    * @pre The marks must have been frozen.
    * @param group_id an array of group_ids, from which the marks must be taken.
    *                 the array can be null
    * @param all_marks: get all marks even if the subject is not frozen
    */

   public function getNotes($group_id = null, $all_marks = false) {
      $notes = array();
      if ($all_marks || $this->getStatus() == self::STATUS_FROZEN) {
         $rendus = $this->getRendus();

         foreach ($rendus as $rendu) {
            $etudiants = $rendu->getStudents();
            foreach ($etudiants as $etu) {
               if (!$group_id || in_array($rendu->getGroup()->getId(), $group_id)) {
                  $notes[] = array('student' => $etu, 'note' => $rendu->getNote(),
                      'group' => $rendu->getGroup());
               }
            }
         }
      }
      return $notes;
   }

   public function isFrozen() {
      return $this->getStatus() == self::STATUS_FROZEN;
   }

   public function isWaiting() {
      return $this->getStatus() == self::STATUS_WAIT;
   }

   public function isConsultable() {
      return (bool) ($this->getStatus() == self::STATUS_CONSULT || $this->getStatus() == self::STATUS_FROZEN);
   }

   public function isMarking() {
      return $this->getStatus() == self::STATUS_MARK;
   }

   /**
    * Does the mark can be displayed?
    * For the moment it is equivalent to the work is consultable
    * but the two events could be dissociated.
    */
   public function isMarkDisplayable() {
      return $this->isConsultable();
   }

   public function setFrozen() {
      $this->setStatus(self::STATUS_FROZEN);
   }

   public function setWaiting() {
      $this->setStatus(self::STATUS_WAIT);
   }

   public function setConsultable() {
      $this->setStatus(self::STATUS_CONSULT);
   }

   public function setMarking() {
      $this->setStatus(self::STATUS_MARK);
   }

   public function setMarkDisplayable() {
      $this->setStatus(self::STATUS_REVEAL_MARK);
   }

   // ToString
   public function __toString() {
      return "(" . $this->getId() . ") " . $this->getNom();
   }

   // Renvoie vrai ssi tous les rendus sont marqués comme terminés.
   public function tousTermines() {
      $toustermines = true;
      foreach ($this->rendus as $rendu) {
         $toustermines &= $rendu->getTermine();
      }
      return $toustermines;
   }

   // Assure que le rendu est modifiable
   public function assertGel() {
      if ($this->getStatus() == self::STATUS_FROZEN) {
         throw new LectureSeuleException();
      }
   }

   /* =============
     = Mutateurs =
     ============= */

   // Marque tous les rendus comme terminés
   public function toutTerminer() {
      foreach ($this->rendus as $rendu) {
         $rendu->setTermine(true);
      }
   }

   // Gele tous les rendus
   // Throws RenduNonTermineException
   public function geler() {
      if (!$this->tousTermines()) {
         throw new RenduNonTermineException();
      }

      foreach ($this->rendus as $rendu) {
         $rendu->geler();
      }

      $this->setFrozen();
   }

   /* =========
     = Copie =
     ========= */

   // Renvoie un nouveau sujet identique à celui ciisLettre()
   // à l'exception près des rendus.
   public function cloner($ue) {
      // Nouveau sujet
       $new_sujet = new Sujet($this->getNom(), $this->getIsTD(), self::STATUS_WAIT, $this->getNotesur(), $this->getNoteInitiale(), $this->getSubjectGuts());

      // Copie des exercices
      $old_exs = $this->getExercices();
      foreach ($old_exs as $old_ex) {
         $new_ex = $old_ex->cloner($new_sujet);
         $new_sujet->addExercice($new_ex);
      }

      // Copie de la liste de modèles
      $new_lm = $this->getListeModeles()->cloner($new_sujet->getExercices());
      $new_sujet->setListeModeles($new_lm);
      // Set UE
      $new_sujet->setUE($ue);


      return $new_sujet;
   }

   public function getSubjectGuts() {
      return $this->guts;
   }

   public function setSubjectGuts($guts_number) {
      $this->guts = $guts_number;
   }

   public function isLettre() {
      return $this->getSubjectGuts() !== 0;
   }

}
