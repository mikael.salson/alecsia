<?php

namespace Alecsia\AnnotationBundle\Entity;

/**
 * GroupRepository.php
 *
 */
use Doctrine\ORM\EntityRepository;

class GroupRepository extends EntityRepository {

   public function getListOfActiveGroupsInUE(UE $ue, Teacher $user) {
      $query = $this->listOfGroupsQuery(false, $user, false, $ue);
      return $query->getResult();
   }

   public function getListOfActiveGroups(Teacher $user, $onePerUE = true) {
      $query = $this->listOfActiveGroupsQuery($user, $onePerUE);
      return $query->getResult();
   }

   public function getListOfArchivedGroups(Teacher $user, $onePerUE = true) {
      $query = $this->listOfArchivedGroupsQuery($user, $onePerUE);
      return $query->getResult();
   }

   public function getDefaultGroupFrom(UE $ue, Teacher $user) {
      $query = $this->getDefaultGroupQuery($ue, $user);
      $result = $query->getResult();

      if (!$result || count($result) < 1)
         return null;

      return $result[0];
   }

   /* ============================== */
   /* Queries Strings                */
   /* ============================== */

   // Returns Query
   protected function getDefaultGroupQuery(UE $ue, AlecsiaUser $user) {
      $stringQuery = "SELECT grp
                     FROM AnnotationBundle:Group grp
                     JOIN grp.UE ue
                     WHERE ue.id = :ue_id
                     AND (ue.owner = :owner
                          OR (grp.UE = ue
                              AND grp.owner = :owner))
                     ORDER BY grp.id ASC";

      $query = $this->getEntityManager()->createQuery($stringQuery)->setMaxResults(1);
      $query->setParameter("owner", $user);
      $query->setParameter("ue_id", $ue->getId());
      return $query;
   }

   protected function listOfActiveGroupsQuery(Teacher $user, $onePerUE = true) {
      return $this->listOfGroupsQuery(false, $user, $onePerUE, null);
   }

   protected function listOfArchivedGroupsQuery(Teacher $user, $onePerUE = true) {
      return $this->listOfGroupsQuery(true, $user, $onePerUE, null);
   }

   // Returns Query
   /**
    * @param $archived: Are the groups among archived UEs?
    * @param $user: The user that owns the groups
    * @param $onePerUE: if true, only one group per UE is returned
    * @param $ue: if not null, just return the groups from that UE
    */
   protected function listOfGroupsQuery($archived = false, Teacher $user, $onePerUE, UE $ue) {
      $stringQuery = "SELECT grp
                     FROM AnnotationBundle:Group grp
                     JOIN grp.UE ue
                     WHERE ue.archived = :archived
                     AND grp.owner = :owner";

      if (!is_null($ue)) {
         $stringQuery .= 'AND ue.id = :ue_id';
      }

      if ($onePerUE) {
         $stringQuery .= ' GROUP BY ue';
      }

      $query = $this->getEntityManager()->createQuery($stringQuery);
      $query->setParameter("archived", $archived);
      $query->setParameter("owner", $user);
      if (!is_null($ue))
         $query->setParameter('ue_id', $ue->getId());

      return $query;
   }

}
