<?php

namespace Alecsia\AnnotationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Alecsia\AnnotationBundle\Entity\UE;

/**
 * Générateur de contenu "d'exemple" pour l'application.
 * Références ajoutées :
 */
class LoadUEs extends AbstractDataFixture implements OrderedFixtureInterface {

   private $data = array(
       array(
           "nom_court" => "PDC",
           "nom" => "Pratique du C",
           'year' => 2014,
           'owner' => 'REF_Teacher_admin',
           "ref" => "REF_UE_PDC",
           'ref_lm' => 'REF_LM_PDC'
       ),
       array(
           "nom_court" => "PDC",
           "nom" => "Pratique du C",
           'year' => 2013,
           'archived' => true,
           'owner' => 'REF_Teacher_admin',
           "ref" => "REF_UE_PDC_archived",
           'ref_lm' => 'REF_LM_PDC_archived'
       ),
       array(
           "nom_court" => "COMPIL",
           "nom" => "Compilation",
           'year' => 2014,
           'owner' => 'REF_Teacher2',
           "ref" => "REF_UE_COMPIL",
           'ref_lm' => 'REF_LM_COMPIL'
       )
   );

   public function doLoad(ObjectManager $manager) {
      foreach ($this->data as $u) {
         $ue = UE::UEFromArray($u, $manager->merge($this->getReference($u['owner'])));
         if (isset($u['archived'])) {
            $ue->setArchived($u['archived']);
         }

         $manager->persist($ue);
         $this->addReference($u["ref"], $ue);
         $this->addReference($u["ref"] . '_group', $ue->getGroups()->first());
         $this->addReference($u['ref_lm'], $ue->getModelList());
      }
      $manager->flush();
   }

   public function getOrder() {
      return 2;
   }

   public function getEnvironments() {
      return array('test');
   }

}
