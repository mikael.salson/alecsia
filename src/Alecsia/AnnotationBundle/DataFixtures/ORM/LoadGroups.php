<?php

namespace Alecsia\AnnotationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Alecsia\AnnotationBundle\Entity\Group;

/**
 * Générateur de contenu "d'exemple" pour l'application.
 * Références ajoutées :
 */
class LoadGroups extends AbstractDataFixture implements OrderedFixtureInterface {

   private $data = array(
       array(
           "owner" => "REF_Teacher_admin",
           "name" => "2",
           'UE' => 'REF_UE_PDC',
            'ref'   => 'REF_Group_PDC2',
            'students' => array('REF_Student3', 'REF_Student4')
       )
   );

   public function doLoad(ObjectManager $manager) {
      foreach ($this->data as $u) {
         $group = new Group($u['name'], $manager->merge($this->getReference($u['UE'])), $manager->merge($this->getReference($u['owner'])));

          foreach ($u['students'] as $student) {
              $group->addStudent($this->getReference($student));
          }

          $manager->persist($group);
          $this->addReference($u["ref"],$group);
      }
      $manager->flush();
   }

   public function getOrder() {
      return 3;
   }

   public function getEnvironments() {
      return array('test');
   }

}
