<?php

namespace Alecsia\AnnotationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Alecsia\AnnotationBundle\Entity\ListeModeles;

/**
 * Générateur de contenu "d'exemple" pour l'application.
 */
class LoadListeModeles extends AbstractDataFixture implements OrderedFixtureInterface {

    private $data = 
    array(
          array('ref' => 'REF_lm1'),
          array('ref' => 'REF_lm2'),
          array('ref' => 'REF_lm3')
   );

   public function doLoad(ObjectManager $manager) {
      foreach ($this->data as $m) {
         $lm = new ListeModeles();
         $manager->persist($lm);
         $this->addReference($m["ref"], $lm);
      }

      $manager->flush();
   }

   public function getOrder() {
      return 3; // the order in which fixtures will be loaded
   }

   public function getEnvironments() {
      return array('test');
   }

}
