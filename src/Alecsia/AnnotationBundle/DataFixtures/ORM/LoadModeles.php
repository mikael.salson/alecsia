<?php

namespace Alecsia\AnnotationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Alecsia\AnnotationBundle\Entity\Modele;
use Alecsia\AnnotationBundle\Entity\ListeModeles;

/**
 * Générateur de contenu "d'exemple" pour l'application.
 */
class LoadModeles extends AbstractDataFixture implements OrderedFixtureInterface {

   private $data = array(
       array(
           "exercice" => NULL,
           "nom" => "Plus de commentaires s'il te plait",
           "comm" => "Il est difficile de comprendre ton code en l'état. En rajoutant des commentaires traduisant les étapes de ta pensée, il aurait été plus facile de te corriger.",
           "val" => "- 5%",
           "ref" => "REF_modele1",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => NULL,
           "nom" => "Bonus/malus en PDC (TP1)",
           "comm" => "Beurk",
           "val" => "- 3",
           "ref" => "REF_modele1b",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => "REF_shello_1",
           "nom" => "Pas de printf ici",
           "comm" => "Il est inutile et couteux d'utiliser printf ici. Tu aurais du utiliser putchar() à la place.",
           "val" => "- 10%",
           "ref" => "REF_modele2",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => "REF_shello_1",
           "nom" => "Pas de scanf ici",
           "comm" => "Il est inutile et couteux d'utiliser scanf ici. Tu aurais pu utiliser getchar() à la place.",
           "val" => "- 10%",
           "ref" => "REF_modele3",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => "REF_shello_1",
           "nom" => "Ce test est inutile",
           "comm" => "Ce test est induit par ce que tu écris précédemment. Il est donc à retirer.",
           "val" => "- 20%",
           "ref" => "REF_modele4",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => "REF_shello_2",
           "nom" => "Valeur de retour volatile",
           "comm" => "Attention, tu retournes ici un pointeur que désalloué pas très loin après. Ton code peut provoquer une erreur de segmentation.",
           "val" => "- 30%",
           "ref" => "REF_modele5",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => "REF_shello_1",
           "nom" => "Code 1 pas fonctionnel",
           "comm" => "",
           "val" => "- 30%",
           "ref" => "REF_modele6",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => "REF_shello_2",
           "nom" => "Code 2 pas fonctionnel",
           "comm" => "",
           "val" => "- 30%",
           "ref" => "REF_modele7",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => "REF_shello_2",
           "nom" => "Pointeur jamais alloué",
           "comm" => "Tu renvoies un pointeur vers une zone jamais allouée, il est donc inutilisable.",
           "val" => "- 50%",
           "ref" => "REF_modele8",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => "REF_shello_3",
           "nom" => "Makefile introuvable",
           "comm" => "Il était demandé un makefile pour ce sujet, je ne le trouve pas.",
           "val" => "- 20%",
           "ref" => "REF_modele9",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => "REF_shello_3",
           "nom" => "Mauvaise commande pour phases.s",
           "comm" => "",
           "val" => "- 10%",
           "ref" => "REF_modele10",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => "REF_shello_3",
           "nom" => "Mauvaise commande pour phases.i",
           "comm" => "",
           "val" => "- 10%",
           "ref" => "REF_modele11",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => "REF_shello_3",
           "nom" => "Code 3 pas fonctionnel",
           "comm" => "",
           "val" => "- 20%",
           "ref" => "REF_modele12",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => "REF_shello_4",
           "nom" => "Mauvais cas de base dans ta factorielle récursive !",
           "comm" => "Ta factorielle bouclera sur fact(-1).",
           "val" => "- 30%",
           "ref" => "REF_modele13",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => "REF_shello_4",
           "nom" => "Calculs inutiles",
           "comm" => "Tu pouvais éviter de faire ce calcul puisqu'il est déjà fait implicitement par ta boucle",
           "val" => "- 20%",
           "ref" => "REF_modele14",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => "REF_shello_4",
           "nom" => "Code 4 pas fonctionnel",
           "comm" => "",
           "val" => "- 40%",
           "ref" => "REF_modele15",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => "REF_shello_4",
           "nom" => "Etrange implémentation...",
           "comm" => "",
           "val" => "- 20%",
           "ref" => "REF_modele16",
           "liste" => "REF_lm1"
       ),
       array(
           "exercice" => NULL,
           "nom" => "Makefile manquant",
           "comm" => "L'oubli de Makefile devrait être passible de la peine de mort.",
           "val" => "- 100%",
           "ref" => "REF_modele17",
           "liste" => "REF_LM_PDC"
       ),
       array(
           "exercice" => NULL,
           "nom" => "Login dans le nom de l'archive",
           "comm" => "Votre login doit toujours être présent dans le nom de l'archive.",
           "val" => "- 100%",
           "ref" => "REF_modele18",
           "liste" => "REF_ML_Teacher2"
       )
   );

   public function doLoad(ObjectManager $manager) {
      foreach ($this->data as $m) {

         // Récup de la liste, ou création, en fonction
         if (!($this->hasReference($m["liste"]))) {
            $liste = new ListeModeles();
            $manager->persist($liste);
            $this->addReference($m["liste"], $liste);
         } else {
            $liste = $manager->merge($this->getReference($m["liste"]));
         }

         $exo = null;
         if ($m['exercice'] != null)
             $exo = $this->getReference($m['exercice']);

         $modele = new Modele(
                 $m["nom"], $m["comm"], $m["val"], $exo, $liste
         );

         $manager->persist($modele);
         $this->addReference($m["ref"], $modele);
      }

      $manager->flush();
   }

   public function getOrder() {
      return 6; // the order in which fixtures will be loaded
   }

   public function getEnvironments() {
      return array('test');
   }

}
