<?php

namespace Alecsia\AnnotationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Alecsia\AnnotationBundle\Entity\Fichier;

/**
 * Générateur de contenu "d'exemple" pour l'application.
 * Références ajoutées :
 * - helloworld.c
 * - drive.c
 * - drive.h
 * - lambda.ml
 */
class LoadFichiers extends AbstractDataFixture implements OrderedFixtureInterface {

   private $data = array(
           // Rendu de McCartney
           // array(
           //     "dossier" => "1",
           //     "nom_fichier" => "helloworld.c",
           //     "rendu" => "REF_rendu_mccartney",
           //     "langage" => "REF_langage_C",
           //     "exclus" => false,
           //     "ref" => "REF_helloworld.c"
           // ),
           // // Rendu de Lennon
           // array(
           //     "dossier" => "2",
           //     "nom_fichier" => "drive.c",
           //     "rendu" => "REF_rendu_lennon",
           //     "langage" => "REF_langage_C",
           //     "exclus" => false,
           //     "ref" => "REF_drive.c"
           // ),
           // array(
           //     "dossier" => "2",
           //     "nom_fichier" => "drive.c~",
           //     "rendu" => "REF_rendu_lennon",
           //     "langage" => "REF_langage_C",
           //     "exclus" => true,
           //     "ref" => "REF_drive~.c"
           // ),
           // array(
           //     "dossier" => "2",
           //     "nom_fichier" => "drive.h",
           //     "rendu" => "REF_rendu_lennon",
           //     "langage" => "REF_langage_C",
           //     "exclus" => false,
           //     "ref" => "REF_drive.h"
           // ),
           // // Rendu de Butler
           // array(
           //     "dossier" => "3",
           //     "nom_fichier" => "lambda.ml",
           //     "rendu" => "REF_rendu_butler",
           //     "langage" => "REF_langage_OCaml",
           //     "exclus" => false,
           //     "ref" => "REF_lambda.ml"
           // )
   );

   public function doLoad(ObjectManager $manager) {
      foreach ($this->data as $f) {
         $fichier = new Fichier(
                 $f["dossier"], $f["nom_fichier"], $manager->merge($this->getReference($f["rendu"])), $manager->merge($this->getReference($f["langage"])), $f["exclus"]
         );
         $manager->persist($fichier);
         $this->addReference($f["ref"], $fichier);
      }
      $manager->flush();
   }

   public function getOrder() {
      return 6; // the order in which fixtures will be loaded
   }

   public function getEnvironments() {
      return array('test');
   }

}
