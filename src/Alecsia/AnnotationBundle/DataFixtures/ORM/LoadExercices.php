<?php

namespace Alecsia\AnnotationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Alecsia\AnnotationBundle\Entity\Exercice;

/**
 * Générateur de contenu "d'exemple" pour l'application.
 * Références ajoutées :
 */
class LoadExercices extends AbstractDataFixture implements OrderedFixtureInterface {

   private $data = array(
       array(
           "sujet" => "REF_sujet_helloworld",
           "numero" => 1,
           "nom" => "Affichez donc quelque chose en C",
           "valeur" => 4,
           'initial' => 20,
           "ref" => "REF_shello_1"
       ),
       array(
           "sujet" => "REF_sujet_helloworld",
           "numero" => 2,
           "nom" => "Valeurs de retour",
           "valeur" => 4,
           'initial' => 20,
           "ref" => "REF_shello_2"
       ),
       array(
           "sujet" => "REF_sujet_helloworld",
           "numero" => 3,
           "nom" => "Essai machin",
           "valeur" => 4,
           'initial' => 20,
           "ref" => "REF_shello_3"
       ),
       array(
           "sujet" => "REF_sujet_helloworld",
           "numero" => 4,
           "nom" => "Truc lambda",
           "valeur" => 4,
           'initial' => 20,
           "ref" => "REF_shello_4"
       ),
       array(
           "sujet" => "REF_sujet_helloworld",
           "numero" => 5,
           "nom" => "Ex 5",
           "valeur" => 4,
           'initial' => 20,
           "ref" => "REF_shello_5"
       )
   );

   public function doLoad(ObjectManager $manager) {
      foreach ($this->data as $e) {
         $exercice = new Exercice(
             $manager->merge($this->getReference($e["sujet"])), $e["numero"], $e["nom"], $e["valeur"],
             $e['initial']
         );
         $manager->persist($exercice);
         $this->addReference($e["ref"], $exercice);
      }
      $manager->flush();
   }

   public function getOrder() {
      return 5;
   }

   public function getEnvironments() {
      return array('test');
   }

}
