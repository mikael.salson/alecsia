<?php

namespace Alecsia\AnnotationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Alecsia\AnnotationBundle\Entity\Student;

/**
 * Générateur de contenu "d'exemple" pour l'application.
 * Références ajoutées :
 */
class LoadStudent extends AbstractDataFixture implements OrderedFixtureInterface {

   private $data = array(array('login' => 'student1',
           'firstName' => 'Stue',
           'lastName' => 'Dent',
           'email' => 'stu1@dent.fr',
           'ref' => 'REF_Student1'
       ),
       array('login' => 'student2',
           'firstName' => 'Stiou',
           'lastName' => 'Dent',
           'email' => 'stu2@dent.fr',
           'ref' => 'REF_Student2'
       ),
       array('login' => 'student3',
           'firstName' => 'Jane',
           'lastName' => 'Doe',
           'email' => 'stu3@dent.fr',
           'ref' => 'REF_Student3'
       ),
       array('login' => 'etudiant4',
           'firstName' => 'Alecsia',
           'lastName' => 'Symfony',
           'email' => '',
           'ref' => 'REF_Student4'
       ),
       array('login' => 'anonymous',
           'firstName' => '',
           'lastName' => '',
           'email' => '',
           'ref' => 'REF_Student5'
       ),
   );

   public function doLoad(ObjectManager $manager) {
      foreach ($this->data as $s) {
         $student = Student::studentFromMap($s);

         $manager->persist($student);
         $this->addReference($s["ref"], $student);
      }
      $manager->flush();
   }

   public function getOrder() {
      return 1;
   }

   public function getEnvironments() {
      return array('test');
   }

}
