<?php

namespace Alecsia\AnnotationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Alecsia\AnnotationBundle\Entity\Sujet;
use Alecsia\AnnotationBundle\Entity\ListeModeles;

/**
 * Générateur de contenu "d'exemple" pour l'application.
 * Références ajoutées :
 */
class LoadSujets extends AbstractDataFixture implements OrderedFixtureInterface {

   private $data = array(
       array(
           "nom" => "TP1 : Premiers pas avec le langage C",
           "status" => Sujet::STATUS_WAIT,
           "modeles" => "REF_lm1",
           "ref" => "REF_sujet_helloworld",
           "ue" => "REF_UE_PDC"
       ),
       array(
           "nom" => "TP2 : Blabla",
           "ref" => "REF_sujet_blabla",
           "modeles" => "REF_lm2",
           "ue" => "REF_UE_PDC"
        ),
        array(
            "nom" => "TP3 : Useless",
            "ref" => "REF_sujet_useless",
            "modeles" => "REF_lm3",
            "ue" => "REF_UE_PDC"
       )
   );

   public function doLoad(ObjectManager $manager) {
      foreach ($this->data as $s) {
         $sujet = new Sujet(
                 $s["nom"]
         );

         // Récup de la liste, ou création, en fonction
         if (!($this->hasReference($s["modeles"]))) {
            $liste = new ListeModeles();
            $manager->persist($liste);
            $this->addReference($s["modeles"], $liste);
         } else {
            $liste = $manager->merge($this->getReference($s["modeles"]));
         }

         if (!is_null($s["modeles"])) {
            $sujet->setListeModeles($liste);
         }

         $ue = $manager->merge($this->getReference($s["ue"]));
         $sujet->setUE($ue);

         $manager->persist($sujet);
         $this->addReference($s["ref"], $sujet);
      }
      $manager->flush();
   }

   public function getOrder() {
      return 4;
   }

   public function getEnvironments() {
      return array('test');
   }

}
