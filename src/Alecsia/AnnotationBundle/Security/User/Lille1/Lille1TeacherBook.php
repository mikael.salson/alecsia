<?php

/**
 * Author: Ludovic Loridan
 * Date: 27/01/13
 * Time: 14:11
 *
 * Call Lille1 Teacher Book's webservice to retrieve infos
 * of teachers.
 */

namespace Alecsia\AnnotationBundle\Security\User\Lille1;

use Buzz\Browser;
use Symfony\Component\Config\Definition\Exception\Exception;
use Alecsia\AnnotationBundle\Entity\Teacher;

class Lille1TeacherBook {

   const SERVICE_URL = "http://teacherbook.local";
   const TEACHER_NOT_FOUND = 404;
   const TEACHER_FOUND = 200;

   private $url;

   /* ============================== */
   /* Initialization                 */
   /* ============================== */

   function __construct(Browser $buzz, $url = '') {
      $this->buzz = $buzz;
      if (!empty($url)) {
         $this->url = $url;
      } else {
         $this->url = self::SERVICE_URL;
      }
   }

   /* ============================================== */
   /* Service                                        */
   /* ============================================== */

   public function getTeacher($login) {
      $teacherResponse = $this->requestTeacherBookServerFor($login);
      $status = $teacherResponse->getStatusCode();

      switch ($status) {
         case self::TEACHER_FOUND:
            $teacherJSON = json_decode($teacherResponse->getContent(), true);

            return Teacher::teacherFromMap($teacherJSON);
         case self::TEACHER_NOT_FOUND:
            return null;
         default:
            throw new Exception("Unable to connect to Lille 1's database");
      }
   }

   public function teacherExists($login) {
      return !is_null($this->getTeacher($login));
   }

   /* ============================== */
   /* Request                        */
   /* ============================== */

   private function requestTeacherBookServerFor($login) {
      $teacherURL = $this->getTeacherResourceURL($login);
      return $this->getBuzz()->get($teacherURL);
   }

   private function getTeacherResourceURL($login) {
      return sprintf("%s/teacher/%s", $this->url, $login);
   }

   /* ============================== */
   /* Services                       */
   /* ============================== */

   private $buzz;

   /** @return Browser */
   private function getBuzz() {
      return $this->buzz;
   }

}
