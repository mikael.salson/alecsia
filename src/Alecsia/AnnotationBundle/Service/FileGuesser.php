<?php

namespace Alecsia\AnnotationBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\DependencyInjection\ContainerAware;
use Doctrine\ORM\Query\ResultSetMapping;
use Alecsia\AnnotationBundle\Entity\Langage;
use Alecsia\AnnotationBundle\Entity\AlecsiaUser;

/**
 * Un service dédié à la détection du langage d'un fichier, ou de son exclusion.
 */
class FileGuesser {

   protected $doctrine;

   public function __construct(Registry $doctrine) {
      $this->doctrine = $doctrine;
   }

   /* Renvoie la note d'un rendu ou d'un exercice, en fonction. */

   public function guessLanguage($dossier, $fichier, AlecsiaUser $user) {

      // Mapping
      $rsm = new ResultSetMapping();
      $rsm->addScalarResult("langage_id", "langage_id");

      // Requête
      $em = $this->doctrine->getManager();

      $request = "SELECT langage_id
         FROM RegleTypeFichier
         WHERE :nom_fichier REGEXP regex = 1
         AND user_id = :id
         ORDER BY priorite desc
         LIMIT 1";

      $sqlQuery = $em->createNativeQuery($request, $rsm);
      $sqlQuery->setParameter("nom_fichier", $fichier);
      $sqlQuery->setParameter("id", $user->getId());

      try {
         $result = $sqlQuery->getSingleScalarResult();
      } catch (\Doctrine\ORM\NoResultException $e) {
         $result = null;
      }

      // Si Null renvoi de simple texte
      if ($result == null) {
         $simple_text = $em->getRepository('AnnotationBundle:Langage')->findOneBy(array('id_geshi' => 'text'));

         if (!$simple_text) {
            $simple_text = new Langage('text', "Simple texte");
            $em->persist($simple_text);
            $em->flush();
         }

         $result = $simple_text;
      }

      // Sinon renvoi du langage
      else {
         $result = $em->getRepository('AnnotationBundle:Langage')->findOneById($result);
      }

      return $result;
   }

   /* Renvoie vrai si le fichier doit être exclus. */

   public function guessExclus($dossier, $fichier, Langage $language, AlecsiaUser $user) {

      // Mapping
      $rsm = new ResultSetMapping();
      $rsm->addScalarResult("nbmatches", "nbmatches");

      // Requête fichier
      $em = $this->doctrine->getManager();

      $request = 'SELECT count(id) as nbmatches
         FROM RegleExclusionFichier
         WHERE :nom_fichier REGEXP regex = 1 AND user_id = :id';

      $sqlQuery = $em->createNativeQuery($request, $rsm);
      $sqlQuery->setParameter("nom_fichier", $fichier);
      $sqlQuery->setParameter("id", $user->getId());

      $result_text = !(AlecsiaViewerCapacity::isViewable($dossier . '/' . $fichier, $language->getIdGeshi()));

      $result_f = $sqlQuery->getSingleScalarResult();

      // Requête dossier
      $request = 'SELECT count(id) as nbmatches
         FROM RegleExclusionDossier
         WHERE :nom_dossier REGEXP regex = 1 AND user_id = :id';

      $sqlQuery = $em->createNativeQuery($request, $rsm);
      $sqlQuery->setParameter("nom_dossier", $dossier);
      $sqlQuery->setParameter("id", $user->getId());

      $result_d = $sqlQuery->getSingleScalarResult();


      return ((bool) $result_f || (bool) $result_d || (bool) $result_text);
   }

}
