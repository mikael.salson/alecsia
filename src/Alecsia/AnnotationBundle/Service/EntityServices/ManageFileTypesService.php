<?php

namespace Alecsia\AnnotationBundle\Service\EntityServices;

use Alecsia\AnnotationBundle\Entity\RegleTypeFichier;
use Alecsia\AnnotationBundle\Entity\AlecsiaUser;
use Doctrine\ORM\EntityManager;

class ManageFileTypesService extends EntityService {

   const managedEntity = 'Alecsia\AnnotationBundle\Entity\RegleTypeFichier';

   private $default_languages = array(
       array(
           "regex" => "^.*\.(c|C|h|H|i|I|a|A)$",
           "language" => "c",
       ),
       array(
           "regex" => "^.*\.(adb|ads)$",
           "language" => "ada",
       ),
       array(
           "regex" => "^.*\.(cpp|CPP|c++)$",
           "language" => "cpp",
       ),
       array(
           "regex" => "^.*\.(css|CSS)$",
           "language" => "css",
       ),
       array(
           "regex" => "^.*\.(html|xhtml)$",
           "language" => "html5",
       ),
       array(
           "regex" => "^.*\.(java)$",
           "language" => "java",
       ),
       array(
           "regex" => "^.*\.(js)$",
           "language" => "javascript",
       ),
       array(
           "regex" => "^.*\.(css|CSS)$",
           "language" => "css",
       ),
       array(
           "regex" => "^.*\.(php)$",
           "language" => "php",
       ),
       array(
           "regex" => "^.*\.(py)$",
           "language" => "python",
       ),
       array(
           "regex" => "^.*\.(rb|rbw)$",
           "language" => "ruby",
       ),
       array(
           "regex" => "^.*\.(scm|ss)$",
           "language" => "scheme",
       ),
       array(
           "regex" => "^.*\.(sql)$",
           "language" => "sql",
       ),
       array(
           "regex" => "^.*\.(xml)$",
           "language" => "xml",
       ),
       array(
           "regex" => "^.*\.(s|S)$",
           "language" => "asm",
       ),
       array(
           "regex" => "^.*\.(ml|Ml|ML)$",
           "language" => "ocaml",
       ),
       array(
           "regex" => "^.*\.(scala)$",
           "language" => "scala",
       ),
       array(
           "regex" => "^(m|M)akefile$",
           "language" => "make",
       ),
       array(
           "regex" => ".(jpg|jpeg|png|bmp|gif|JPG|JPEG|PNG|BMP|GIF)",
           "language" => "img"
       )
   );

   function __construct($doctrine, $validator) {
      parent::__construct($doctrine, $validator);
   }

   public function createDefaultRules(AlecsiaUser $user) {
      $repo_lang = $this->doctrine->getRepository('AnnotationBundle:Langage');

      foreach ($this->default_languages as $d) {
         $lang = $repo_lang->findOneBy(array('id_geshi' => $d['language']));
         $this->add(new RegleTypeFichier($d['regex'], $lang, 0, $user));
      }
   }

}
