<?php

namespace Alecsia\AnnotationBundle\Service\EntityServices;

use Alecsia\AnnotationBundle\Entity\RegleExclusionFichier;
use Alecsia\AnnotationBundle\Entity\AlecsiaUser;
use Doctrine\ORM\EntityManager;

class ManageExcludedFilesService extends EntityService {

   const managedEntity = 'Alecsia\AnnotationBundle\Entity\RegleExclusionFichier';

   private $default_excluded = array(
       array(
           "regex" => "\.(exe|Exe|EXE)$",
       ),
       array(
           "regex" => "\.(o|O)$",
       ),
       array(
           "regex" => "^\..*$",
       ),
       array(
           "regex" => "\.(i|s)$",
       ),
       array(
           "regex" => "\.pdf$",
       ),
       array(
           "regex" => "\.cm[io]$",
       ),
       array(
           "regex" => "\.(bmp|gif|jpg|jpeg|png)$",
       ),
       array(
           "regex" => ".*~$",
       )
   );

   function __construct($doctrine, $validator) {
      parent::__construct($doctrine, $validator);
   }

   public function createDefaultRules(AlecsiaUser $user) {
      foreach ($this->default_excluded as $d) {
         $this->add(new RegleExclusionFichier($d['regex'], $user));
      }
   }

}
