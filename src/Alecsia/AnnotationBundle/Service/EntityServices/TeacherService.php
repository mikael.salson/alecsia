<?php

/**
 * Author: Ludovic Loridan
 * Date: 27/01/13
 * Time: 14:10
 *
 * TeacherService.php
 *
 */

namespace Alecsia\AnnotationBundle\Service\EntityServices;

use Alecsia\AnnotationBundle\Entity\Teacher;

class TeacherService extends EntityService {

   const managedEntity = 'Alecsia\AnnotationBundle\Entity\Teacher';

   /* ============================== */
   /* Initialization                 */
   /* ============================== */

   function __construct($doctrine, $validator) {
      parent::__construct($doctrine, $validator);
   }

   /* ============================== */
   /* Accessors                      */
   /* ============================== */

   public function getTeacherByLogin($login) {
      return $this->getRepository()->findOneByLogin($login);
   }

   public function teacherExists($login) {
      return !(is_null($this->getTeacherByLogin($login)));
   }

   /* ============================== */
   /* Actions                        */
   /* ============================== */

   public function getTeacherOrAddIt(Teacher $teacherToTest) {
      $login = $teacherToTest->getLogin();
      if ($this->teacherExists($login)) {
         return $this->getTeacherByLogin($login);
      } else {
         return $this->add($teacherToTest);
      }
   }

   /* ===================================== */
   /* Example teacher                       */
   /* ===================================== */

   public function getExampleTeacher() {
      $teacher = Teacher::teacherFromMap(array(
                  "login" => "ron.swanson",
                  "firstName" => "Ron",
                  "lastName" => "Swanson",
                  "email" => "ron.swanson@pawnee.org"
      ));
      return $this->getTeacherOrAddIt($teacher);
   }

}
