<?php

namespace Alecsia\AnnotationBundle\Service\EntityServices;

/**
 * ModeleService.php
 *
 */
use Alecsia\AnnotationBundle\Entity\ModeleRepository;
use Alecsia\AnnotationBundle\Entity\Modele;
use Alecsia\AnnotationBundle\Entity\Sujet;
use Alecsia\AnnotationBundle\Entity\UE;
use Alecsia\AnnotationBundle\Service\NumberEncrypter;
use Doctrine\ORM\EntityManager;

class ModeleService extends EntityService {

   const managedEntity = 'Alecsia\AnnotationBundle\Entity\Modele';
   const MODEL_LOCAL = 0;
   const MODEL_UE = 1;
   const MODEL_GLOBAL = 2;

   protected $exerciceService;

   /* ============================== */
   /* Initialization                 */
   /* ============================== */

   function __construct($doctrine, $validator, $exerciceService) {
      parent::__construct($doctrine, $validator);
      $this->exerciceService = $exerciceService;
   }

   function getAbstractJson($view, $category, $value, $exercise, $modelId, $focus, $isDefault, $disabled, $makeModel = "-1", $name = "", $formattedValue = "", $comment = "") {
      return array(
          "category" => $category,
          "label" => $view,
          "value" => $value,
          "fma_nom" => $name,
          "fma_valeur" => $formattedValue,
          "fma_commentaire" => $comment,
          "fma_exercice" => $exercise,
          "fma_modele_id" => $modelId,
          "fma_makemodelein" => $makeModel,
          "focus" => $focus,
          "isDefault" => $isDefault,
          "disable_fma_exercice" => $disabled
      );
   }

   /**
    * @return All the models related to a bonus/malus for the Sujet $sujet.
    */
   function getBonusMalusModels(Sujet $sujet) {
      return $this->getRepository()
                      ->findBy(array("liste" => $sujet->getListeModeles()->getId(),
                          "exercice" => null)
      );
   }

   /**
    * @return the category ie, depending on the model, the name of the exercise,
    *         bonus/malus (tp), bonus/malus (ue), bonus/malus (global)
    */
   function getCategory(Modele $model) {
      $type = $this->getModelType($model);
      switch ($type) {
         case self::MODEL_LOCAL:
            return $this->exerciceService->getExName($model->getExercice());
         case self::MODEL_UE:
            $ue = $this->getEntityRepository('UE')->findOneByModel($model);
            return ExerciceService::BONUS_MALUS . ' (' . $ue->getNomCourt() . ')';
         default:
            return ExerciceService::BONUS_MALUS . ' (global)';
      }
   }

   /**
    * @return the models from the current exercise
    * @param $sujet: the Sujet
    * @param $current_exercise: the Id of the current exercise (must be a real ID)
    */
   function getCurrentExerciseModels(Sujet $sujet, $current_exercise) {
      $models = $this->getRepository()
              ->findBy(array("liste" => $sujet->getListeModeles()->getId(),
          "exercice" => $current_exercise)
      );

      return $models;
   }

   /**
    * @return an array of Modele containing all the models.
    * First appear the models from the current exercise (if it exists),
    * Then the models specific to this subject, from the other exercises.
    * Then the models specific to the UE.
    * And finally the global models.
    * @param $sujet: the Sujet we want the models for.
    * @param $current_exercise: the ID of the current exercise (-1 otherwise)
    */
   function getInterestingModels(Sujet $sujet, $current_exercise) {
      $models = array();
      if ($current_exercise != -1) {
         $models = $this->getCurrentExerciseModels($sujet, $current_exercise);
      }

      $models = array_merge($models, $this->getOtherExerciseModels($sujet, $current_exercise));
      /* Bonus/Malus from the Sujet */
      $models = array_merge($models, $this->getBonusMalusModels($sujet));
      /* Models from UE */
      $models = array_merge($models, $this->getUEModels($sujet->getUE()));

      return $models;
   }

   /**
    * @return the exercises that are in the Sujet $sujet but are not in the exercise
    *         $current_exercise
    */
   function getOtherExerciseModels(Sujet $sujet, $current_exercise) {
      return $this->getRepository()
                      ->getByListeExoExclus($sujet->getListeModeles(), $current_exercise);
   }

   /**
    * @return MODEL_LOCAL if the model is local to this work
    *         MODEL_UE if the model is global to the UE
    *         MODEL_GLOBAL if the model is global to the application
    */
   function getModelType(Modele $model) {
      if ($this->getEntityRepository('UE')->findOneByModel($model) != null)
         return self::MODEL_UE;
      else if ($this->getEntityRepository('Sujet')->findOneByModel($model) != null)
         return self::MODEL_LOCAL;
      else
         return self::MODEL_GLOBAL;
   }

   /**
    * @param $modele: A Modele entity
    * @param $view: the view of the specified model
    * @param $category: The category, the model should appear in
    */
   function getJson(Modele $modele, $view, $category) {
      return $this->getAbstractJson($view, $category, $modele->getNom(), $this->exerciceService->getExId($modele->getExercice()), $modele->getId(), "fma_submit", false, false, "-1", $modele->getNom(), $modele->getValeurFormatee(), $modele->getCommentaire());
   }

   /**
    * Get the json for every model stored in $models.
    * The view is rendered using the $controller with the URL $url and
    * the data $data, if specified (or it defaults to an array giving access
    * to the model).
    */
   function getJsons($models, $controller, $url, $data = null) {
      $result = array();
      foreach ($models as $model) {
         if ($data == null)
            $renderData = array('m' => $model);
         else
            $renderData = $data;
         $result[] = $this->getJson($model, $controller->renderView($url, $renderData), $this->getCategory($model));
      }
      return $result;
   }

   /**
    * @return All the models that are global to that UE
    */
   function getUEModels(UE $ue) {
      return $ue->getModelList()->getModeles()->toArray();
   }

}
