<?php

/**
 * Author: Ludovic Loridan
 * Date: 27/01/13
 * Time: 14:10
 *
 * StudentService.php
 *
 */

namespace Alecsia\AnnotationBundle\Service\EntityServices;

use Alecsia\AnnotationBundle\Entity\Student;
use Alecsia\AnnotationBundle\Entity\Rendu;
use Alecsia\AnnotationBundle\Entity\UE;
use Alecsia\AnnotationBundle\Service\NumberEncrypter;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class StudentService extends EntityService {

   const managedEntity = 'Alecsia\AnnotationBundle\Entity\Student';
   const notRegisteredLoginPrefix = 'xxx@';

   /* ============================== */
   /* Initialization                 */
   /* ============================== */

   function __construct($doctrine, $validator) {
      parent::__construct($doctrine, $validator);
   }

   /* ============================== */
   /* Accessors                      */
   /* ============================== */

   /** Search students whose name starts as the one given in parameter */
   public function getAllStudentsByName($name) {
      return $this->getRepository()->findByStudentNameOrLogin($name);
   }

   public function getStudentByLogin($login) {
      return $this->getRepository()->findOneByLogin($login);
   }

   public function studentExists($login) {
      return !(is_null($this->getStudentByLogin($login)));
   }

   public function getExistingStudent(Student $student) {
     $login = $student->getLogin();
     if ($this->studentExists($login)) {
       return $this->getStudentByLogin($login);
     }
     throw new UsernameNotFoundException();
   }

   /* ============================== */
   /* Actions                        */
   /* ============================== */

   public function getStudentOrAddIt(Student $studentToTest) {
     try {
       $this->getExistingStudent($studentToTest);
     } catch (UsernameNotFoundException $notFound) {
       return $this->add($studentToTest);
     }
   }

   public function addNotRegisteredStudent($name, $firstName, $email) {
      $encrypter = new NumberEncrypter();
      /* Create a temporary login for a not registered student */
      do {
         $login = static::notRegisteredLoginPrefix . $encrypter->encryptNumber(rand());
      } while ($this->studentExists($login));

      /* Add the student to the database */
      return $this->getStudentOrAddIt
                      (Student::studentFromMap(array("login" => $login,
                          "firstName" => $firstName,
                          "lastName" => $name,
                          "email" => $email,
                          "registered" => false)));
   }

   /**
    * Merge the accounts of $student1 and $student2.
    * All the stuff attributed to $student2 has been attributed to $student1.
    * At the end, the account of $student2 doesn't exist anymore.
    */
   public function mergeStudents(Student $student1, Student $student2, $user = null) {
      if ($student1->equals($student2)) {
         throw new InvalidArgumentException();
      }

      /* Get all works performed by the student and change the student */
      foreach ($student2->getWorks() as $work) {
         $work->removeStudent($student2);
         if (!$work->hasStudent($student1))
            $work->addStudent($student1);
      }

      $this->remove($student2, $user);
   }

   /* ===================================== */
   /* Example teacher                       */
   /* ===================================== */

   public function getExampleStudent() {
      $student = Student::studentFromMap(array(
                  "login" => "tom.haverford",
                  "firstName" => "Tom",
                  "lastName" => "Haverford",
                  "email" => "tom.haverford@pawnee.org"
      ));
      return $this->getStudentOrAddIt($student);
   }

}
