<?php

namespace Alecsia\AnnotationBundle\Service\EntityServices;

/**
 * GroupService.php
 *
 */
use Alecsia\AnnotationBundle\Entity\AlecsiaUser;
use Alecsia\AnnotationBundle\Entity\Student;
use Alecsia\AnnotationBundle\Entity\Teacher;
use Alecsia\AnnotationBundle\Entity\UE;
use Alecsia\AnnotationBundle\Entity\Group;
use Alecsia\AnnotationBundle\Service\NumberEncrypter;
use Doctrine\ORM\EntityManager;

class GroupService extends EntityService {

   const managedEntity = 'Alecsia\AnnotationBundle\Entity\Group';

   /* ============================== */
   /* Initialization                 */
   /* ============================== */

   function __construct($doctrine, $validator) {
      parent::__construct($doctrine, $validator);
   }

   public function getDefaultGroupFrom(UE $ue, Teacher $user) {
      return $this->getRepository()->getDefaultGroupFrom($ue, $user);
   }

   public function getFromShareCode($shareCode, AlecsiaUser $user) {
      $decrypter = new NumberEncrypter();
      $id = $decrypter->decryptString($shareCode);
      if ($id !== FALSE) {
         return $this->get($id, $user);
      } else {
         return null;
      }
   }

   /* ============================== */
   /* Students                       */
   /* ============================== */

   public function addStudentInGroup(Group $group, Student $studentToAdd, AlecsiaUser $user) {
      if (!$group->hasStudent($studentToAdd)) {
         $group->addStudent($studentToAdd);
         return $this->update($group, $user);
      }
      return $group;
   }

   public function removeStudentInGroup(Group $group, Student $studentToRemove, AlecsiaUser $user) {
      if ($group->hasStudent($studentToRemove)) {
         $group->removeStudent($studentToRemove);
         return $this->update($group, $user);
      }
      return $group;
   }

   /* ============================== */
   /* List Accessors                 */
   /* ============================== */

   /**
    * @return An ArrayCollection of Group whose owner is $user, and whose UE is $ue
    */
   public function listOfActiveGroupsInUE(UE $ue, Teacher $user) {
      return $this->getRepository()->getListOfActiveGroupsInUE($ue, $user);
   }

   /**
    * @return the active groups of a given teacher.
    * if onePerUE is true, only one group per UE is returned
    */
   public function getListOfActiveGroups(Teacher $user, $onePerUE = true) {
      return $this->getRepository()->getListOfActiveGroups($user, $onePerUE);
   }

   /**
    * @return the archived groups of a given teacher.
    * if onePerUE is true, only one group per UE is returned
    */
   public function getListOfArchivedGroups(Teacher $user, $onePerUE = true) {
      return $this->getRepository()->getListOfArchivedGroups($user, $onePerUE);
   }

}
