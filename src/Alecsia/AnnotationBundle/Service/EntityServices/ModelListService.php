<?php

namespace Alecsia\AnnotationBundle\Service\EntityServices;

use Alecsia\AnnotationBundle\Entity\AlecsiaUser;
use Alecsia\AnnotationBundle\Entity\ListeModeles;
use Alecsia\AnnotationBundle\Entity\Preference;
use Doctrine\ORM\EntityManager;

class ModelListService extends EntityService {

   const managedEntity = 'Alecsia\AnnotationBundle\Entity\ListeModeles';

   public function createGlobalList(AlecsiaUser $user) {
      $pref = $this->doctrine->getRepository("AnnotationBundle:Preference")
              ->findOneByUser($user);

      // Création de la liste et enregistrement
      $ml = new ListeModeles();
      $this->add($ml);

      if ($pref == null) {
         $pref = new Preference('liste_globale', $ml->getId(), $user);
      } else {
         $pref->setValeur($ml->getId());
      }
      $this->getManager()->persist($pref);
      $this->getManager()->flush();

      return $ml;
   }

   /**
    * get a global list for an user and create it if needed
    */
   public function getGlobalList(AlecsiaUser $user) {
      $pref = $this->doctrine->getRepository("AnnotationBundle:Preference")
              ->findOneBy(array('cle' => "liste_globale",
          'user' => $user->getId()));

      if (is_null($pref)) {
         return $this->createGlobalList($user);
      }

      $list_id = $pref->getValeur();

      // Id trouvé : Retour de la liste globale
      $list = $this->getRepository()->findOneById($list_id);

      if (is_null($list)) {
         return $this->createGlobalList($user);
      } else {
         return $list;
      }
   }

}
