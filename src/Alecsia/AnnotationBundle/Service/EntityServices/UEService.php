<?php

namespace Alecsia\AnnotationBundle\Service\EntityServices;

/**
 * Author: Ludovic Loridan
 *
 * UEService.php
 *
 */
use Alecsia\AnnotationBundle\Entity\AlecsiaUser;
use Alecsia\AnnotationBundle\Entity\UERepository;
use Alecsia\AnnotationBundle\Entity\UE;
use Alecsia\AnnotationBundle\Entity\Teacher;
use Alecsia\AnnotationBundle\Entity\Student;
use Alecsia\AnnotationBundle\Service\NumberEncrypter;
use Doctrine\ORM\EntityManager;

class UEService extends EntityService {

   const managedEntity = 'Alecsia\AnnotationBundle\Entity\UE';

   /* ============================== */
   /* Initialization                 */
   /* ============================== */

   function __construct($doctrine, $validator) {
      parent::__construct($doctrine, $validator);
   }

   /* ============================== */
   /* List Accessors                 */
   /* ============================== */

   public function getListOfActiveUEs(Teacher $user) {
      return $this->getRepository()->getListOfActiveUEs($user);
   }

   public function getListOfArchivedUEs(Teacher $user) {
      return $this->getRepository()->getListOfArchivedUEs($user);
   }

}
