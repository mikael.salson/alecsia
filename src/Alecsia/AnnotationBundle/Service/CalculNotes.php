<?php

namespace Alecsia\AnnotationBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\DependencyInjection\ContainerAware;
use Alecsia\AnnotationBundle\Entity\Rendu;
use Alecsia\AnnotationBundle\Entity\Modele;
use Alecsia\AnnotationBundle\Entity\Exercice;
use Alecsia\AnnotationBundle\Entity\Sujet;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Un service dédié au calcul de notes dans Symfony.
 */
class CalculNotes {

   protected $doctrine;

   public function __construct(Registry $doctrine) {
      $this->doctrine = $doctrine;
   }

   /* Construit une requête DQL pour obtenir la valeur des annotations filtrées
     selon les arguments */

   private function getSQLQuery(Rendu $rendu, $exercice, $relatif) {

// MAPPING DES RESULTATS
      $rsm = new ResultSetMapping();
      $rsm->addScalarResult("result", "result");


// REQUÊTE DE BASE
      $baseQuery = "SELECT
                          SUM(CASE a.deleguer_valeur
                          WHEN 1 THEN m.valeur
                          ELSE a.valeur
                          END) as result
                        FROM   Annotation a
                        LEFT JOIN   Fichier f ON f.id = a.fichier_id
                        LEFT JOIN   Modele m  ON m.id = a.modele_id
                        WHERE  a.rendu_id = :rendu";

      $sqlQuery = $this->doctrine->getManager()->createNativeQuery($baseQuery, $rsm);
      $sqlQuery->setParameter("rendu", $rendu->getId());


// CONTRAINTES
// 1 - Appartenance à l'exercice
      $getExercice = "(CASE a.deleguer_exercice
         WHEN 1 THEN m.exercice_id
         ELSE a.exercice_id
         END)";
      if ($exercice == null) {
         $contrainteExo = "AND $getExercice IS NULL";
      } else {
         $contrainteExo = "AND $getExercice = :exercice";
         $sqlQuery->setParameter("exercice", $exercice->getId());
      }

// 2 - Relatif / Absolu
      $getRelatif = "(CASE a.deleguer_relatif
         WHEN 1 THEN m.relatif
         ELSE a.relatif
         END)";
      if ($relatif) {
         $contrainteRel = "AND $getRelatif = 1";
      } else {
         $contrainteRel = "AND $getRelatif = 0";
      }

// 3 - Annotations pertinentes uniquement
      $contraintePerti = "AND (a.fichier_id IS NULL OR f.exclus = 0)";


// REQUÊTE FINALE
      $stringQuery = $baseQuery . " " . $contrainteExo . " " . $contrainteRel . " " . $contraintePerti;
      $sqlQuery->setSQL($stringQuery);

      return $sqlQuery;
   }

   /* Renvoie l'influence "relative" sur la note d'un exercice
     Par ex, si l'étudiant a fait trois erreurs à "-10%" sur l'ex. donné, cette
     méthode renverra "-0.3".
     Si Exercice est fixé à null, calcule l'influence des annotations "ensemble du sujet".
    */

   private function getInfluenceRelative(Rendu $rendu, $exercice = null) {

      $sqlQuery = $this->getSQLQuery($rendu, $exercice, true);
      $sqlresult = $sqlQuery->getSingleScalarResult();

      return $sqlresult;
   }

   /* Renvoie l'influence "absolue" sur la note d'un exercice
     Par ex, si l'étudiant a fait trois erreurs à "-0,3" sur l'ex. donné, cette
     méthode renverra "-0.9"
    */

   private function getInfluenceAbsolue(Rendu $rendu, $exercice = null) {
      $sqlQuery = $this->getSQLQuery($rendu, $exercice, false);
      $sqlresult = $sqlQuery->getSingleScalarResult();

      return $sqlresult;
   }

   /* Renvoie la note qu'a reçue un étudiant à un exercice */

   private function getNoteExercice(Rendu $rendu, Exercice $exercice) {
      $valeur_ex = $exercice->getValeur();
//$pointsRel = $valeur_ex * $this->getInfluenceRelative($rendu, $exercice);
      $bonusMalus = $exercice->getBonusMalus();
      $initialMark = $exercice->getInitialMark();
      $pointsRel = $valeur_ex * $this->getInfluenceRelative($rendu, $exercice);
      $pointsAbs = $this->getInfluenceAbsolue($rendu, $exercice);

      $note = $initialMark + $bonusMalus + $pointsRel + $pointsAbs;

// Refus des notes négatives
      if ($note < 0) {
         $note = 0;
      } else {
         if ($note > $valeur_ex) {
            return $valeur_ex;
         } else {

            return $note;
         }
      }
   }

   /* Renvoie la note qu'a reçue un étudiant à un exercice */

   public function getBareme(Rendu $rendu, $lectureseule = false) {

      $sujet = $rendu->getSujet();
      $bareme = new Bareme($rendu);

      $note = 0;

// Recup des exercices associés au rendu
      $em = $this->doctrine->getManager();
      $repo = $em->getRepository("AnnotationBundle:Exercice");
      $exercices = $repo->findBySujet($sujet->getId());

// Notes des exercices
      foreach ($exercices as $exercice) {
         $valeur_ex = $this->getNoteExercice($rendu, $exercice);
         $note += $valeur_ex;
         $bareme->ajouterExercice($exercice, $valeur_ex);
      }

// + Influence des bonus/malus
      $valeur_sujet = $sujet->getNoteSur();
      $pointsRel = $valeur_sujet * $this->getInfluenceRelative($rendu);
      $pointsAbs = $this->getInfluenceAbsolue($rendu);

      $valeur_bonusmalus = $pointsRel + $pointsAbs;
      $note += $valeur_bonusmalus;

      $bareme->setBonusMalus($valeur_bonusmalus);

// Refus des notes négatives & sup. à valeur du sujet
      if ($note < 0) {
         $note = 0;
      } else if ($note > $valeur_sujet) {
         $note = $valeur_sujet;
      }

      $bareme->setNoteFinale($note);

// Sauvegarde de la note finale dans le rendu
      if (!$lectureseule) {
         if (empty($exercices))
            $rendu->setNote(Rendu::NO_MARK);
         else
            $rendu->setNote($note);

         $this->doctrine->getManager()->flush();
      }
      return $bareme;
   }

   /* Renvoie la note d'un rendu ou d'un exercice, en fonction. */

   public function getNote(Rendu $rendu, $exercice = null) {
      if (($exercice == null) ||  (!($exercice instanceof Exercice))) {
         return $this->getBareme($rendu)->getNoteFinale();
      } else {
         return $this->getNoteExercice($rendu, $exercice);
      }
   }

   /* Rafraichit la note d'un rendu */

   public function rafraichirNote(Rendu $rendu) {
      $this->getBareme($rendu);
   }

   public function rafraichirNotes(Sujet $sujet) {
      $rendus = $sujet->getRendus();
      foreach ($rendus as $rendu) {
         $this->rafraichirNote($rendu);
      }
   }

   /* Avertit le module qu'un modèle a été modifié */

   public function modeleModifie(Modele $modele) {
      $em = $this->doctrine->getManager();

      $rendus_to_refresh = $em->getRepository("AnnotationBundle:Rendu")
              ->findByModele($modele);
      foreach ($rendus_to_refresh as $rendu) {
         $this->rafraichirNote($rendu);
      }
   }

}
