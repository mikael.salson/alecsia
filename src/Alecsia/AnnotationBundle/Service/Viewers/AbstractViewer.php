<?php

namespace Alecsia\AnnotationBundle\Service\Viewers;

/**
 * isNicelyViewable($id, $path) ==> canView($id) but the reverse is not necessarily true
 */
abstract class AbstractViewer {

   /**
    * @return true iff the viewer is able to view the provided ID
    */
    public static function canView($id){}

   /**
    * @return true iff the viewer is able to display nicely the file with
    *         the provided ID at the given path.
    */
    public static function isNicelyViewable($id, $path){}

   /**
    * @return the view of the given file
    * @pre canView($id)
    */
    public static function view($content, $id, $filename, $kernel){}
}
