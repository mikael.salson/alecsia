<?php

namespace Alecsia\AnnotationBundle\Service\Viewers;

use Highlight\Bundle\Providers\Providers;
use Symfony\Component\Yaml\Yaml;

define('MAX_FILESIZE', '100000');

class GeshiViewer extends AbstractViewer {

   public static function isNicelyViewable($id, $path) {
      $finfo = finfo_open(FILEINFO_MIME);
      if (!$finfo || !is_readable($path) || filesize($path) > MAX_FILESIZE)
         return false;
      /* Thanks to http://stackoverflow.com/questions/632685/how-to-check-if-file-is-ascii-or-binary-in-php */
      try {
         if (substr(finfo_file($finfo, $path), 0, 4) != 'text') {
            /* In case finfo_open fails, we check with a chunk at the start if the
              file seems readable or not */
            $chunk_size = 512;
            $fh = fopen($path, "r");
            $blk = fread($fh, $chunk_size);
            $chunk_size = strlen($blk);
            fclose($fh);
            clearstatcache();
            /* Check number of lines or tabs */
            $nb_lines = preg_match_all("/[\r\n\t]/", $blk);
            /* Check number of printable characters */
            $nb_print = preg_match_all("/[ -~]/", $blk);
            /* Remove special chars for ctype_print */
            $blk = str_replace(array(chr(10), chr(13), chr(9)), '', $blk);

            return (($nb_lines + $nb_print) / ($chunk_size+1) > 0.7
                    /* Check number of non printable chars (right?) */ && substr_count($blk, "\x00") < 0.01) || ctype_print($blk);
         }
      } catch (\Exception $e) {
         return false;
      }
      return true;
   }

   public static function canView($id) {
      return true;
   }

   public static function view($content, $id, $filename, $kernel) {
      $content = str_replace("\t", "    ", $content);

      $geshiConfig = $kernel->locateResource('@HighlightBundle/Resources/config/config.yml');
      $config = Yaml::parse(file_get_contents($geshiConfig));
      $geshi = new Providers($config['highlight'], $kernel->getCacheDir());
      return $geshi->getHtml($content, $id);
   }

}
