<?php

namespace Alecsia\AnnotationBundle\Service\Viewers;

class ImageViewer extends AbstractViewer {

   public static function canView($id) {
      return substr($id, 0, 3) == 'img';
   }

   public static function isNicelyViewable($id, $path) {
      return self::canView($id);
   }

   public static function view($content, $id, $filename, $kernel) {
      $pos_ext = strrpos($filename, '.') + 1;
      $ext = substr($filename, $pos_ext);
      return '<img src="data:image/' . $ext . ';base64,' . str_replace('\n', '', base64_encode($content)) . '" />';
   }

}

?>