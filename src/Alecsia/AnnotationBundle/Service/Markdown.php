<?php

namespace Alecsia\AnnotationBundle\Service;

class Markdown
{
    private $parser;

    public function __construct()
    {
        $this->parser = new \Parsedown();
    }

    public function toHtml($text)
    {
        $html = $this->parser->text($text);

        return $html;
    }

    public function toHtmlOneLine($text)
    {
        $html = $this->parser->line($text);

        return $html;
    }
}


?>