<?php

namespace Alecsia\AnnotationBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Alecsia\AnnotationBundle\Entity\Rendu;

/**
 * Un service dédié à la génération de chaîne aléatoire
 */
class CamouflageGenerator {

   protected $doctrine;

   const AVAILABLE_LETTERS = '()*,.0123456789:;<=>@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_abcdefghijklmnopqrstuvwxyz';

   public function __construct(Registry $doctrine) {
      $this->doctrine = $doctrine;
   }

   /**
    * Generate a random string of the given length.
    * There are about 1 billion 5-character strings.
    */
   public function generate($length) {
      $em = $this->doctrine->getManager();
      $possibilities = strlen(self::AVAILABLE_LETTERS);
      do {
         $camouflaging = '';
         for ($i = 0; $i < $length; $i++) {
            $camouflaging .= substr(self::AVAILABLE_LETTERS, rand(0, $possibilities - 1), 1);
         }
      } while ($em->getRepository("AnnotationBundle:Rendu")
              ->findOneByCamouflaging($camouflaging));
      return $camouflaging;
   }

}
