<?php

namespace Alecsia\AnnotationBundle\Service;


/**
 * A service of little util functions on strings and files
 */

class AlecsiaUtils {

    public static function remove_accents($str, $charset='utf-8')
    {
        $str = htmlentities($str, ENT_NOQUOTES, $charset);
        $str = preg_replace('#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
        
        return $str;
    }

    // Retire les caractères spéciaux du nom du fichier
    public static function cleanFileName ($old_filename) {
        $new_filename = AlecsiaUtils::remove_accents($old_filename);
        $new_filename = preg_replace('/[^a-zA-Z0-9\.\-~]/', '_', $old_filename);
        return $new_filename;
    }


    public static function cleanRename ($dossier, $old_filename, $base_folder) {
        $new_filename = AlecsiaUtils::cleanFileName($old_filename);
        if ($base_folder != "") {
            $base_folder = $base_folder . "/";
        }
        rename($base_folder.$dossier."/".$old_filename,  $base_folder.$dossier."/".$new_filename);
        return $new_filename;
    }
}