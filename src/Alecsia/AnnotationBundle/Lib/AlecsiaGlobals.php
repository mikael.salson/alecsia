<?php

namespace Alecsia\AnnotationBundle\Lib;

# Thanks to http://dev4theweb.blogspot.fr/2012/08/how-to-access-configuration-values.html

class AlecsiaGlobals {

   protected static $upload_dir;

    public static $maxModelPopular = 10;

   public static function setUploadDir($upload_dir) {
      self::$upload_dir = $upload_dir;
   }

   public static function getUploadDir() {
      return self::$upload_dir;
   }

}

?>