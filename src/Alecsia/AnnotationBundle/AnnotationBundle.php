<?php

namespace Alecsia\AnnotationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Alecsia\AnnotationBundle\Lib\AlecsiaGlobals;

class AnnotationBundle extends Bundle {

   public function boot() {
      AlecsiaGlobals::setUploadDir($this->container->getParameter('alecsia.upload_dir'));
   }

}
