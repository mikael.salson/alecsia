<?php

namespace Alecsia\AnnotationBundle\Tests\Service\EntityServices;

use Alecsia\AnnotationBundle\Tests\AlecsiaTestCase;
use Alecsia\AnnotationBundle\Service\EntityServices\ModeleService;
use Alecsia\AnnotationBundle\Service\EntityServices\ExerciceService;

class ModeleServiceTest extends AlecsiaTestCase {

   private function createModeleService() {
      return new ModeleService($this->doctrine, $this->validator, new ExerciceService($this->doctrine, $this->validator));
   }

   public function testBonusMalusModels1() {
      $ms = $this->createModeleService();

      $subject = $this->getRepository('Sujet')
              ->findOneByNom('TP1 : Premiers pas avec le langage C');

      $models = $ms->getBonusMalusModels($subject);

      $this->assertCount(2, $models);
      $this->assertThat("Plus de commentaires s'il te plait", $this->logicalOr($models[0]->getNom(), $models[1]->getNom()));
      $this->assertThat("Bonus/malus en PDC (TP1)", $this->logicalOr($models[0]->getNom(), $models[1]->getNom()));
   }

   public function testBonusMalusModelsEmpty() {
      $ms = $this->createModeleService();

      $subject = $this->getRepository('Sujet')
              ->findOneByNom('TP2 : Blabla');

      $models = $ms->getBonusMalusModels($subject);

      $this->assertCount(0, $models);
   }

   public function testCategoryLocal() {
      $ms = $this->createModeleService();

      $model = $this->getRepository('Modele')
              ->findOneByNom('Pas de printf ici');

      $category = $ms->getCategory($model);

      $this->assertEquals($model->getExercice()->__toString(), $category);
   }

   public function testCategoryLocalBonus() {
      $ms = $this->createModeleService();

      $model = $this->getRepository('Modele')
              ->findOneByNom("Plus de commentaires s'il te plait");

      $category = $ms->getCategory($model);

      $this->assertEquals(ExerciceService::BONUS_MALUS, $category);
   }

   public function testCategoryUE() {
      $ms = $this->createModeleService();

      $model = $this->getRepository('Modele')
              ->findOneByNom("Makefile manquant");

      $category = $ms->getCategory($model);

      $this->assertEquals(ExerciceService::BONUS_MALUS . ' (PDC)', $category);
   }

   public function testCategoryGlobal() {
      $ms = $this->createModeleService();

      $model = $this->getRepository('Modele')
              ->findOneByNom("Login dans le nom de l'archive");

      $category = $ms->getCategory($model);

      $this->assertEquals(ExerciceService::BONUS_MALUS . ' (global)', $category);
   }

   public function testCurrentExerciseModels() {
      $ms = $this->createModeleService();

      $exo = $this->getRepository('Exercice')
              ->findOneByNom('Affichez donc quelque chose en C');

      $models = $ms->getCurrentExerciseModels($exo->getSujet(), $exo->getId());

      foreach ($models as $model) {
         $this->assertEquals($model->getExercice(), $exo);
      }
   }

   public function testCurrentExerciseModelsNone() {
      $ms = $this->createModeleService();

      $exo = $this->getRepository('Exercice')
              ->findOneByNom('Affichez donc quelque chose en C');

      $models = $ms->getCurrentExerciseModels($exo->getSujet(), -1);
      $this->assertCount(0, $models);
   }

   private function checkInterestingModels($subjectName, $exName) {
      $ms = $this->createModeleService();

      $subject = $this->getRepository('Sujet')
              ->findOneByNom($subjectName);

      if ($exName) {
         $exo = $this->getRepository('Exercice')
                 ->findOneByNom($exName);
         $exo_id = $exo->getId();
      } else {
         $exo = null;
         $exo_id = -1;
      }

      $models = $ms->getInterestingModels($subject, $exo_id);

      $i = 0;

      $count = count($models);

      while ($i < $count && $models[$i]->getExercice() == $exo) {
         $i++;
      }
      $exo2 = $models[$i]->getExercice();

      $old_num = -1;
      while ($i < $count && $models[$i]->getExercice() != null) {
         $this->assertGreaterThanOrEqual($old_num, $models[$i]->getExercice()->getNumero());
         $this->assertNotEquals($exo_id, $models[$i]->getExercice()->getId());
         $old_num = $models[$i]->getExercice()->getNumero();
         $i++;
      }

      while ($i < $count) {
         $this->assertEquals(null, $models[$i]->getExercice());
         $i++;
      }
   }

   public function testInterestingModels() {
      $this->checkInterestingModels('TP1 : Premiers pas avec le langage C', 'Valeurs de retour');
   }

   public function testInterestingModelsNoExo() {
      $this->checkInterestingModels('TP1 : Premiers pas avec le langage C', false);
   }

   public function testOtherExerciseModels() {
      $ms = $this->createModeleService();

      $subject = $this->getRepository('Sujet')
              ->findOneByNom('TP1 : Premiers pas avec le langage C');

      $exo = $this->getRepository('Exercice')
              ->findOneByNom('Valeurs de retour');

      $models = $ms->getOtherExerciseModels($subject, $exo->getId());

      $count = count($models);
      for ($i = 0; $i < $count; $i++) {
         $this->assertNotEquals($exo->getId(), $models[$i]->getExercice()->getId());
      }
   }

   public function testModelType() {
      $ms = $this->createModeleService();
      $t = $ms->getModelType($this->getRepository('Modele')
                      ->findOneByNom('Makefile manquant'));

      $this->assertEquals(ModeleService::MODEL_UE, $t);

      $t = $ms->getModelType($this->getRepository('Modele')
                      ->findOneByNom("Login dans le nom de l'archive"));

      $this->assertEquals(ModeleService::MODEL_GLOBAL, $t);

      $t = $ms->getModelType($this->getRepository('Modele')
                      ->findOneByNom("Plus de commentaires s'il te plait"));

      $this->assertEquals(ModeleService::MODEL_LOCAL, $t);
   }

   public function testUEModels() {
      $ms = $this->createModeleService();
      $ue = $this->getRepository('UE')
              ->findOneByNom('Pratique du C');

      $models = $ms->getUEModels($ue);

      for ($i = 0; $i < count($models); $i++)
         $this->assertEquals($ue->getModelList()->getId(), $models[$i]->getListe()->getId());
   }

   public function testUEModelsEmpty() {
      $ms = $this->createModeleService();
      $ue = $this->getRepository('UE')
              ->findOneByNom('Compilation');

      $models = $ms->getUEModels($ue);

      $this->assertCount(0, $models);
   }

}
