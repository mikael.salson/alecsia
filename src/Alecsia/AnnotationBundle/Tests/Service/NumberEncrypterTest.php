<?php

// src/Acme/DemoBundle/Tests/Utility/CalculatorTest.php

namespace Alecsia\AnnotationBundle\Tests\Service;

use Alecsia\AnnotationBundle\Service\NumberEncrypter;

class NumberEncrypterTest extends \PHPUnit_Framework_TestCase {

   public function testEncryption() {
      $encrypter = new NumberEncrypter();
      $outputString = $encrypter->encryptNumber(2);
      $this->assertEquals($outputString, "scu7k3");
   }

   public function testDecryption() {
      $encrypter = new NumberEncrypter();

      $originalNumber = 901;
      $encryptedString = $encrypter->encryptNumber($originalNumber);
      $decryptedNumber = $encrypter->decryptString($encryptedString);

      echo $encryptedString;

      $this->assertEquals($originalNumber, $decryptedNumber);
   }

}
