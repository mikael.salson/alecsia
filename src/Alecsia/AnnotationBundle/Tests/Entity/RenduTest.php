<?php

namespace Alecsia\AnnotationBundle\Tests\Entity;

use Alecsia\AnnotationBundle\Tests\AlecsiaTestCase;
use Alecsia\AnnotationBundle\Entity\Exceptions\LectureSeuleException;
use Alecsia\AnnotationBundle\Entity\Sujet;

class RenduTest extends AlecsiaTestCase {

   public function testGetStudentSome() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp2_lennon_starr.tar.gz');

      $this->assertCount(1, $rendu);

      $student = $this->getRepository('Student')
              ->findByStudentNameOrLogin('Doe');

      $this->assertCount(1, $student);

      $this->assertEquals($rendu[0]->getStudents()[0], $student[0]);
   }

   public function testGetStudentNone() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp1_butler_chassagne.tar.gz');

      $student = $rendu[0]->getStudents();

      $this->assertCount(0, $student);
   }

   public function testAddOneStudent() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp1_butler_chassagne.tar.gz');

      $this->assertCount(1, $rendu);

      $student = $this->getRepository('Student')
              ->findByStudentNameOrLogin('Symfony');

      $this->assertCount(1, $student);

      $rendu[0]->addStudent($student[0]);

      $this->assertEquals($rendu[0]->getStudents()[0], $student[0]);
   }

   public function testAddStudentAlreadyExisting() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp2_lennon_starr.tar.gz');

      $this->assertCount(1, $rendu);

      $student = $this->getRepository('Student')
              ->findByStudentNameOrLogin('Doe');

      $this->assertCount(1, $student);

      $rendu[0]->addStudent($student[0]);

      $this->em->persist($rendu[0]);
      $this->em->flush();

      $this->assertCount(1, $rendu[0]->getStudents());
   }

   public function testRemoveStudentExisting() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp2_lennon_starr.tar.gz');

      $this->assertCount(1, $rendu);

      $student = $this->getRepository('Student')
              ->findByStudentNameOrLogin('Doe');

      $this->assertCount(1, $student);

      $rendu[0]->removeStudent($student[0]);

      $this->assertCount(0, $rendu[0]->getStudents());
   }

   public function testRemoveStudentNonexistant() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp2_lennon_starr.tar.gz');

      $this->assertCount(1, $rendu);

      $student = $this->getRepository('Student')
              ->findByStudentNameOrLogin('Symfony');

      $this->assertCount(1, $student);

      $this->assertCount(1, $rendu[0]->getStudents());

      $rendu[0]->removeStudent($student[0]);

      $this->assertCount(1, $rendu[0]->getStudents());
   }

   public function testSetNomAfterFreeze() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp1_mccartney_harrison.tar.gz');

      $this->assertCount(1, $rendu);

      $sujet = $this->getRepository('Sujet')
              ->findByNom('TP1 : Premiers pas avec le langage C');

      $this->assertCount(1, $sujet);

      $rendu[0]->getSujet()->setFrozen();

      try {
         $rendu[0]->setNom('tp1_mcCartnez_harriSon.tar.bz2');
      } catch (LectureSeuleException $e) {
         $except = true;
      }
      $this->assertTrue($except);

      $this->assertEquals($rendu[0]->getNom(), 'tp1_mccartney_harrison.tar.gz');
   }

   /* qd un sujet est frozen, on peut toujours ajuster les notes? */

   public function testAjusterSameNote() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp1_mccartney_harrison.tar.gz');

      $this->assertCount(1, $rendu);

      $this->assertEquals($rendu[0]->getNote(), 18);

      $rendu[0]->ajusterNote(18);

      $this->assertEquals($rendu[0]->getNote(), 18);
   }

   public function testAjusterDifferentNote() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp1_mccartney_harrison.tar.gz');

      $this->assertCount(1, $rendu);

      $this->assertEquals($rendu[0]->getNote(), 18);

      $rendu[0]->ajusterNote(16);

      $this->assertEquals($rendu[0]->getNote(), 16);
   }

   public function testHasStudent() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp1_mccartney_harrison.tar.gz');

      $this->assertCount(1, $rendu);

      $studentFalse = $this->getRepository('Student')
              ->findByStudentNameOrLogin('Doe');

      $this->assertCount(1, $studentFalse);

      $this->assertFalse($rendu[0]->hasStudent($studentFalse[0]));

      $studentTrue = $this->getRepository('Student')
              ->findByLogin('student1');

      $this->assertCount(1, $studentTrue);

      $this->assertTrue($rendu[0]->hasStudent($studentTrue[0]));
   }

   public function testGetNameWithoutExtension() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp1_mccartney_harrison.tar.gz');

      $this->assertCount(1, $rendu);

      $this->assertEquals($rendu[0]->getNameWithoutExtension(), 'tp1_mccartney_harrison');
   }

   public function testGetSujet() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp1_butler_chassagne.tar.gz');

      $this->assertCount(1, $rendu);

      $sujet = $this->getRepository('Sujet')
              ->findByNom('TP1 : Premiers pas avec le langage C');

      $this->assertCount(1, $sujet);

      $this->assertEquals($rendu[0]->getSujet(), $sujet[0]);
   }

   public function testSetSujet() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp1_butler_chassagne.tar.gz');

      $this->assertCount(1, $rendu);

      $sujet = $this->getRepository('Sujet')
              ->findByNom('TP2 : Blabla');

      $this->assertCount(1, $sujet);

      $rendu[0]->setSujet($sujet[0]);

      $this->assertEquals($rendu[0]->getSujet(), $sujet[0]);
   }

   public function testGetGroup() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp1_butler_chassagne.tar.gz');

      $this->assertCount(1, $rendu);

      $group = $this->getRepository('Group')
              ->findById(4);

      $this->assertCount(1, $group);

      $this->assertEquals($rendu[0]->getGroup(), $group[0]);
   }

   public function testSetGroup() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp1_butler_chassagne.tar.gz');

      $this->assertCount(1, $rendu);

      $group = $this->getRepository('Group')
              ->findById('3');

      $this->assertCount(1, $group);

      $rendu[0]->setGroup($group[0]);

      $this->assertEquals($rendu[0]->getGroup(), $group[0]);
   }

   public function testIsUserAllowedToGet() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp2_lennon_starr.tar.gz');

      $this->assertCount(1, $rendu);

      $teacher = $this->getRepository('Teacher')
              ->findByLogin('admin');

      $this->assertCount(1, $teacher);

      $student1 = $this->getRepository('Student')
              ->findByLogin('student3');

      $this->assertCount(1, $student1);

      $student2 = $this->getRepository('Student')
              ->findByLogin('student1');

      $this->assertCount(1, $student1);

      $this->assertCount(1, $student2);

      $unrelatedTeacher = $this->getRepository('Teacher')
              ->findOneByLogin('teacher');

      $this->assertTrue($rendu[0]->isUserAllowedToGet($teacher[0]));

      $this->assertFalse($rendu[0]->isUserAllowedToGet($student1[0]));
      $this->assertFalse($rendu[0]->isUserAllowedToGet($student2[0]));
      $this->assertFalse($rendu[0]->isUserAllowedToGet($unrelatedTeacher));

      $rendu[0]->getSujet()->setConsultable();
      $this->assertTrue($rendu[0]->isUserAllowedToGet($student1[0]));
      $this->assertFalse($rendu[0]->isUserAllowedToGet($student2[0]));
      $this->assertFalse($rendu[0]->isUserAllowedToGet($unrelatedTeacher));
   }

   public function testIsUserAllowedToAdd() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp1_mccartney_harrison.tar.gz');

      $this->assertCount(1, $rendu);

      $teacher = $this->getRepository('Teacher')
              ->findByLogin('admin');

      $this->assertCount(1, $teacher);

      $student = $this->getRepository('Student')
              ->findByLogin('student1');

      $this->assertCount(1, $student);

      $unrelatedTeacher = $this->getRepository('Teacher')
              ->findOneByLogin('teacher');

      $this->assertTrue($rendu[0]->isUserAllowedToAdd($teacher[0]));

      $this->assertFalse($rendu[0]->isUserAllowedToAdd($student[0]));
      $this->assertFalse($rendu[0]->isUserAllowedToAdd($unrelatedTeacher));
   }

   public function testIsUserAllowedToUpdate() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp2_lennon_starr.tar.gz');

      $this->assertCount(1, $rendu);

      $teacher = $this->getRepository('Teacher')
              ->findByLogin('admin');

      $this->assertCount(1, $teacher);

      $student = $this->getRepository('Student')
              ->findByLogin('student3');

      $this->assertCount(1, $student);

      $unrelatedTeacher = $this->getRepository('Teacher')
              ->findOneByLogin('teacher');

      $this->assertTrue($rendu[0]->isUserAllowedToUpdate($teacher[0]));

      $this->assertFalse($rendu[0]->isUserAllowedToUpdate($student[0]));
      $this->assertFalse($rendu[0]->isUserAllowedToUpdate($unrelatedTeacher));
   }

   public function testIsUserAllowedToDelete() {
      $rendu = $this->getRepository('Rendu')
              ->findByNom('tp2_lennon_starr.tar.gz');

      $this->assertCount(1, $rendu);

      $teacher = $this->getRepository('Teacher')
              ->findByLogin('admin');

      $this->assertCount(1, $teacher);

      $student = $this->getRepository('Student')
              ->findByLogin('student3');

      $this->assertCount(1, $student);

      $unrelatedTeacher = $this->getRepository('Teacher')
              ->findOneByLogin('teacher');

      $this->assertTrue($rendu[0]->isUserAllowedToDelete($teacher[0]));

      $this->assertFalse($rendu[0]->isUserAllowedToDelete($student[0]));
      $this->assertFalse($rendu[0]->isUserAllowedToDelete($unrelatedTeacher));
   }

   //changer la méthode en public si on veut faire le test
   /*
     public function testIsCorrectedByTeacher()
     {
     $rendu = $this->getRepository('Rendu')
     ->findByNom('tp1_butler_chassagne.tar.gz');

     $this->assertCount(1, $rendu);

     $teacher = $this->getRepository('Teacher')
     ->findByLogin('admin');

     $this->assertCount(1, $teacher);

     $this->assertTrue($rendu[0]->isCorrectedByTeacher($teacher[0]));
     } */
}
