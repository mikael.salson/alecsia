<?php

namespace Alecsia\AnnotationBundle\Tests\Entity;

use Alecsia\AnnotationBundle\Tests\AlecsiaTestCase;

class SujetRepositoryTest extends AlecsiaTestCase {

   public function testSearchByModel() {
      $sujet = $this->getRepository('Sujet')
              ->findOneByNom("TP1 : Premiers pas avec le langage C");

      $model = $this->getRepository('Modele')
              ->findOneByNom('Pas de scanf ici');

      $this->assertEquals($sujet, $this->getRepository('Sujet')
                      ->findOneByModel($model));

      /* UE-wide model */
      $model = $this->getRepository('Modele')
              ->findOneByNom("Makefile manquant");

      $this->assertEquals(null, $this->getRepository('Sujet')
                      ->findOneByModel($model));
   }

   public function testMoyenneOneGroup() {
      $sujet = $this->getRepository('Sujet')
              ->findOneByNom('TP1 : Premiers pas avec le langage C');

      $firstGroup = $sujet->getUE()->getGroups()->first();
      $secondGroup = $sujet->getUE()->getGroups()[1];

      $averageFirstGroup = $this->getRepository('Sujet')
              ->getMoyenne($sujet, $firstGroup);
      $averageSecondGroup = $this->getRepository('Sujet')
              ->getMoyenne($sujet, $secondGroup);

      $this->assertEquals(18, $averageFirstGroup);
      $this->assertEquals(6.5, $averageSecondGroup);
   }

   public function testMoyenneAllGroups() {
      $sujet = $this->getRepository('Sujet')
              ->findOneByNom('TP1 : Premiers pas avec le langage C');
      $average = $this->getRepository('Sujet')
              ->getMoyenne($sujet);

      $this->assertEquals((18 + 2 + 11) / 3., $average);
   }

   public function testMoyenneNoRendu() {
      $sujet = $this->getRepository('Sujet')
              ->findOneByNom('TP2 : Blabla');

      $average = $this->getRepository('Sujet')
              ->getMoyenne($sujet);

      $this->assertNull($average);
   }

   public function testNbWorksOneGroup() {
      $sujet = $this->getRepository('Sujet')
              ->findOneByNom('TP1 : Premiers pas avec le langage C');

      $firstGroup = $sujet->getUE()->getGroups()->first();
      $secondGroup = $sujet->getUE()->getGroups()[1];

      $nbFirstGroup = $this->getRepository('Sujet')
              ->getNbWorks($sujet, $firstGroup);
      $nbSecondGroup = $this->getRepository('Sujet')
              ->getNbWorks($sujet, $secondGroup);

      $this->assertEquals(1, $nbFirstGroup);
      $this->assertEquals(2, $nbSecondGroup);
   }

   public function testNbWorksNoRendu() {
      $sujet = $this->getRepository('Sujet')
              ->findOneByNom('TP2 : Blabla');

      $count = $this->getRepository('Sujet')
              ->getNbWorks($sujet, $sujet->getUE()->getGroups()->first());

      $this->assertEquals(0, $count);
   }

   public function testNbWorksNotCorrectedOneGroup() {
      $sujet = $this->getRepository('Sujet')
              ->findOneByNom('TP1 : Premiers pas avec le langage C');

      $firstGroup = $sujet->getUE()->getGroups()->first();
      $secondGroup = $sujet->getUE()->getGroups()[1];

      $nbFirstGroup = $this->getRepository('Sujet')
              ->getNbWorksNotCorrected($sujet, $firstGroup);
      $nbSecondGroup = $this->getRepository('Sujet')
              ->getNbWorksNotCorrected($sujet, $secondGroup);

      $this->assertEquals(0, $nbFirstGroup);
      $this->assertEquals(1, $nbSecondGroup);
   }

   public function testNbWorksNotCorrectedNoRendu() {
      $sujet = $this->getRepository('Sujet')
              ->findOneByNom('TP2 : Blabla');

      $count = $this->getRepository('Sujet')
              ->getNbWorksNotCorrected($sujet, $sujet->getUE()->getGroups()->first());

      $this->assertEquals(0, $count);
   }

}
