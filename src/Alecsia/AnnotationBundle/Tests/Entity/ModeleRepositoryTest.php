<?php

namespace Alecsia\AnnotationBundle\Tests\Entity;

use Alecsia\AnnotationBundle\Tests\AlecsiaTestCase;
use Symfony\Component\Security\Acl\Exception\Exception;

class ModeleRepositoryTest extends AlecsiaTestCase {

   public function testGetByListeExoExclus() {
      $excludedExo = $this->getRepository('Exercice')
              ->findOneByNom('Affichez donc quelque chose en C');
      $liste = $excludedExo->getSujet()->getListeModeles();

      $this->assertEquals($excludedExo->getNumero(), 1);
      $this->assertEquals($excludedExo->getValeur(), 4);

      $models = $this->getRepository('Modele')
              ->getByListeExoExclus($liste, $excludedExo->getId());

      $last_num = -1;
      foreach ($models as $model) {
         $this->assertTrue($model->getExercice()->getId() != $excludedExo->getId());

         $this->assertGreaterThanOrEqual($last_num, $model->getExercice()->getNumero());
         $last_num = $model->getExercice()->getNumero();
         $this->assertEquals($liste->getId(), $model->getListe()->getId());
      }
   }

  public function test_get_most_popular(){
    $max = 2;
    $sid = 1;
    $renduRep = $this->getRepository("Rendu");

    //Cette variable contient un tableau indexé par des entiers de 0 à $max-1.
    //Chaque case de ce tableau contient un tableau contenant 2 valeurs:
    //- un objet Modèle indexé par l'indice 0
    //- un entier correspondant au nombre d'occurrences du modèle indexé par la chaîne "occ" (voir requête dans la fonction getMostPopular)
    $modeles = $this->getRepository("Modele")->getMostPopular($sid, $max);

    $try = true;
//    $cpt = 1;
    $prec_occ = -1;
    foreach($modeles as $key => $val){
      //La première valeur ne peut être comparée
      if($prec_occ != -1) {
        //si l'occurrence précédente $prec_occ est inférieure à l'occurrence courante $val['occ'] alors on passe $try à false
        if ($val["occ"] > $prec_occ)
          $try = false;
      }
      $prec_occ = $val["occ"];
      //on verifie aussi que les $max premiers modelès populaires ($val['popular'] = true)
      /*if($cpt <= $max)
        $this->assertTrue($val['popular']);
      else $this->assertFalse($val['popular']);
      $cpt++;*/
    }

    $sujet = $this->getRepository("Sujet")->findOneBy(array("id" => $sid));

    $annots = $this->getRepository("Annotation")->findByRendu($renduRep->findBySujet($sujet));

    $modelsFromListModels = $this->getRepository("Modele")->findByListe($sujet->getListeModeles());

    //modOcc contiendra tous les modèles associés au sujet (c-a-d ceux qui fonft partie de la liste ListeModele du sujet
    //plus les plausibles modèles globaux utilisés
    $modOcc = array();
    $max = 0;
    foreach($annots as $key => $val){
      if($val->getModele() != null)
        if(!array_key_exists($val->getModele()->getNom(), $modOcc)){
          $modOcc[$val->getModele()->getNom()] = 1;
          //nb de d'annotations basées sur un modèle peut être inférieure ou égal à la valeur initiale de $max (out en haut)
          $max++;
        }
        else $modOcc[$val->getModele()->getNom()]++;
    }

    $nbPop = 0;
    foreach ($modeles as $k => $v) {
      if($v['popular']){
        $nbPop++;
      }
    }

    //on vérifie que le nombre de modèles populaires renvoyés est bien inférieur ou égal à $max
    $this->assertTrue($nbPop<=$max);

    //on vérifie que les $max premiers modèles retournés par getMostPopular sont "populaires"
    $i = 0;
    foreach($modeles as $k => $v){
      if($i < $nbPop)
        $this->assertTrue($v['popular']);
      else $this->assertFalse($v['popular']);
      $i++;
    }

    //on ajoute ici, les modèles de la ListeModele   sujet qui ne sont pas utilisés par les annotations
    foreach ($modelsFromListModels as $k => $v) {
      //si le nom du modele ne fait pas partie des index du tableau modOcc alors aucune annotation ne l'utilise
      //=> son occurrence est égale à 0
      if(!array_key_exists($v->getNom(), $modOcc)){
        $modOcc[$v->getNom()] = 0;
      }
    }

    //On trie le tableau par ordre décroissant.
    arsort($modOcc);

    //Vérification que le tableau est trié dans l'ordre décroissant en fonction du nombre d'occurrences
    $this->assertTrue($try);

    //Vérification que le nombre d'annotation ne dépasse pas $max.
//    $this->assertTrue(sizeof($modeles) <= $max);

    //Tous les modèles associés au sujet sont retournés avec leurs nb d'occurrences
    foreach($modeles as $key => $val){
      $this->assertEquals($modOcc[$val['modele']->getNom()], $val["occ"]);
    }
  }

  /*
   * Vérification que l'on retrouve le même résultat en utilisant les repository.
   * Mêmes tests que ceux qui sont dans la fonction test_get_most_popular sauf
   * qu'on ajoute le paramètre $gid (pour le groupe)
   */
  public function test_get_most_popular_avec_groupe()
  {
    $max = 3;
    $sid = 1;
    $gid = 4;

    $modeles = $this->getRepository("Modele")->getMostPopular($sid, $max, $gid);

    $renduRep = $this->getRepository("Rendu");

    $try = true;
//    $cpt = 1;
    $prec_occ = -1;
    foreach ($modeles as $key => $val) {
      //La première valeur ne peut être comparée
      if ($prec_occ != -1) {
        //si l'occurrence précédente $prec_occ est inférieure à l'occurrence courante $val['occ'] alors on passe $try à false
        if ($val["occ"] > $prec_occ)
          $try = false;
      }
      $prec_occ = $val["occ"];
      //on verifie aussi que les $max premiers modelès populaires ($val['popular'] = true)
      /*if($cpt <= $max)
        $this->assertTrue($val['popular']);
      else $this->assertFalse($val['popular']);
      $cpt++;*/
    }

    $sujet = $this->getRepository("Sujet")->findOneById($sid);

    $group = $this->getRepository("Group")->findOneById($gid);

    $annots = $this->getRepository("Annotation")->findByRendu($renduRep->findBy(array("sujet" => $sujet, "group" => $group)));

    $modelsFromListModels = $this->getRepository("Modele")->findByListe($sujet->getListeModeles());

    //modOcc contiendra tous les modèles associés au sujet (c-a-d ceux qui fonft partie de la liste ListeModele du sujet
    //plus les plausibles modèles globaux utilisés
    $modOcc = array();
    $max = 0;
    foreach($annots as $key => $val){
      if($val->getModele() != null)
        if(!array_key_exists($val->getModele()->getNom(), $modOcc)){
          $modOcc[$val->getModele()->getNom()] = 1;
          //nb de d'annotations basées sur un modèle peut être inférieure ou égal à la valeur initiale de $max (out en haut)
          $max++;
        }
        else $modOcc[$val->getModele()->getNom()]++;
    }

    $nbPop = 0;
    foreach ($modeles as $k => $v) {
      if($v['popular']){
        $nbPop++;
      }
    }

    //on vérifie que le nombre de modèles populaires renvoyés est bien inférieur ou égal à $max
    $this->assertTrue($nbPop<=$max);

    //on vérifie que les $max premiers modèles retournés par getMostPopular sont "populaires"
    $i = 0;
    foreach($modeles as $k => $v){
      if($i < $nbPop)
        $this->assertTrue($v['popular']);
      else $this->assertFalse($v['popular']);
      $i++;
    }

    //on ajoute ici, les modèles de la ListeModele sujet qui ne sont pas utilisés par les annotations
    foreach ($modelsFromListModels as $k => $v) {
      //si le nom du modele ne fait pas partie des index du tableau modOcc alors aucune annotation ne l'utilise
      //=> son occurrence est égale à 0
      if(!array_key_exists($v->getNom(), $modOcc)){
        $modOcc[$v->getNom()] = 0;
      }
    }

    //On trie le tableau par ordre décroissant.
    arsort($modOcc);

    //Vérification que le tableau est trié dans l'ordre décroissant en fonction du nombre d'occurrences
    $this->assertTrue($try);

    //Vérification que le nombre d'annotation ne dépasse pas $max.
//    $this->assertTrue(sizeof($modeles) <= $max);

    //Vérification que les $max premiers modeles sont bien ceux retournés par la fonction getMostPopular en comparant le nombre d'occurrences de ces modeles
    foreach ($modeles as $key => $val) {
      $this->assertEquals($modOcc[$val['modele']->getNom()], $val["occ"]);
    }
  }

  /*
   * Vérification que pour un sujet, ou un groupe inexistant la fonction retourne une erreur.
   */
  public function get_most_popular_with_wrong_sid(){
    $max = 10;
    $sid = null;
    $gid = 1000;

    //Check the function fails with these parameters: correct sid & wrong gid
    try{
      $resultats = $this->getRepository("Modele")->getMostPopular($sid, $max, $gid);
    }
    catch(Exception $e){
      $this->assertTrue(strpos($e->getMessage(), "sid") >= 0);
      return;
    }

    $this->fails('Wrong sid exception was expected to be raised.');


    $sid = -1000;
    //Check that the function fails with these parameters: wrong sid
    try{
      $resultats = $this->getRepository("Modele")->getMostPopular($sid, $max, $gid);
    }
    catch(Exception $e){
      $this->assertTrue(strpos($e->getMessage(), "sid") >= 0);
      return ;
    }

    $this->fails("Wrong sid exception was expected to raised.");
  }

  /*
 * Vérification que l'on retrouve le même résultat en utilisant les repository.
 * Mêmes tests que ceux qui sont dans la fonction test_get_most_popular sauf
 * qu'on fait la recherche pour une ue et non pour un sujet.
 */
  public function test_get_most_popular_ue()
  {
    $max = 3;
    $ue_id = 1;

    $modeles = $this->getRepository("Modele")->getMostPopularByUE($ue_id, $max);

    $renduRep = $this->getRepository("Rendu");

    $try = true;
//    $cpt = 1;
    $prec_occ = -1;
    foreach ($modeles as $key => $val) {
      //La première valeur ne peut être comparée
      if ($prec_occ != -1) {
        //si l'occurrence précédente $prec_occ est inférieure à l'occurrence courante $val['occ'] alors on passe $try à false
        if ($val["occ"] > $prec_occ)
          $try = false;
      }
      $prec_occ = $val["occ"];
      //on verifie aussi que les $max premiers modèles populaires ($val['popular'] = true)
      /*if($cpt <= $max)
        $this->assertTrue($val['popular']);
      else $this->assertFalse($val['popular']);
      $cpt++;*/
    }

    $ue = $this->getRepository("UE")->findOneById($ue_id);

    $sujets = $this->getRepository("Sujet")->findBy(array("UE" => $ue));

    $annots = $this->getRepository("Annotation")->findByRendu($renduRep->findBy(array("sujet" => $sujets)));

    $listModels = array();

    foreach ($sujets as $k => $v) {
      $listModels = $listModels + $this->getRepository("ListeModeles")->findById($v->getListeModeles());
    }

    $modelsFromListModels = $this->getRepository("Modele")->findByListe($listModels);

    //modOcc contiendra tous les modèles associés aux sujets (c-a-d ceux qui fonft partie des ListeModeles des sujets
    //plus les plausibles modèles globaux utilisés
    $modOcc = array();
    $max = 0;
    foreach($annots as $key => $val){
      if($val->getModele() != null)
        if(!array_key_exists($val->getModele()->getNom(), $modOcc)){
          $modOcc[$val->getModele()->getNom()] = 1;
          //nb de d'annotations basées sur un modèle peut être inférieure ou égal à la valeur initiale de $max (out en haut)
          $max++;
        }
        else $modOcc[$val->getModele()->getNom()]++;
    }

    $nbPop = 0;
    foreach ($modeles as $k => $v) {
      if($v['popular']){
        $nbPop++;
      }
    }

    //on vérifie que le nombre de modèles populaires renvoyés est bien inférieur ou égal à $max
    $this->assertTrue($nbPop<=$max);

    //on vérifie que les $max premiers modèles retournés par getMostPopular sont "populaires"
    $i = 0;
    foreach($modeles as $k => $v){
      if($i < $nbPop)
        $this->assertTrue($v['popular']);
      else $this->assertFalse($v['popular']);
      $i++;
    }

    //on ajoute ici, les modèles de la ListeModele sujet qui ne sont pas utilisés par les annotations
    foreach ($modelsFromListModels as $k => $v) {
      //si le nom du modele ne fait pas partie des index du tableau modOcc alors aucune annotation ne l'utilise
      //=> son occurrence est égale à 0
      if(!array_key_exists($v->getNom(), $modOcc)){
        $modOcc[$v->getNom()] = 0;
      }
    }

    //On trie le tableau par ordre décroissant.
    arsort($modOcc);

    //Vérification que le tableau est trié dans l'ordre décroissant en fonction du nombre d'occurrences
    $this->assertTrue($try);

    //Vérification que le nombre d'annotation ne dépasse pas $max.
//    $this->assertTrue(sizeof($modeles) <= $max);

    //Vérification que les $max premiers modeles sont bien ceux retournés par la fonction getMostPopular en comparant le nombre d'occurrences de ces modeles
    foreach ($modeles as $key => $val) {
      $this->assertEquals($modOcc[$val['modele']->getNom()], $val["occ"]);
    }
  }

  /*
 * Vérification que l'on retrouve le même résultat en utilisant les repository.
 * Mêmes tests que ceux qui sont dans la fonction test_get_most_popular_ue sauf
 * qu'on ajoute le paramètre $gid (pour le groupe).
 */
  public function test_get_most_popular_ue_avec_groupe()
  {
    $max = 3;
    $ue_id = 1;
    $gid = 4;

    $modeles = $this->getRepository("Modele")->getMostPopularByUE($ue_id, $max, $gid);

    $renduRep = $this->getRepository("Rendu");

    $try = true;
//    $cpt = 1;
    $prec_occ = -1;
    foreach ($modeles as $key => $val) {
      //La première valeur ne peut être comparée
      if ($prec_occ != -1) {
        //si l'occurrence précédente $prec_occ est inférieure à l'occurrence courante $val['occ'] alors on passe $try à false
        if ($val["occ"] > $prec_occ)
          $try = false;
      }
      $prec_occ = $val["occ"];
      //on verifie aussi que les $max premiers modelès populaires ($val['popular'] = true)
      /*if($cpt <= $max)
        $this->assertTrue($val['popular']);
      else $this->assertFalse($val['popular']);
      $cpt++;*/
    }

    $ue = $this->getRepository("UE")->findOneById($ue_id);

    $sujets = $this->getRepository("Sujet")->findBy(array("UE" => $ue));

    $group = $this->getRepository("Group")->findOneById($gid);

    $annots = $this->getRepository("Annotation")->findByRendu($renduRep->findBy(array("sujet" => $sujets, "group" => $group)));

    $listModels = array();

    foreach ($sujets as $k => $v) {
      $listModels = $listModels + $this->getRepository("ListeModeles")->findById($v->getListeModeles());
    }

    $modelsFromListModels = $this->getRepository("Modele")->findByListe($listModels);

    //modOcc contiendra tous les modèles associés aux sujets (c-a-d ceux qui font partie des ListeModeles des sujets
    //plus les plausibles modèles globaux utilisés
    $modOcc = array();
    $max = 0;
    foreach($annots as $key => $val){
      if($val->getModele() != null)
        if(!array_key_exists($val->getModele()->getNom(), $modOcc)){
          $modOcc[$val->getModele()->getNom()] = 1;
          //nb de d'annotations basées sur un modèle peut être inférieure ou égal à la valeur initiale de $max (out en haut)
          $max++;
        }
        else $modOcc[$val->getModele()->getNom()]++;
    }

    $nbPop = 0;
    foreach ($modeles as $k => $v) {
      if($v['popular']){
        $nbPop++;
      }
    }

    //on vérifie que le nombre de modèles populaires renvoyés est bien inférieur ou égal à $max
    $this->assertTrue($nbPop<=$max);

    //on vérifie que les $max premiers modèles retournés par getMostPopular sont "populaires"
    $i = 0;
    foreach($modeles as $k => $v){
      if($i < $nbPop)
        $this->assertTrue($v['popular']);
      else $this->assertFalse($v['popular']);
      $i++;
    }

    //on ajoute ici, les modèles de la ListeModele sujet qui ne sont pas utilisés par les annotations
    foreach ($modelsFromListModels as $k => $v) {
      //si le nom du modele ne fait pas partie des index du tableau modOcc alors aucune annotation ne l'utilise
      //=> son occurrence est égale à 0
      if(!array_key_exists($v->getNom(), $modOcc)){
        $modOcc[$v->getNom()] = 0;
      }
    }

    //On trie le tableau par ordre décroissant.
    arsort($modOcc);

    //Vérification que le tableau est trié dans l'ordre décroissant en fonction du nombre d'occurrences
    $this->assertTrue($try);

    //Vérification que le nombre d'annotation ne dépasse pas $max.
//    $this->assertTrue(sizeof($modeles<= $max);

    //Vérification que les $max premiers modeles sont bien ceux retournés par la fonction getMostPopular en comparant le nombre d'occurrences de ces modeles
    foreach ($modeles as $key => $val) {
      $this->assertEquals($modOcc[$val['modele']->getNom()], $val["occ"]);
    }
  }

  /*
 * Vérification que pour une ue null ou negative la fonction retourne une erreur.
 */
  public function get_most_popular_ue_with_wrong_ueid(){
    $max = 10;
    $ue_id = -1;
    $gid = null;

    //Check that function fails with these parameters: ue_id negative
    try{
      $resultats = $this->getRepository("Modele")->getMostPopularByUE($ue_id, $max, $gid);
    }
    catch(Exception $e){
      $this->assertTrue(strpos($e->getMessage(), "ue_id") >= 0);
      return;
    }

    $this->fails('Wrong ue_id exception was expected to be raised.');


    $ue_id = null;
    //Check that the function fails with these parameters: ue_id null
    try{
      $resultats = $this->getRepository("Modele")->getMostPopularByUE($ue_id, $max, $gid);
    }
    catch(Exception $e){
      $this->assertTrue( strpos($e->getMessage(), "ue_id") >= 0);
      return ;
    }

    $this->fails("Wrong ue_id exception was expected to raised.");
  }
}
