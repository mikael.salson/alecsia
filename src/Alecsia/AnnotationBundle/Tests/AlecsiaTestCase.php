<?php

namespace Alecsia\AnnotationBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AlecsiaTestCase extends WebTestCase {

   /**
    * @var \Doctrine\ORM\EntityManager
    */
   protected $em;
   protected $validator;
   protected $doctrine;

   protected function getRepository($entity) {
      return $this->em->getRepository('AnnotationBundle:' . $entity);
   }

   /**
    * {@inheritDoc}
    */
   public function setUp() {
      static::$kernel = static::createKernel();
      static::$kernel->boot();
      $this->doctrine = static::$kernel->getContainer()
              ->get('doctrine');
      $this->em = $this->doctrine
              ->getManager();
      $this->validator = static::$kernel->getContainer()
              ->get('validator');
   }

   /**
    * {@inheritDoc}
    */
   protected function tearDown() {
      parent::tearDown();
      $this->em->close();
   }

}
