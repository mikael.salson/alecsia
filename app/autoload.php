<?php

use Doctrine\Common\Annotations\AnnotationRegistry;

require_once __DIR__ . '/../vendor/symfony/symfony/src/Symfony/Component/ClassLoader/UniversalClassLoader.php';

use Symfony\Component\ClassLoader\UniversalClassLoader;

$loader = new UniversalClassLoader();

// enregistrez les espaces de noms et préfixes ici (voir ci-dessous)
// vous pouvez rechercher dans l'« include_path » en dernier recours.
$loader->useIncludePath(true);

$loader = include __DIR__ . '/../vendor/autoload.php';

// intl
if (!function_exists('intl_get_error_code')) {
   require_once __DIR__ . '/../vendor/symfony/symfony/src/Symfony/Component/Locale/Resources/stubs/functions.php';

   $loader->add('', __DIR__ . '/../vendor/symfony/symfony/src/Symfony/Component/Locale/Resources/stubs');
}

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

return $loader;
